<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Smartech Integraciones</title>
</head>
<body>
	Greeting : <c:out value="${greeting}"/>
      
      	<form action = "customers" method = "POST">
         Customer: <input type = "text" name = "customer">
         <input type = "submit" value = "Submit" />
      </form>
      <br />
      <form action = "items" method = "POST">
         Item: <input type = "text" name = "item">
         <input type = "submit" value = "Submit" />
      </form>
      <br />

<!--  
       <br />
        <form action = "mobile" method = "GET">
         <input type = "submit" value = "Mobile" />
      </form>
      -->

</body>
</html>
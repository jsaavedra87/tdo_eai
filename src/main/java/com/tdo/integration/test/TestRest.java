package com.tdo.integration.test;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public class TestRest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RestTemplate restTemplate = createRestTemplate();
		HttpHeaders headers = createHeaders("javila@smartech.com.mx", "1234qwer");
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		try {
			String url = "https://eiyp.fa.us6.oraclecloud.com/hcmCoreSetupApi/scim/Users/6EAF4F8AB3C74302E0503B0A9DA914D5";
			
			ResponseEntity<String> res = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			System.out.println(res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	
	private static RestTemplate createRestTemplate() {

	    ClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

	    RestTemplate restTemplate = new RestTemplate(factory);
	    // set the media types properly
	    return restTemplate;
	}
	
	public static HttpHeaders createHeaders(final String username, final String password){
		final String data = "";
		HttpHeaders hdrs =  new HttpHeaders() {{
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.encodeBase64( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		      }};
		      
		      return hdrs;
		}
		
}

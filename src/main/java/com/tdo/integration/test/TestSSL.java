package com.tdo.integration.test;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class TestSSL {

	public static void main(String[] args) {
		
		try {
		
			System.setProperty("javax.net.debug", "ssl,handshake");
	        System.setProperty("javax.net.ssl.keyStoreType", "JKS");
	        System.setProperty("javax.net.ssl.keyStore", "C:/temp/clientSicar.jks");
	        System.setProperty("javax.net.ssl.keyStorePassword", "admin");
			

		 	final URL url = new URL("https://192.168.0.81:9443//rest/articulos/buscarArticulo?pagina=1");
	        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
	        
	        conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "Basic YWRtaW46YWRtaW4=");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
	 
	        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String inputLine;
	        final StringBuffer response = new StringBuffer();
	        while ((inputLine = bufferedReader.readLine()) != null) {
	            response.append(inputLine);
	        }
	        bufferedReader.close();
	        System.out.println(response.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}



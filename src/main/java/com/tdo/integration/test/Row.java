package com.tdo.integration.test;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;


public class Row {
	
     protected String column0;
     protected String column1;
     protected String column2;
     protected String column3;
     protected String column4;
     protected XMLGregorianCalendar column5;
     protected String column6;
     protected XMLGregorianCalendar column7;
     protected XMLGregorianCalendar column8;
     protected String column9;
     protected String column10;
     protected String column11;
     protected double column12;
     protected String column13;
     protected String column14;
     protected XMLGregorianCalendar column15;
     
	public String getColumn0() {
		return column0;
	}
	
	@XmlElement(name = "Column0", required = true)
	public void setColumn0(String column0) {
		this.column0 = column0;
	}
	public String getColumn1() {
		return column1;
	}
	
	@XmlElement(name = "Column1", required = true)
	public void setColumn1(String column1) {
		this.column1 = column1;
	}
	public String getColumn2() {
		return column2;
	}
	
	@XmlElement(name = "Column2", required = true)
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	public String getColumn3() {
		return column3;
	}
	
	@XmlElement(name = "Column3", required = true)
	public void setColumn3(String column3) {
		this.column3 = column3;
	}
	public String getColumn4() {
		return column4;
	}
	
	@XmlElement(name = "Column4", required = true)
	public void setColumn4(String column4) {
		this.column4 = column4;
	}
	public XMLGregorianCalendar getColumn5() {
		return column5;
	}
	
	@XmlElement(name = "Column5", required = true)
    @XmlSchemaType(name = "dateTime")
	public void setColumn5(XMLGregorianCalendar column5) {
		this.column5 = column5;
	}
	public String getColumn6() {
		return column6;
	}
	
	@XmlElement(name = "Column6", required = true)
	public void setColumn6(String column6) {
		this.column6 = column6;
	}
	public XMLGregorianCalendar getColumn7() {
		return column7;
	}
	
	@XmlElement(name = "Column7", required = true)
    @XmlSchemaType(name = "dateTime")
	public void setColumn7(XMLGregorianCalendar column7) {
		this.column7 = column7;
	}
	public XMLGregorianCalendar getColumn8() {
		return column8;
	}
	
	@XmlElement(name = "Column8", required = true)
    @XmlSchemaType(name = "dateTime")
	public void setColumn8(XMLGregorianCalendar column8) {
		this.column8 = column8;
	}
	public String getColumn9() {
		return column9;
	}
	
	@XmlElement(name = "Column9", required = true)
	public void setColumn9(String column9) {
		this.column9 = column9;
	}
	public String getColumn10() {
		return column10;
	}
	
	@XmlElement(name = "Column10", required = true)
	public void setColumn10(String column10) {
		this.column10 = column10;
	}
	public String getColumn11() {
		return column11;
	}
	
	@XmlElement(name = "Column11", required = true)
	public void setColumn11(String column11) {
		this.column11 = column11;
	}
	public double getColumn12() {
		return column12;
	}
	
	@XmlElement(name = "Column12", required = true)
	public void setColumn12(double column12) {
		this.column12 = column12;
	}
	
	@XmlElement(name = "Column13", required = true)
	public void setColumn13(String column13) {
		this.column13 = column13;
	}
	public String getColumn13() {
		return column13;
	}

	@XmlElement(name = "Column14", required = true)
	public void setColumn14(String column14) {
		this.column14 = column14;
	}
	public String getColumn14() {
		return column14;
	}

	@XmlElement(name = "Column15", required = true)
    @XmlSchemaType(name = "dateTime")	
	public void setColumn15(XMLGregorianCalendar column15) {
		this.column15 = column15;
	}	
	public XMLGregorianCalendar getColumn15() {
		return column15;
	}

}

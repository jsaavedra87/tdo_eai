package com.tdo.integration.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.tdo.integration.data.model.tdo.Abono;
import com.tdo.integration.data.model.tdo.Articulo;
import com.tdo.integration.data.model.tdo.ArticuloImpuesto;
import com.tdo.integration.data.model.tdo.CancelacionVenta;
import com.tdo.integration.data.model.tdo.Cliente;
import com.tdo.integration.data.model.tdo.Credito;
import com.tdo.integration.data.model.tdo.CustomerAdditionalSpecs;
import com.tdo.integration.data.model.tdo.FileTransferControl;
import com.tdo.integration.data.model.tdo.InventoryMonitor;
import com.tdo.integration.data.model.tdo.InventoryTransaction;
import com.tdo.integration.data.model.tdo.MovimientosCaja;
import com.tdo.integration.data.model.tdo.NotaCredito;
import com.tdo.integration.data.model.tdo.NotaCreditoAbono;
import com.tdo.integration.data.model.tdo.Sales;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.dto.CreditDTO;
import com.tdo.integration.dto.CreditNoteDTO;
import com.tdo.integration.dto.CreditNoteDetailDTO;
import com.tdo.integration.dto.SalesDTO;

@Repository("integrationTDODataDao")
@Transactional 
public class IntegrationTDODataDao {
	
	DataSource dataSource;
	Connection conn = null;
    private SessionFactory sessionFactory;
    private final int ID_CLIENTE_VENTA_PUBLICO = 1;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void setDataSource(DataSource dataSource) {
		try {
			this.dataSource = dataSource;
			this.conn = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public CustomerAdditionalSpecs saveCustomerAdditionalSpecs(CustomerAdditionalSpecs specs) throws Exception{
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(specs);
			return specs;
		}catch(Exception e) {
			throw e;
		}
	
	}

	public boolean saveOrUpdateCustomer(Cliente cliente) {
		try {			
			Cliente clienteCDB = new Cliente();
			Session session = sessionFactory.getCurrentSession();			
			
			clienteCDB = (Cliente)session.byNaturalId(Cliente.class).using("clienteNubeId", cliente.getClienteNubeId()).load();
			
			if(clienteCDB == null) {
				//En la alta del cliente el Limite de Credito Absoluto es el Limite de Credito que viene por primera vez.
				cliente.setClienteLimiteAbsoluto(cliente.getClienteLimite());
				session.save(cliente);
			} else {
				//Se recuperan los valores que se encuentran unicamente en la BD centralizada.
				cliente.setClienteLimiteAbsoluto(clienteCDB.getClienteLimiteAbsoluto());
				
				if((clienteCDB.getClientePrecio() == null && cliente.getClientePrecio() != null)
						|| (clienteCDB.getClientePrecio() != null && cliente.getClientePrecio() != null && clienteCDB.getClientePrecio().intValue() != cliente.getClientePrecio().intValue())) { 
					clienteCDB.setClientePrecio(cliente.getClientePrecio());
					session.update(clienteCDB);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	@SuppressWarnings({"unchecked" })
	public boolean saveSales(SalesDTO salesDTO){
		boolean isExists = false;
		
		try {
			Session session = sessionFactory.getCurrentSession();
			
			List<Sales> newSales = salesDTO.getSales();
			List<Sales> currentSales = null;
			
			if(newSales != null) {
				Sales s = newSales.get(0);
				Criteria criteria = session. createCriteria(Sales.class);
				criteria.add(Restrictions.eq("idResumenCaja", s.getIdResumenCaja()));
				criteria.add(Restrictions.eq("sucursal", s.getSucursal()));
				currentSales = criteria.list();
				if(currentSales != null) {
					if(!currentSales.isEmpty()) {
						isExists = true;
						return true;
					}
				}
			}
			
//			if(isExists) {
//				for(Sales nSale: newSales) {
//					for(Sales cSale: currentSales) {
//						if(cSale.getIdVenta().equals(nSale.getIdVenta())) {
//							nSale.setId(cSale.getId());							
//							if(nSale.getArticulo() != null && !nSale.getArticulo().isEmpty() && cSale.getArticulo() != null && !cSale.getArticulo().isEmpty()) {
//								for(Articulo nItem : nSale.getArticulo()) {
//									for(Articulo cItem : cSale.getArticulo()) {
//										if(cItem.getArticuloId().equals(nItem.getArticuloId())) {
//											nItem.setId(cItem.getId());
//											break;
//										}
//									}									
//								}
//							}
//							
//							if(nSale.getArticuloImpuesto() != null && !nSale.getArticuloImpuesto().isEmpty() && cSale.getArticuloImpuesto() != null && !cSale.getArticuloImpuesto().isEmpty()) {
//								for(ArticuloImpuesto nItemTax) {
//									
//								}
//							}
//						}
//					}
//				}
//			}

			if(salesDTO.getSales() != null) {
				for(Sales sale : salesDTO.getSales()) {
					session.saveOrUpdate(sale);
				}
			}
			
			if(salesDTO.getNcExtemporaneas() != null) {
				for(NotaCredito nc : salesDTO.getNcExtemporaneas()) {
					session.saveOrUpdate(nc);
				}
			}

			if(salesDTO.getMovimientosCaja() != null) {
				for(MovimientosCaja mov : salesDTO.getMovimientosCaja()) {
					session.saveOrUpdate(mov);
				}
			}

			if(salesDTO.getCancelaciones() != null) {
				for(CancelacionVenta can : salesDTO.getCancelaciones()) {
					session.saveOrUpdate(can);
				}
			}

			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	
	}
	
	@SuppressWarnings({"unchecked" })
	public boolean saveInventoryMonitor(InventoryMonitor inventoryMonitor){
		try {
			Session session = sessionFactory.getCurrentSession();			
			
			if(inventoryMonitor != null) {
//				Criteria criteria = session. createCriteria(InventoryMonitor.class);
//				criteria.add(Restrictions.eq("branchId", inventoryMonitor.getBranchId()));
//				criteria.add(Restrictions.eq("branchName", inventoryMonitor.getBranchName()));
//				criteria.add(Restrictions.eq("branchCode", inventoryMonitor.getBranchCode()));				
//				InventoryMonitor im = (InventoryMonitor)criteria.uniqueResult();
//				
//				if(im != null) {
//					im.setItemCount(inventoryMonitor.getItemCount());
//					im.setLotCount(inventoryMonitor.getLotCount());
//					im.setCloudCount(inventoryMonitor.getCloudCount());
//					im.setLastUpdate(inventoryMonitor.getLastUpdate());
//					im.setStatus(inventoryMonitor.getStatus());
//					session.update(im);
//				} else {
					session.save(inventoryMonitor);
//				}
				
			}
			
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	
	}
	
	public boolean saveOrUpdateCredit(List<CreditDTO> lstCreditoDTO) {
		try {			
			Session session = sessionFactory.getCurrentSession();
			
			if(lstCreditoDTO != null && lstCreditoDTO.size() > 0) {
				for(CreditDTO c : lstCreditoDTO) {
					
					this.saveOrUpdateCustomer(c.getCliente());
						
					if(c.getCreditos() != null && c.getCreditos().size() > 0) {
						for(Credito credit : c.getCreditos()) {
							Criteria criteria = session.createCriteria(Credito.class);
							Credito creditCDB = (Credito)criteria
														.add(Restrictions.eq("creditoId", credit.getCreditoId()))
														.add(Restrictions.eq("sucursal", credit.getSucursal()))
														.uniqueResult();
							if(creditCDB != null) {
								if(!creditCDB.getStatus().equals(-1) && !creditCDB.getStatus().equals(2)) {
									this.saveOrUpdatePayment(credit.getAbono());
									this.saveOrUpdateCreditNotePayment(credit.getNotacredito());
									creditCDB.setStatus(credit.getStatus());
									session.update(creditCDB);
								}
							} else {
								session.save(credit);
							}							
						}
					}
				}				
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}	
	
	public boolean saveOrUpdatePayment(List<Abono> lstPayment) {
		try {
			Session session = sessionFactory.getCurrentSession();
			
			if(lstPayment != null && !lstPayment.isEmpty()) {
				for(Abono payment : lstPayment) {
					Criteria criteria = session.createCriteria(Abono.class);
					Abono paymentCDB = (Abono)criteria
							.add(Restrictions.eq("abonoId", payment.getAbonoId()))
							.add(Restrictions.eq("sucursal", payment.getSucursal()))
							.uniqueResult();
					if(paymentCDB != null) {
						if(paymentCDB.getIdResumenCaja() == null && payment.getIdResumenCaja() != null) {
							paymentCDB.setIdResumenCaja(payment.getIdResumenCaja());
							session.update(paymentCDB);
						} 
					} else {
						session.save(payment);
					}
				}
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean saveOrUpdateCreditNotePayment(List<NotaCreditoAbono> lstPayment) {
		try {
			Session session = sessionFactory.getCurrentSession();
			
			if(lstPayment != null && !lstPayment.isEmpty()) {
				for(NotaCreditoAbono payment : lstPayment) {
					Criteria criteria = session.createCriteria(NotaCreditoAbono.class);
					NotaCreditoAbono paymentCDB = (NotaCreditoAbono)criteria
							.add(Restrictions.eq("idNotaCredito", payment.getIdNotaCredito()))
							.add(Restrictions.eq("sucursal", payment.getSucursal()))
							.uniqueResult();
					if(paymentCDB != null) {
						if(paymentCDB.getIdResumenCaja() == null && payment.getIdResumenCaja() != null) {
							paymentCDB.setIdResumenCaja(payment.getIdResumenCaja());
							session.update(paymentCDB);
						} 
					} else {
						session.save(payment);
					}
				}
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}	
	
	public BigDecimal getCustomerCreditAmount(String sucursal, Cliente cliente) {
		BigDecimal creditoTotal = new BigDecimal(0);
		
		try {			
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Credito.class);			
			if(!"".equals(sucursal)) {
				criteria.add(Restrictions.eq("sucursal", sucursal));
			}
			criteria.add(Restrictions.eq("clienteNubeId", cliente.getClienteNubeId()));
			criteria.add(Restrictions.ne("status", -1));
		    criteria.setProjection(Projections.sum("total"));
		    creditoTotal = (BigDecimal) criteria.uniqueResult();
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return creditoTotal != null ? creditoTotal : new BigDecimal(0);
	}
	
	public BigDecimal getCustomerPaymentAmount(String sucursal, Cliente cliente) {
		BigDecimal abonoTotal = new BigDecimal(0);
		
		try {			
			Session session = sessionFactory.getCurrentSession();			
			Criteria criteria = session.createCriteria(Abono.class);
			
			if(!"".equals(sucursal)) {
				criteria.add(Restrictions.eq("sucursal", sucursal));
			}
			criteria.add(Restrictions.eq("clienteNubeId", cliente.getClienteNubeId()));
		    criteria.setProjection(Projections.sum("abonoTotal"));
		    abonoTotal = (BigDecimal) criteria.uniqueResult();
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abonoTotal != null ? abonoTotal : new BigDecimal(0);
	}

	public BigDecimal getCustomerCreditNoteAmount(String sucursal, Cliente cliente) {
		BigDecimal abonoTotal = new BigDecimal(0);
		
		try {			
			Session session = sessionFactory.getCurrentSession();			
			Criteria criteria = session.createCriteria(NotaCreditoAbono.class);
			
			if(!"".equals(sucursal)) {
				criteria.add(Restrictions.eq("sucursal", sucursal));
			}
			criteria.add(Restrictions.eq("clienteNubeId", cliente.getClienteNubeId()));
		    criteria.setProjection(Projections.sum("total"));
		    abonoTotal = (BigDecimal) criteria.uniqueResult();
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		return abonoTotal != null ? abonoTotal : new BigDecimal(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<CreditDTO> getCustomerCreditByName(String value) {
		List<CreditDTO> listCreditDTO = new ArrayList<CreditDTO>();
		Session session = sessionFactory.getCurrentSession();
		
		try { 
			Criteria criteria = session.createCriteria(Cliente.class);
			criteria.add(
					Restrictions.conjunction()
					.add(Restrictions.ne("clienteNubeId", new BigInteger("0")))
					.add(Restrictions.ilike("clienteNombre", value, MatchMode.ANYWHERE))
					);
			List<Cliente> list = criteria.list();
			
			if(list != null && list.size() > 0) {
				CreditDTO creditDTO;
				for(Cliente c : list) {
					creditDTO = new CreditDTO();
					creditDTO.setCliente(c);
					listCreditDTO.add(creditDTO);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listCreditDTO;
	}
	
	public boolean updateCreditLimit(CreditDTO creditDTO) {
		
		try {
			Session session = sessionFactory.getCurrentSession();
			
			String sql = String.format("update Cliente set clienteLimiteAbsoluto = %s where clienteNubeId = %s", creditDTO.getCliente().getClienteLimiteAbsoluto(), creditDTO.getCliente().getClienteNubeId());
			Query q = session.createQuery(sql);
			q.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean deleteCreditByCustomer(String sucursal, Cliente cliente) {
		
		try {		
			Session session = sessionFactory.getCurrentSession();
			
			String sql = String.format("delete Abono where sucursal = '%s' and clienteNubeId = %s", sucursal, cliente.getClienteNubeId());
			Query q = session.createQuery(sql);
			q.executeUpdate();
			
			sql = String.format("delete Credito where sucursal = '%s' and clienteNubeId = %s", sucursal, cliente.getClienteNubeId());
			q = session.createQuery(sql);
			q.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	
	/**
	 * 
	 * @param udc
	 * @throws Exception
	 */
	public boolean saveOrUpdateUdc(Udc udc) throws Exception{
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(udc);
			return true;

		}catch(Exception e) {
			throw e;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Udc> getUdcByUdcKey(String udcKey) {
		
			Session session = this.sessionFactory.getCurrentSession();
			Criteria cr = session.createCriteria(Udc.class);
			cr.add(Restrictions.eq("udckey",udcKey));
			List<Udc> results = cr.list();
			return results;
	}

	@SuppressWarnings("unchecked")
	public List<FileTransferControl> getFileTransferControlByIdCC(FileTransferControl ftc){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branchId", ftc.getBranchId()))
				.add(Restrictions.eq("branch", ftc.getBranch()))				
				.add(Restrictions.eq("transactionType", ftc.getTransactionType()))
				.add(Restrictions.eq("idCC", ftc.getIdCC()))
				);
		return (List<FileTransferControl>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<FileTransferControl> getFileTransferControlByFromDate(FileTransferControl ftc){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branchId", ftc.getBranchId()))
				.add(Restrictions.eq("branch", ftc.getBranch()))
				.add(Restrictions.eq("fromDate", ftc.getFromDate()))
				.add(Restrictions.eq("transactionType", ftc.getTransactionType()))				
				);		
		return (List<FileTransferControl>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<FileTransferControl> getFileTransferControlByStatus(FileTransferControl ftc){
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branchId", ftc.getBranchId()))
				.add(Restrictions.eq("branch", ftc.getBranch()))				
				.add(Restrictions.eq("transactionType", ftc.getTransactionType()))
				.add(Restrictions.eq("status", ftc.getStatus()))
				);
		return (List<FileTransferControl>) criteria.list();
	}
		
	@SuppressWarnings("unchecked")
	public void saveOrUpdateFileTransferControl(FileTransferControl ftc) {
		Session session = this.sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(FileTransferControl.class);
		criteria.add(
				Restrictions.conjunction()
				.add(Restrictions.eq("branchId", ftc.getBranchId()))
				.add(Restrictions.eq("idCC", ftc.getIdCC()))
				.add(Restrictions.eq("fromDate", ftc.getFromDate()))
				.add(Restrictions.eq("transactionType", ftc.getTransactionType()))				
				);
		List<FileTransferControl> lstFTC = (List<FileTransferControl>) criteria.list();
		if(lstFTC != null && lstFTC.size() > 0) {
			FileTransferControl o = lstFTC.get(0);
			o.setLastUpdate(ftc.getLastUpdate());
			o.setStatus(ftc.getStatus());
			o.setLog(ftc.getLog());
			o.setFileBase64(ftc.getFileBase64());
			o.setFileZip(ftc.getFileZip());
			o.setFileProcessId(ftc.getFileProcessId());
			o.setFileStatus(ftc.getFileStatus());
			session.update(o);
		} else {
			session.save(ftc);
		}		
	}

	@SuppressWarnings("unchecked")
	public List<InventoryTransaction> getInventoryTransaction(String branch, Date dateFrom) {
		Session session = this.sessionFactory.getCurrentSession();		
		
		Criteria criteria = session.createCriteria(InventoryTransaction.class);
		criteria.add(Restrictions.conjunction()
					.add(Restrictions.eq("inventoryOrganizationCode", branch))
					.add(Restrictions.ge("transactionDate", dateFrom))
					);
		criteria.addOrder(Order.asc("itemName"));
		criteria.addOrder(Order.desc("quantity"));
		return (List<InventoryTransaction>) criteria.list();
	}
	
	public void saveInventoryTransaction(InventoryTransaction it) {
		Session session = this.sessionFactory.getCurrentSession();		
		
		Criteria criteria = session.createCriteria(InventoryTransaction.class);
		criteria.add(Restrictions.conjunction()
					.add(Restrictions.eq("transactionId", it.getTransactionId())));
		
		InventoryTransaction dbIT = (InventoryTransaction) criteria.uniqueResult();		
		if(dbIT != null) {
			if(Double.compare(dbIT.getAverageCost(), it.getAverageCost()) != 0) {
				dbIT.setAverageCost(it.getAverageCost());
				dbIT.setLastUpdate(new Date());
				session.update(dbIT);
			}
		} else {
			session.save(it);
		}
	}
	
}

package com.tdo.integration.ws;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.AppStringUtils;

@Service("SOAPFileImportService")
public class SOAPFileImportService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	public boolean requestStatusMonitor(String response, StringBuilder message, StringBuilder status, StringBuilder transactionProcessId) {
		boolean statusOk = true;
		String processStatus = "";
		String statusResponse = "";
		int count = 0;
		
		try {
			if(response != null) {
				response = AppStringUtils.getEnvResponse(response);
				JSONObject xmlJSONObj1 = XML.toJSONObject(response, true);
				JsonElement jelement1 = new JsonParser().parse(xmlJSONObj1.toString());
				JsonObject jobject1 = jelement1.getAsJsonObject();
				JsonElement value1 = jobject1.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:importBulkDataResponse").getAsJsonObject();
				
				String processId = value1.getAsJsonObject().get("result").toString();
				processId = AppStringUtils.getContentFromEnv(processId);
				
				if(processId != null && !processId.isEmpty() && !processId.contains(AppConstants.XML_NULL_RESPONSE)){
					processId = processId.replaceAll("\"", "");			
					transactionProcessId.append(processId);
					message.append(", ProcessId: ").append(processId);					
				} else {
					statusOk = false;
					message.append(", ProcessId: No fue posible obtener el Process Id.");
				}
				
				while(statusOk && !AppConstants.CLOUD_REQUEST_SUCCEEDED.contentEquals(processStatus) && count < AppConstants.MAX_REQUEST_ATTEMPTS) {					
					statusResponse = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=",
							PayloadProducer.getProcessStatus(processId),
							AppConstants.CLOUD_CREDENTIALS);
					
					statusResponse = AppStringUtils.getEnvResponse(statusResponse);
					JSONObject xmlJSONObj2 = XML.toJSONObject(statusResponse, true);
					JsonElement jelement2 = new JsonParser().parse(xmlJSONObj2.toString());
					JsonObject jobject2 = jelement2.getAsJsonObject();
					JsonElement value2 = jobject2.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:getESSJobStatusResponse").getAsJsonObject();
					
					processStatus = value2.getAsJsonObject().get("result").toString();
					processStatus = AppStringUtils.getContentFromEnv(processStatus);
					
					if(processStatus.isEmpty() || processStatus.contains(AppConstants.XML_NULL_RESPONSE) || processStatus.contains(AppConstants.CLOUD_REQUEST_ERROR)){
						message.append(", Status Cloud: Status no válido para el Process Id: ").append(processId).append(". Status: ").append(processStatus);
						statusOk = false;
					}

					Thread.sleep(2000);
					count++;
				}
				
				if(statusOk) {
					message.append(", Status Cloud: ").append(processStatus).append(", Tiempo: ").append(String.valueOf(count*2));
				}								
			} else {
				statusOk = false;
			}
			
			status.append(processStatus);						
		} catch (Exception e) {
			e.printStackTrace();
			message.append("Exception: ").append(e.getMessage());
			statusOk = false;
		}
		
		return statusOk;
	}
	
	public boolean uploadARBulkFile(String content, String fileName, String ID, StringBuilder message, StringBuilder status, StringBuilder processId) {
		
		String response = null;
		try {
			
			response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getARBulkFileImportSOAPXmlContent(content, fileName, ID),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			message.append("Send Response: Ok");
		}catch(Exception e) {
			e.printStackTrace();
			message.append("Send Exception: " + e.getMessage());
			return false;
		}
		
		return requestStatusMonitor(response, message, status, processId);
	}
	
	
	public boolean uploadInvBulkFile(String content, String fileName, StringBuilder message) {
		
		try {
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getInvBulkFileImportSOAPXmlContent(content, fileName),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				message.append(response);
				return true;
			}else {
				message.append("null");
				return false;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			message.append(String.valueOf(e.getStackTrace()));
			return false;
		}

	}
	
	
	public boolean uploadGLBulkFile(String content, String fileName, String DATA_ACCESS_SET, String LEDGER_ID, StringBuilder message) {
		
		try {
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_BULKIMPORT_URL + "?invoke=", 
                                             PayloadProducer.getGLBulkFileImportSOAPXmlContent(content, fileName,  DATA_ACCESS_SET,  LEDGER_ID),
                                             AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				message.append(response);
				return true;
			}else {
				message.append("null");
				return false;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			message.append(String.valueOf(e.getStackTrace()));
			return false;
		}

	}
	
	
	
}

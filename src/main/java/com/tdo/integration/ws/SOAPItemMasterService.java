package com.tdo.integration.ws;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.pojo.Articulo;
import com.tdo.integration.pojo.ArticulosJson;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.NullValidator;

@Service("SOAPItemMasterService")
public class SOAPItemMasterService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	public List<ArticulosJson> getItems(String request) {
		
		List<ArticulosJson> itemList = new ArrayList<ArticulosJson>();
		
		try {
			String organizationCode = "";
			String value = "";
			String[] str = request.split("/");
			
			if(str.length != 2) {
				return null;
			}else {
				organizationCode = str[0];
				value = str[1];
			}
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_ITEMMASTER_URL + "?invoke=", 
					                                            PayloadProducer.getItemMasterSOAPXmlContent(value),
					                                            AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				itemList = getItemsFromJson(jelement);	
				if(!itemList.isEmpty()) {
					for(ArticulosJson c : itemList) {
						if(c.getArticulo().getPrecioCompra() <= 0) {
							setItemCost(c, organizationCode);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return itemList;
	}
	
	private void setItemCost(ArticulosJson c, String organizationCode) {
		try {
			
			String itemNumber = c.getArticulo().getClave();
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_ITEMCOST_URL + "?invoke=", 
	                PayloadProducer.getItemCostSOAPXmlContent(itemNumber, organizationCode),
	                AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				JsonObject jobject = jelement.getAsJsonObject();
				JsonElement result = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:retrieveItemCostResponse").getAsJsonObject().get("ns2:result").getAsJsonObject();
				
				try {
					JsonElement value = result.getAsJsonObject().get("ns1:Value").getAsJsonObject();					
			    	String costStr = NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemCost").toString());
			    	if("".equals(costStr)) {
			    			c.getArticulo().setPrecioCompra(0);
			    	}else {
			    		float lp= Float.valueOf(costStr);
			    		if(lp == -1) {
			    			c.getArticulo().setPrecioCompra(0);
			    		}else {
			    			c.getArticulo().setPrecioCompra(lp);
			    		}
			    	}
			    	
					//accNbr = value.getAsJsonObject().get("ns2:AccountNumber").toString();
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<ArticulosJson> getItemsFromJson(JsonElement jelement){
		JsonObject jobject = jelement.getAsJsonObject();
		JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findItemResponse");
		JsonElement bdy = soapEnvelope.getAsJsonObject().get("ns2:result").getAsJsonObject().get("ns0:Value");
		
		List<ArticulosJson> articulosJson = new ArrayList<ArticulosJson>();
		if(bdy != null) {

			if(bdy instanceof JsonArray) {
				JsonArray jsonarray = bdy.getAsJsonArray();
				for (int i = 0; i < jsonarray.size(); i++) {
					JsonElement op = jsonarray.get(i).getAsJsonObject();
					ArticulosJson c = getItemFromJSON(op);
					if(c != null) {
						articulosJson.add(c); 
					}
				}
			}else {
				ArticulosJson c = getItemFromJSON(bdy);
				articulosJson.add(c);	
			}
		}
		return articulosJson;
	}
	
	
	private ArticulosJson getItemFromJSON(JsonElement value) {
		ArticulosJson articulos = new ArticulosJson();
		 try {	    	   
			    
			    //JsonElement value = root.getAsJsonObject().get("ns0:Value");
		    	Articulo art = new Articulo();
		    	String taxType = NullValidator.isNull(value.getAsJsonObject().get("ns1:OutputTaxClassificationCodeValue").toString());
		    	if("".equals(taxType)) {
		    		articulos.setIncluyeImpuestos(false);
		    	}else {
		    		articulos.setIncluyeImpuestos(true);
		    		switch(taxType) {
					   case "IVA_16_AR" :
					    	art.setImpuestosId(new String[]{"4"}); 
						   	break;
					   case "IVA_CERO" :
					    	art.setImpuestosId(new String[]{"8"}); 
						   	break;
					   case "IEPS_6_AR" :
					    	art.setImpuestosId(new String[]{"3", "8"}); 
						   	break;
					   case "IEPS_7_AR" :
					    	art.setImpuestosId(new String[]{"7", "8"}); 
						   	break;
					   case "IEPS_9_AR" :
					    	art.setImpuestosId(new String[]{"5", "8"}); 
						   	break;
					   case "IVA_EXENTO" :
					    	art.setImpuestosId(new String[]{"2"}); 
						   	break;
					   default :
					        break;
					}
		    	}
		    	
		    	
		    	art.setId(0);
		    	art.setClave(NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemNumber").toString()));
		    	art.setClaveAlterna(NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemId").toString()));
		    	art.setDescripcion(NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemDescription").toString()));
		    	
		    	if("Servicios".equals(NullValidator.isNull(value.getAsJsonObject().get("ns1:UserItemTypeValue").toString())))
		    		art.setServicio(true);
		    	else
		    		art.setServicio(false);
		    	
		    	art.setLocalizacion("");
		    	art.setInvMin(0);
		    	art.setInvMax(0);
		    	art.setFactor(1); 
		    	art.setPrecioCompra(0); 
		    	
		    	String listPrice = NullValidator.isNull(value.getAsJsonObject().get("ns1:ListPrice").toString());
		    	if("".equals(listPrice)) {
		    		art.setPrecioCompra(0);
		    	}else {
			    	JsonElement lpElement = value.getAsJsonObject().get("ns1:ListPrice").getAsJsonObject();
			    	String lpContent = NullValidator.isNull(lpElement.getAsJsonObject().get("content").toString());
		    		float lp= Float.valueOf(lpContent);
		    		art.setPrecioCompra(lp);
		    	}
		    	
	    		art.setPrecio1(0);
		    	art.setPrecio2(0); 
		    	art.setPrecio3(0); 
		    	art.setPrecio4(0); 
		    	art.setMayoreo2(0); 
		    	art.setMayoreo3(0); 
		    	art.setMayoreo4(0);
		    	art.setCaracteristicas("");
		    	art.setReceta(false);
		    	art.setGranel(false);
		    	art.setPeso(0);
		    	art.setPesoAut(false);
		    	
		    	String ff = NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemDFF").toString());
		    	if("".equals(ff)) {
		    		art.setClaveProdServ("");
		    	}else {
		    		JsonElement ffElement = value.getAsJsonObject().get("ns1:ItemDFF").getAsJsonObject();
		    		
		    		try {
		    			art.setClaveProdServ(NullValidator.isNull(ffElement.getAsJsonObject().get("ns7:claveProdSerSat").toString()));
		    		}catch(Exception e) {
		    			art.setClaveProdServ("");
		    		}
		    		
		    		try {
		    			art.setCaracteristicas(NullValidator.isNull(ffElement.getAsJsonObject().get("ns7:moneda").toString()));
		    		}catch(Exception e) {
		    			art.setCaracteristicas("");
		    		}
		    	}
		    	
		    	String uom = NullValidator.isNull(value.getAsJsonObject().get("ns1:PrimaryUOMValue").toString());
		    	
		    	switch(uom.trim()) {
				   case "PIEZA" :
				    	art.setUnidadCompraId(1);
				    	art.setUnidadVentaId(1);
					   	break;
				   case "PZ" :
				    	art.setUnidadCompraId(1);
				    	art.setUnidadVentaId(1);
					   	break;
				   case "LITRO" :
					    art.setUnidadCompraId(5);
				    	art.setUnidadVentaId(5);
					   	break;
				   case "LT" :
					    art.setUnidadCompraId(5);
				    	art.setUnidadVentaId(5);
					   	break;
				   case "KILO" :
					    art.setUnidadCompraId(4);
				    	art.setUnidadVentaId(4);
					   	break;					   	
				   case "KG" :
					    art.setUnidadCompraId(4);
				    	art.setUnidadVentaId(4);
					   	break;
				   case "MT" :
					    art.setUnidadCompraId(3);
				    	art.setUnidadVentaId(3);
					   	break;
				   case "METRO" :
					    art.setUnidadCompraId(3);
				    	art.setUnidadVentaId(3);
					   	break;
				   case "METRO2" :
					    art.setUnidadCompraId(6);
				    	art.setUnidadVentaId(6);
					   	break;					   	
				   case "MT2" :
					    art.setUnidadCompraId(6);
				    	art.setUnidadVentaId(6);
					   	break;
				   case "CAJA" :
					    art.setUnidadCompraId(2);
				    	art.setUnidadVentaId(2);
					   	break;
				   case "SER" :
					    art.setUnidadCompraId(6);
				    	art.setUnidadVentaId(6);
					   	break;
				   default :
					    art.setUnidadCompraId(1);
				    	art.setUnidadVentaId(1);
				        break;
		    	}
				        			
		    	art.setCategoriaId(1); 
		    	articulos.setArticulo(art);
		    	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		 
   	  return articulos;
	}

}

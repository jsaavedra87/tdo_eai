package com.tdo.integration.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.util.AppConstants;

public class PayloadProducer {
	
	static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	public static String getProcessStatus(String statusId) {
		
		String SOAPRequest = 	"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\">" + 
								"   <soapenv:Header/>" + 
								"   <soapenv:Body>" + 
								"      <typ:getESSJobStatus>" + 
								"         <typ:requestId>" + statusId +"</typ:requestId>" + 
								"      </typ:getESSJobStatus>" + 
								"   </soapenv:Body>" + 
								"</soapenv:Envelope>";
		
		return SOAPRequest;
	}
	
	public static String getSalesPartySOAPXmlContent(String request, int customerFetchStart) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"<ns2:findSalesParty xmlns=\"http://xmlns.oracle.com/adf/svc/types/\" xmlns:ns2=\"http://xmlns.oracle.com/apps/crmCommon/salesParties/salesPartiesService/types/\"> " +
				" <ns2:findCriteria> " + 
				" <fetchStart>" + customerFetchStart + "</fetchStart> " + 
				" <fetchSize>" + AppConstants.PAYLOAD_CUSTOMER_FETCH_SIZE +"</fetchSize> " + 
				" <filter> " + 
				" <group> " + 
				//" <item> " + 
				//" <attribute>PartyNumber</attribute> " + 
				//" <operator>=</operator> " + 
				//" <value>" + request + "</value> " +
				//" </item> " + 
				" <item> " +
		        " <attribute>OrganizationParty</attribute> " +
                " <nested> " +
                "   <group> " +
                "       <item> " +
                "           <attribute>CreationDate</attribute> " +
                "           <operator>ONORAFTER</operator> " +
                "           <value>" + request + "</value> " +
                "       </item> " +
                "   </group> " +
                " </nested> " +
                " </item> " +
				" </group> " + 
				" </filter> " + 
				" <findAttribute>PartyId</findAttribute> " + 
				" <findAttribute>PartyName</findAttribute> " + 
				" <findAttribute>PartyNumber</findAttribute> " + 
				" <findAttribute>PartyType</findAttribute> " + 
				" <findAttribute>Status</findAttribute> " + 
				" <findAttribute>OrganizationParty</findAttribute> " + 
				" <childFindCriteria> " + 
				" <findAttribute>CreatedBy</findAttribute> " + 
				" <findAttribute>CreationDate</findAttribute> " + 
				" <findAttribute>JgzzFiscalCode</findAttribute> " + 
				" <childAttrName>OrganizationParty</childAttrName> " + 
				" <findAttribute>PartySite</findAttribute> " + 
				" <findAttribute>OrganizationProfile</findAttribute> " + 
				" <childFindCriteria> " + 
				" <findAttribute>Address1</findAttribute> " + 
				" <findAttribute>Address2</findAttribute> " + 
				" <findAttribute>Address3</findAttribute> " + 
				" <findAttribute>City</findAttribute> " + 
				" <findAttribute>Country</findAttribute> " + 
				" <findAttribute>PostalCode</findAttribute> " + 
				" <findAttribute>PartySiteNumber</findAttribute> " + 
				" <findAttribute>State</findAttribute> " + 
				" <childAttrName>PartySite</childAttrName> " + 
				" </childFindCriteria> " + 
				" <childFindCriteria> " + 
				" <findAttribute>PreferredContactName</findAttribute> " + 
				" <findAttribute>JgzzFiscalCode</findAttribute> " + 
				" <findAttribute>CurrencyCode</findAttribute> " + 
				" <findAttribute>PrimaryPhoneNumber</findAttribute> " + 
				" <findAttribute>PrimaryEmailAddress</findAttribute> " + 
				" <childAttrName>OrganizationProfile</childAttrName> " + 
				" </childFindCriteria> " + 
				" </childFindCriteria> " + 
				" </ns2:findCriteria> " + 
				" </ns2:findSalesParty> " +
				" </soapenv:Body> " + 
				" </soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerProfileSOAPXmlContent(String partyId, String accountNbr){
		partyId = partyId.replace("\"", "");	
		accountNbr = accountNbr.replace("\"", "");
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\" xmlns:cus1=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileDff/\" xmlns:cus2=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileGdf/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body>" + 
				"   <typ:getActiveCustomerProfile xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\"> " +
			    "    <typ:customerProfile> " +
			    "       <cus:AccountNumber>" + accountNbr + "</cus:AccountNumber> " +
			    "       <cus:PartyId>" + partyId + "</cus:PartyId> " +
			    "    </typ:customerProfile> " +
			    " </typ:getActiveCustomerProfile>" +
				" </soapenv:Body> " + 
				" </soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerAccountSOAPXmlContent(String sessionId, String organizationName){		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\">\r\n" + 
				"    <soapenv:Header />\r\n" + 
				"    <soapenv:Body>\r\n" + 
				"        <v7:executeSQLQuery>\r\n" + 
				"            <v7:sql><![CDATA[SELECT\r\n" + 
				"		0 s_0,\r\n" + 
				"		\"Receivables - Customer Real Time\".\"Customer Account Details\".\"Customer Account Number\" s_1,\r\n" + 
				"		\"Receivables - Customer Real Time\".\"Organization\".\"Organization Name\" s_2,\r\n" + 
				"		\"Receivables - Customer Real Time\".\"Site Address\".\"Site Number\" s_3\r\n" + 
				"		FROM \"Receivables - Customer Real Time\"\r\n" + 
				"		WHERE (\"Organization\".\"Organization Name\" IN ('" + organizationName + "'))\r\n" + 
				"		ORDER BY 1, 3 ASC NULLS LAST, 2 ASC NULLS LAST, 4 ASC NULLS LAST]]></v7:sql>\r\n" + 
				"            <v7:outputFormat>XML</v7:outputFormat>\r\n" + 
				"            <v7:executionOptions>\r\n" + 
				"                <v7:async></v7:async>\r\n" + 
				"                <v7:maxRowsPerPage></v7:maxRowsPerPage>\r\n" + 
				"                <v7:refresh></v7:refresh>\r\n" + 
				"                <v7:presentationInfo></v7:presentationInfo>\r\n" + 
				"                <v7:type></v7:type>\r\n" + 
				"            </v7:executionOptions>\r\n" + 
				"            <v7:sessionID>" + sessionId + "</v7:sessionID>\r\n" + 
				"        </v7:executeSQLQuery>\r\n" + 
				"    </soapenv:Body>\r\n" + 
				"</soapenv:Envelope>";
		
		return SOAPRequest;
	}
	
	public static String getCustomerAccountOldSOAPXmlContent(String partyId){
		partyId = partyId.replace("\"", "");		
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findCustomerAccount> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:filter> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                     <typ1:attribute>PartyId</typ1:attribute> " + 
				"                     <typ1:operator>=</typ1:operator> " + 
				"                     <typ1:value> " + partyId + "</typ1:value> " + 
				"                  </typ1:item> " + 
				"               </typ1:group> " + 
				"            </typ1:filter> " + 
				"            <typ1:findAttribute>AccountNumber</typ1:findAttribute> " + 
				"         </typ:findCriteria> " + 
				"      </typ:findCustomerAccount> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	
	
	public static String getItemMasterSOAPXmlContent(String request){
		
		request = request.replace("\"", "");
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" > " + 
				"<soapenv:Header/> " + 
				"<soapenv:Body>  " +
				"<ns2:findItem xmlns:ns2=\"http://xmlns.oracle.com/apps/scm/productModel/items/itemServiceV2/types/\" xmlns=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"<ns2:findCriteria> " + 
				"<fetchStart>0</fetchStart> " + 
				"<fetchSize>100</fetchSize> " + 
				"<filter> " + 
				"<group> " + 
				//"<item> " + 
				//"<conjunction>And</conjunction> " + 
				//"<attribute>ItemNumber</attribute> " + 
				//"<operator>=</operator> " + 
				//"<value>" + request +"</value> " + 
				//"</item> " + 
				"<item> " + 
				"<conjunction>And</conjunction> " + 
				"<attribute>CreationDate</attribute> " + 
				"<operator>ONORAFTER</operator> " + 
				"<value>" + request + "</value> " + 
				"</item> " + 
				"<item> " + 
				"<conjunction>And</conjunction> " + 
				"<attribute>OrganizationCode</attribute> " + 
				"<operator>=</operator> " + 
				"<value>TDO</value> " + 
				"</item> " + 
				"</group> " + 
				"<nested/> " + 
				"</filter> " + 
				"<findAttribute>ItemId</findAttribute> " + 
				"<findAttribute>ItemNumber</findAttribute> " + 
				"<findAttribute>OrganizationCode</findAttribute> " + 
				"<findAttribute>ItemDescription</findAttribute> " + 
				"<findAttribute>UserItemTypeValue</findAttribute> " + 
				"<findAttribute>ListPrice</findAttribute> " + 
				"<findAttribute>LotControlValue</findAttribute> " + 
				"<findAttribute>OutputTaxClassificationCodeValue</findAttribute> " +
				"<findAttribute>PrimaryUOMValue</findAttribute> " + 
				"<findAttribute>CreationDate</findAttribute> " +
				"<findAttribute>ItemDFF</findAttribute> " +
				"<childFindCriteria> " +
				"     <findAttribute>moneda</findAttribute> " +
				"     <findAttribute>claveProdSerSat</findAttribute> " +
				"     <childAttrName>ItemDFF</childAttrName> " +
				"</childFindCriteria> " +
				"</ns2:findCriteria> " + 
				"</ns2:findItem> " +
				"</soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getItemCostSOAPXmlContent(String itemNumber, String organizationCode){

		itemNumber = itemNumber.replace("\"", "");
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/types/\" xmlns:item=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:retrieveItemCost> " + 
				"         <typ:costparams> " + 
				"               <item:ItemNumber>"+ itemNumber + "</item:ItemNumber> " + 
				"               <item:InventoryOrganizationCode>" + organizationCode + "</item:InventoryOrganizationCode> " + 
				"               <item:CurrencyCode>MXN</item:CurrencyCode> " + 
				"            </typ:costparams> " + 
				"      </typ:retrieveItemCost> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getLocationSOAPXmlContent(CustomerDTO c) {

		String SOAPRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> " + 
				"<soap:Body xmlns:ns1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/applicationModule/types/\"> " + 
				"<ns1:createLocation> " + 
				"<ns1:location xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/\"> " + 
				"<ns2:Country>" + c.getCountry() + "</ns2:Country> " + 
				"<ns2:Address1>" + c.getAddress1() + "</ns2:Address1> " + 
				"<ns2:City>" + c.getCity() + "</ns2:City> " + 
				"<ns2:PostalCode>" + c.getPostalCode() + "</ns2:PostalCode> " + 
				"<ns2:State>"+ c.getState() +"</ns2:State> " + 
				"<ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"</ns1:location> " + 
				"</ns1:createLocation> " + 
				"</soap:Body> " + 
				"</soap:Envelope> ";
		return SOAPRequest;
	}

	public static String getCustomerOrganizationSOAPXmlContent(CustomerDTO c, String locationId) {
		
		String SOAPRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> " + 
				"   <soap:Body xmlns:ns1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\"> " + 
				"      <ns1:createOrganization> " + 
				"         <ns1:organizationParty xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/\"> " + 
				"            <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"            <ns2:PartyUsageAssignment xmlns:ns3=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\"> " + 
				"               <ns3:PartyUsageCode>CUSTOMER</ns3:PartyUsageCode> " + 
				"               <ns3:CreatedByModule>AMS</ns3:CreatedByModule> " + 
				"            </ns2:PartyUsageAssignment> " + 
				"            <ns2:PartyUsageAssignment xmlns:ns3=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\"> " + 
				"               <ns3:PartyUsageCode>EXTERNAL_LEGAL_ENTITY</ns3:PartyUsageCode> " + 
				"               <ns3:CreatedByModule>AMS</ns3:CreatedByModule> " + 
				"            </ns2:PartyUsageAssignment> " + 
				"            <ns2:OrganizationProfile> " + 
				"               <ns2:OrganizationName>" + c.getName() + "</ns2:OrganizationName> " + 
				"               <ns2:JgzzFiscalCode>" + c.getRfc() +"</ns2:JgzzFiscalCode> " + 
				"               <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"            </ns2:OrganizationProfile> " + 
				"            <ns2:PartySite xmlns:ns28=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\"> " + 
				"               <ns28:LocationId>" + locationId +"</ns28:LocationId> " + 
				"               <ns28:Mailstop>Empty</ns28:Mailstop> " + 
				"               <ns28:IdentifyingAddressFlag>true</ns28:IdentifyingAddressFlag> " + 
				"               <ns28:PartySiteName>" + c.getName() +"</ns28:PartySiteName> " + 
				"               <ns28:Addressee>SiteName Address</ns28:Addressee> " + 
				"               <ns28:CreatedByModule>AMS</ns28:CreatedByModule> " + 
				"               <ns28:Comments>Created by interface</ns28:Comments> " + 
				"               <ns28:CurrencyCode>USD</ns28:CurrencyCode> " + 
				"               <ns28:CorpCurrencyCode>USD</ns28:CorpCurrencyCode> " + 
				"               <ns28:CurcyConvRateType>Corporate</ns28:CurcyConvRateType> " + 
				"               <ns28:PartySiteUse> " + 
				"                  <ns28:Comments>Created by interface</ns28:Comments> " + 
				"                  <ns28:SiteUseType>BILL_TO</ns28:SiteUseType> " + 
				"                  <ns28:CreatedByModule>AMS</ns28:CreatedByModule> " + 
				"               </ns28:PartySiteUse> " + 
				"            </ns2:PartySite> " + 
//				"            <ns2:Relationship xmlns:ns14=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/relationshipService/\"> " + 
//				"                <ns14:SubjectType>ORGANIZATION</ns14:SubjectType> " + 
//				"                <ns14:SubjectTableName>HZ_PARTIES</ns14:SubjectTableName> " + 
//				"                <ns14:ObjectType>PERSON</ns14:ObjectType> " + 
//				"                <ns14:ObjectId>" + AppConstants.DEFAULT_CONTACT_ID + "</ns14:ObjectId> " + 
//				"                <ns14:ObjectTableName>HZ_PARTIES</ns14:ObjectTableName> " + 
//				"                <ns14:RelationshipCode>EMPLOYER_OF</ns14:RelationshipCode> " + 
//				"                <ns14:RelationshipType>EMPLOYMENT</ns14:RelationshipType> " + 
//				"                <ns14:Comments>Created by interface</ns14:Comments> " + 
//				"                <ns14:CurrencyCode>USD</ns14:CurrencyCode> " + 
//				"                <ns14:CurcyConvRateType>Corporate</ns14:CurcyConvRateType> " + 
//				"                <ns14:CorpCurrencyCode>USD</ns14:CorpCurrencyCode> " + 
//				"                <ns14:CreatedByModule>AMS</ns14:CreatedByModule>  " + 
//				"            </ns2:Relationship> " + 
				"         </ns1:organizationParty> " + 
				"      </ns1:createOrganization> " + 
				"   </soap:Body> " + 
				"</soap:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerAccountSOAPXmlContent(CustomerDTO c, String partyId, String partySiteId, String partyNumber, String setId) {

		String SOAPRequest = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> " + 
				"   <soap:Body xmlns:ns1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/applicationModule/types/\"> " + 
				"      <ns1:createCustomerAccount> " + 
				"         <ns1:customerAccount xmlns:ns2=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/customerAccountService/\"> " + 
				"            <ns2:PartyId>" + partyId +"</ns2:PartyId> " + 
				"            <ns2:AccountNumber>" + partyNumber + "AC" + "</ns2:AccountNumber> " + 
				"            <ns2:Status>A</ns2:Status> " + 
				"            <ns2:CustomerType>R</ns2:CustomerType> " +
//				"            <ns2:CustomerClassCode>PRIVATE</ns2:CustomerClassCode> " +
				"            <ns2:AccountName>" + c.getName() +"</ns2:AccountName> " + 
				"            <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"            <ns2:CustomerAccountSite> " + 
				"               <ns2:PartySiteId>" + partySiteId +"</ns2:PartySiteId> " + 
				"               <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"               <ns2:SetId>" + setId +"</ns2:SetId> " + 
				"               <ns2:CustomerAccountSiteUse> " + 
				"                  <ns2:SiteUseCode>BILL_TO</ns2:SiteUseCode> " + 
				"                  <ns2:CreatedByModule>AMS</ns2:CreatedByModule> " + 
				"               </ns2:CustomerAccountSiteUse> " + 
				"            </ns2:CustomerAccountSite> " + 
				"         </ns1:customerAccount> " + 
				"      </ns1:createCustomerAccount> " + 
				"   </soap:Body> " + 
				"</soap:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getCustomerProfileServiceSOAPXmlContent(String partyId, String customerAccountId) {

		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/types/\" xmlns:cus=\"http://xmlns.oracle.com/apps/financials/receivables/customers/customerProfileService/\" xmlns:cus1=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileDff/\" xmlns:cus2=\"http://xmlns.oracle.com/apps/financials/receivables/customerSetup/customerProfiles/model/flex/CustomerProfileGdf/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:createCustomerProfile> " + 
				"         <typ:customerProfile> " + 
				"            <cus:PartyId>" + partyId +"</cus:PartyId> " + 
				"            <cus:CustomerAccountId>" + customerAccountId +"</cus:CustomerAccountId> " + 
				"        </typ:customerProfile> " + 
				"      </typ:createCustomerProfile> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope> ";
		return SOAPRequest;
	}
	
	public static String getSessionIdSOAPXmlContent() {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:logon> " + 
				"         <v7:name>INTEGUSER</v7:name> " + 
				"         <v7:password>Smartech2019$</v7:password> " + 
				"      </v7:logon> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String logOffSessionIdSOAPXmlContent(String sessionId) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:logoff> " + 
				"         <v7:sessionID>" + sessionId +"</v7:sessionID> " + 
				"      </v7:logoff> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	
	@SuppressWarnings("rawtypes")
	public static String getItemCostSOAPXmlContent(Map<String, InventoryTransactionDTO> invMap, String defaultOrganization) {
		
		if(invMap.size() > 0) {
			String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/types/\" xmlns:item=\"http://xmlns.oracle.com/apps/scm/costing/itemCosts/itemCostServiceV2/\"> " + 
					"   <soapenv:Header/> " + 
					"   <soapenv:Body> " + 
					"      <typ:retrieveItemCost> ";
					
			Iterator entries = invMap.entrySet().iterator();
			while (entries.hasNext()) {
			    Map.Entry entry = (Map.Entry) entries.next();
			    InventoryTransactionDTO o = (InventoryTransactionDTO)entry.getValue();
			    SOAPRequest = SOAPRequest + " <typ:costparams> " + 
						"  <item:ItemNumber>" + o.getItemName() +"</item:ItemNumber> " + 
						"  <item:InventoryOrganizationCode>" + defaultOrganization + "</item:InventoryOrganizationCode> " + 
						"  <item:CurrencyCode>MXN</item:CurrencyCode> " +
						" </typ:costparams> "; 
			}

			SOAPRequest = SOAPRequest + "</typ:retrieveItemCost> " + 
			"   </soapenv:Body> " + 
			"</soapenv:Envelope>";
			
			return SOAPRequest;
		}else {
			return null;
		}
		
	}
	
	public static String getInventoryTransactionTransferSOAPXmlContent(String sessionId, String branch, String dateFrom, String dateTo) {
		String branchCondition = "";
		String dateFromCondition = "";
		String dateToCondition = "";
		
		if(branch != null && branch.length() > 0) {
			branchCondition = " (\"Inventory Organization\".\"Inventory Organization Code\" = '" + branch + "') AND ";
		}
		
		if(dateFrom != null && dateFrom.length() > 0) {
			dateFromCondition = " (\"Inventory Transactions Details\".\"Transaction Date\" >= timestamp '" + dateFrom + "') AND ";
		}
		
		if(dateTo != null && dateTo.length() > 0) {
			dateToCondition = " (\"Inventory Transactions Details\".\"Transaction Date\" <= timestamp '" + dateTo + "') AND ";
		}		 
				
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:executeSQLQuery> " + 
				"         <v7:sql><![CDATA[SELECT " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"- Main\".\"Item Name\" s_0, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"- Main\".\"Item Description\" s_1, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"- Main\".\"Item Primary Unit of Measure\" s_2, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Organization\".\"Inventory Organization Code\" s_3, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Organization\".\"Inventory Organization Name\" s_4, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Transactions Details\".\"Transaction Date\" s_5, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Locator\".\"Locator Inventory Location\" s_6, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Lot\".\"Creation Date\" s_7, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Lot\".\"Expiration Date\" s_8, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Lot\".\"Lot Number\" s_9, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Subinventory\".\"Subinventory Name\" s_10, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Transaction Type\".\"Transaction Type\" s_11, " + 
				"   \"Inventory - Inventory Transactions Real Time\".\"Inventory Transactions\".\"Primary Quantity\" s_12," +
				"   \"Inventory - Inventory Balance Real Time\".\"Category\".\"Category Name\" s_13, " +
				"	\"Inventory - Inventory Transactions Real Time\".\"Inventory Transactions Details\".\"Transaction Id\" s_14, " +
				"	\"Inventory - Inventory Transactions Real Time\".\"Time of Transaction Date\".\"Date\" s_15 " +
				"FROM \"Inventory - Inventory Transactions Real Time\" " + 
				"WHERE (" + branchCondition + dateFromCondition + dateToCondition + 
				" (\"Catalog\".\"Catalog Name\" = 'MONEDA') AND " +
				" (\"Transaction Type\".\"Transaction Type\" IN ('in', 'Intransit Receipt', 'Intransit Shipment', 'Direct Organization Transfer', 'Subinventory Transfer', "
				+ " 'Purchase Order Receipt', 'Miscellaneous issue', 'Miscellaneous Receipt', 'Transfer to Owned', 'Cycle Count Adjustment', "
				+ " 'Purchase Order Receipt Adjustment', 'Return to Supplier','Physical Inventory Adjustment'))) " +							
				" ORDER BY 6 ASC " + 
				" FETCH FIRST 5001 ROWS ONLY]]></v7:sql> " + 
				"         <v7:outputFormat>XML</v7:outputFormat> " + 
				"         <v7:executionOptions> " + 
				"            <v7:async></v7:async> " + 
				"            <v7:maxRowsPerPage></v7:maxRowsPerPage> " + 
				"            <v7:refresh></v7:refresh> " + 
				"            <v7:presentationInfo></v7:presentationInfo> " + 
				"            <v7:type></v7:type> " + 
				"         </v7:executionOptions> " + 
				"         <v7:sessionID>" + sessionId + "</v7:sessionID> " + 
				"      </v7:executeSQLQuery> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	
	public static String findOrganizationByFCSOAPXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\">" + 
				"   <soapenv:Header/>" + 
				"   <soapenv:Body>" + 
				"      <typ:findOrganization>" + 
				"         <typ:findCriteria>" + 
				"            <typ1:fetchStart>0</typ1:fetchStart>" + 
				"            <typ1:fetchSize>-1</typ1:fetchSize>" + 
				"            <typ1:filter>" + 
				"               <typ1:group>" + 
				"                  <typ1:item>" + 
				"                     <typ1:attribute>JgzzFiscalCode</typ1:attribute>" + 
				"                     <typ1:operator>=</typ1:operator>" + 
				"                     <typ1:value>" + request + "</typ1:value>" + 
				"                  </typ1:item>" + 
				"               </typ1:group>" + 
				"            </typ1:filter>" + 
				"            <typ1:findAttribute>PartyName</typ1:findAttribute>" + 
				"         </typ:findCriteria>" + 
				"      </typ:findOrganization>" + 
				"   </soapenv:Body>" + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	
	public static String findOrganizationByFiscalCodeSOAPXmlContent(String request) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/organizationService/applicationModule/types/\" xmlns:typ1=\"http://xmlns.oracle.com/adf/svc/types/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:findOrganization> " + 
				"         <typ:findCriteria> " + 
				"            <typ1:fetchStart>0</typ1:fetchStart> " + 
				"            <typ1:fetchSize>-1</typ1:fetchSize> " + 
				"            <typ1:filter> " + 
				"               <typ1:group> " + 
				"                  <typ1:item> " + 
				"                     <typ1:attribute>PartyName</typ1:attribute> " + 
				"                     <typ1:operator>=</typ1:operator> " + 
				"                     <typ1:value>" + request +"</typ1:value> " + 
				"                  </typ1:item> " + 
				"               </typ1:group> " + 
				"            </typ1:filter> " + 
				"			<typ1:findAttribute>PartyId</typ1:findAttribute> " + 
				"			<typ1:findAttribute>PartySite</typ1:findAttribute> " + 
				"			<typ1:childFindCriteria> " + 
				"			      <typ1:childAttrName>PartySite</typ1:childAttrName> " + 
				"			      <typ1:findAttribute>LocationId</typ1:findAttribute> " + 
				"			</typ1:childFindCriteria> " + 
				"         </typ:findCriteria> " + 
				"      </typ:findOrganization> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	public static String findPurchaseAverageCostSOAPXmlContent(String request, String sessionId, String organizationCode) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\">\r\n" + 
				"    <soapenv:Header />\r\n" + 
				"    <soapenv:Body>\r\n" + 
				"        <v7:executeSQLQuery>\r\n" + 
				"            <v7:sql><![CDATA[SELECT saw_0, saw_1, saw_2, sum(saw_3) saw_3, sum(saw_4) saw_4\r\n" + 
				"			FROM (\r\n" + 
				"			SELECT \"inventory organization\".\"Inventory Organization Code\"		saw_0, \r\n" + 
				"							\"- main\".\"Item Name\"								saw_1, \r\n" + 
				"							\"lot\".\"Lot Number\"									saw_2, \r\n" + 
				"							\"current on-hand inventory\".\"Available Quantity\"	saw_3, \r\n" + 
				"							0                                                   	saw_4,\r\n" + 
				"							0														saw_5\r\n" + 
				"					 FROM   \"inventory - inventory balance real time\" \r\n" + 
				"					 WHERE  ( \"catalog\".\"Catalog Code\" = 'MONEDA' ) \r\n" + 
				"							AND ( \"category\".\"Category Name\" = 'USD' ) \r\n" + 
				"							AND ( \"- main\".\"Item Name\" = '" + request + "' ) \r\n" + 
				"							AND ( \"inventory organization\".\"Inventory Organization Code\" = '" + organizationCode + "') \r\n" + 
				"							AND ( \"current on-hand inventory\".\"Available Quantity\" > 0 )\r\n" + 
				"			UNION\r\n" + 
				"			SELECT \"inventory organization\".\"Inventory Organization Code\"		saw_0, \r\n" + 
				"							\"-  main\".\"Item Name\"								saw_1, \r\n" + 
				"							\"cost transaction details\".\"Lot Number\"				saw_2, \r\n" + 
				"							0														saw_3, \r\n" + 
				"							\"- purchase order\".\"Price\"							saw_4,\r\n" + 
				"							\"Cost Transaction Details\".\"Inv Transaction Id\"		saw_5\r\n" + 
				"					 FROM   \"costing - cost accounting real time\" \r\n" + 
				"					 WHERE  ( \"Cost Transaction Details\".\"Inv Transaction Id\" IN (\r\n" + 
				"			SELECT saw_5 FROM (\r\n" + 
				"			SELECT saw_1, saw_2, saw_3, MAX(saw_5) saw_5 FROM (\r\n" + 
				"			SELECT \"inventory organization\".\"Inventory Organization Code\"		saw_0, \r\n" + 
				"							\"-  main\".\"Item Name\"								saw_1, \r\n" + 
				"							\"cost transaction details\".\"Lot Number\"				saw_2, \r\n" + 
				"							\"work order\".\"Quantity Scrapped\"					saw_3, \r\n" + 
				"							\"- purchase order\".\"Price\"							saw_4,\r\n" + 
				"							\"Cost Transaction Details\".\"Inv Transaction Id\"		saw_5\r\n" + 
				"					 FROM   \"costing - cost accounting real time\" \r\n" + 
				"					 WHERE  ( \"-  main\".\"Item Name\" = '" + request + "' ) \r\n" + 
				"							AND ( \"inventory organization\".\"Inventory Organization Code\" = '" + organizationCode + "' ) \r\n" + 
				"							AND ( \"- purchase order\".\"Currency Code\" = 'USD' )\r\n" + 
				"			) T \r\n" + 
				"			GROUP BY saw_1, saw_2, saw_3\r\n" + 
				"			ORDER BY saw_2 DESC\r\n" + 
				"			) T1))) T2 \r\n" + 
				"			GROUP BY saw_0, saw_1, saw_2\r\n" + 
				"			HAVING (sum(saw_3) > 0) AND (sum(saw_4) > 0) ]]></v7:sql>\r\n" + 
				"            <v7:outputFormat>XML</v7:outputFormat>\r\n" + 
				"            <v7:executionOptions>\r\n" + 
				"                <v7:async />\r\n" + 
				"                <v7:maxRowsPerPage />\r\n" + 
				"                <v7:refresh />\r\n" + 
				"                <v7:presentationInfo />\r\n" + 
				"                <v7:type />\r\n" + 
				"            </v7:executionOptions>\r\n" + 
				"            <v7:sessionID>" + sessionId + "</v7:sessionID>\r\n" + 
				"        </v7:executeSQLQuery>\r\n" + 
				"    </soapenv:Body>\r\n" + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
		
	public static String findPurchaseAverageCostSOAPXmlContentOld(String request, String sessionId, String organizationCode) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:executeSQLQuery> " + 
				"         <v7:sql><![CDATA[SELECT saw_0, saw_1, saw_2, sum(saw_3), sum(saw_4) " + 
				" FROM ((SELECT \"Inventory Organization\".\"Inventory Organization Code\" saw_0,  " + 
				" \"- Main\".\"Item Name\" saw_1,  " + 
				" \"Lot\".\"Lot Number\" saw_2,  " + 
				" \"Current On-Hand Inventory\".\"Available Quantity\" saw_3,  " + 
				" \"Item Subinventory\".\"Processing Number of Days\" saw_4 " + 
				" FROM \"Inventory - Inventory Balance Real Time\" " + 
				" WHERE (\"Catalog\".\"Catalog Code\" = 'MONEDA')  " +
				" AND (\"Category\".\"Category Name\" = 'USD') " +
				" AND (\"- Main\".\"Item Name\" = '" + request + "') " + 
				" AND (\"Inventory Organization\".\"Inventory Organization Code\" = '" + organizationCode + "') " +
				" AND ( \"Current On-Hand Inventory\".\"Available Quantity\" > 0))  " + 
				" UNION (SELECT \"Inventory Organization\".\"Inventory Organization Code\" saw_0,  " + 
				"               \"-  Main\".\"Item Name\" saw_1,  " + 
				"               \"Cost Transaction Details\".\"Lot Number\" saw_2,  " + 
				"               \"Work Order\".\"Quantity Scrapped\" saw_3,  " + 
				"               \"- Purchase Order\".\"Price\" saw_4 " + 
				"                FROM \"Costing - Cost Accounting Real Time\" " + 
				"                WHERE (\"-  Main\".\"Item Name\" = '" + request + "')" +
			    "       AND (\"Inventory Organization\".\"Inventory Organization Code\" = '" + organizationCode+ "')" +
				"       AND (\"- Purchase Order\".\"Currency Code\" = 'USD') )) t1 " + 
				" GROUP BY saw_0, saw_1, saw_2 " + 
				" HAVING (sum(saw_3) > 0)  " + 
				" ORDER BY saw_0, saw_1, saw_2]]></v7:sql> " + 
				"         <v7:outputFormat>XML</v7:outputFormat> " + 
				"         <v7:executionOptions> " + 
				"            <v7:async/> " + 
				"            <v7:maxRowsPerPage/> " + 
				"            <v7:refresh/> " + 
				"            <v7:presentationInfo/> " + 
				"            <v7:type/> " + 
				"         </v7:executionOptions> " + 
				"         <v7:sessionID>" + sessionId + "</v7:sessionID> " + 
				"      </v7:executeSQLQuery> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getLocationUpdateSOAPXmlContent(String request, CustomerDTO c) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/applicationModule/types/\" xmlns:loc=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/locationService/\" xmlns:par=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/partyService/\" xmlns:sour=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/sourceSystemRef/\" xmlns:loc1=\"http://xmlns.oracle.com/apps/cdm/foundation/parties/flex/location/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:updateLocation> " + 
				"         <typ:location> " + 
				"            <loc:LocationId>" + request + "</loc:LocationId> " + 
				"            <loc:Address1>" + c.getAddress1() + "</loc:Address1> " + 
				"            <loc:City>" + c.getCity() + "</loc:City> " + 
				"            <loc:PostalCode>" + c.getPostalCode() + "</loc:PostalCode> " + 
				"            <loc:State>" + c.getState() + "</loc:State> " +  
				"         </typ:location> " + 
				"      </typ:updateLocation> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getARBulkFileImportSOAPXmlContent(String content, String fileName, String ID) {
		String fecha =  dateFormat.format(new Date());	
				
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:importBulkData> " + 
				"         <typ:document> " + 
				"            <erp:Content>" + content +"</erp:Content> " + 
				"            <erp:FileName>" + fileName +"</erp:FileName> " + 
				"            <erp:ContentType>zip</erp:ContentType> " + 
				"         </typ:document> " + 
				"         <typ:jobDetails> " + 
				"            <erp:JobName>/oracle/apps/ess/financials/receivables/transactions/autoInvoices,AutoInvoiceImportEss</erp:JobName> " + 
				"            <erp:ParameterList>"+ ID + ",SICAR,"+ fecha + ",#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,#NULL,Y,#NULL</erp:ParameterList> " + 
				"         </typ:jobDetails> " + 
				"         <typ:notificationCode>00</typ:notificationCode> " + 
				"         <typ:callbackURL>#NULL</typ:callbackURL> " + 
				"      </typ:importBulkData> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getInvBulkFileImportSOAPXmlContent(String content, String fileName) {
			
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:importBulkData> " + 
				"         <typ:document> " + 
				"            <erp:Content>" + content +"</erp:Content> " + 
				"            <erp:FileName>" + fileName +"</erp:FileName> " + 
				"            <erp:ContentType>zip</erp:ContentType> " + 
				"         </typ:document> " + 
				"         <typ:jobDetails> " + 
				"            <erp:JobName>/oracle/apps/ess/scm/inventory/materialTransactions/txnManager,SingleTMEssJob</erp:JobName> " + 
				"            <erp:ParameterList>#NULL</erp:ParameterList> " + 
				"         </typ:jobDetails> " + 
				"         <typ:notificationCode>00</typ:notificationCode> " + 
				"         <typ:callbackURL>#NULL</typ:callbackURL> " + 
				"      </typ:importBulkData> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getGLBulkFileImportSOAPXmlContent(String content, String fileName, String DATA_ACCESS_SET, String LEDGER_ID ) {
						
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/\" xmlns:erp=\"http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <typ:importBulkData> " + 
				"         <typ:document> " + 
				"            <erp:Content>" + content +"</erp:Content> " + 
				"            <erp:FileName>" + fileName +"</erp:FileName> " + 
				"            <erp:ContentType>zip</erp:ContentType> " + 
				"         </typ:document> " + 
				"         <typ:jobDetails> " + 
				"            <erp:JobName>/oracle/apps/ess/financials/generalLedger/programs/common,JournalImportLauncher</erp:JobName> " + 
				"            <erp:ParameterList>"+ DATA_ACCESS_SET + ",Balance Transfer,"+ LEDGER_ID + ",ALL,N,N,N</erp:ParameterList> " + 
				"         </typ:jobDetails> " + 
				"         <typ:notificationCode>00</typ:notificationCode> " + 
				"         <typ:callbackURL>#NULL</typ:callbackURL> " + 
				"      </typ:importBulkData> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		return SOAPRequest;
	}
	
	public static String getFindTransactionNumberSOAPXmlContent(String sessionId, String transactionNumber, String itemCode) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\"> " + 
				"   <soapenv:Header/> " + 
				"   <soapenv:Body> " + 
				"      <v7:executeSQLQuery> " + 
				"         <v7:sql><![CDATA[SELECT " + 
				"   		0 s_0, " + 
				"	   	\"Receivables - Transactions Real Time\".\"- General Information\".\"Transaction Number\" s_1, " + 
				"	   	\"Receivables - Transactions Real Time\".\"- General Information\".\"Transaction Source Name\" s_2, " + 
				"	   	\"Receivables - Transactions Real Time\".\"- General Information\".\"Transaction Type\" s_3, " + 
				"	   	\"Receivables - Transactions Real Time\".\"- Line Information\".\"Transaction Line Number\" s_4, " + 
				"	   	\"Receivables - Transactions Real Time\".\"- Main\".\"Item Name\" s_5 " + 
				"		FROM \"Receivables - Transactions Real Time\" " + 
				"		WHERE ((\"- General Information\".\"Transaction Type\" = 'FACTURA') " +				
				"		AND (\"- General Information\".\"Transaction Number\" = '" + transactionNumber + "') " +
				" 		AND (\"- Main\".\"Item Name\" = '" + itemCode + "')) " + 
				"		ORDER BY 1, 3 ASC NULLS LAST, 2 ASC NULLS LAST, 5 ASC NULLS LAST, 4 ASC NULLS LAST, 6 ASC NULLS LAST " + 
				"	 	FETCH FIRST 1 ROWS ONLY]]></v7:sql> " + 
				"         <v7:outputFormat>XML</v7:outputFormat> " + 
				"         <v7:executionOptions> " + 
				"            <v7:async></v7:async> " + 
				"            <v7:maxRowsPerPage></v7:maxRowsPerPage> " + 
				"            <v7:refresh></v7:refresh> " + 
				"            <v7:presentationInfo></v7:presentationInfo> " + 
				"            <v7:type></v7:type> " + 
				"         </v7:executionOptions> " + 
				"         <v7:sessionID>" + sessionId + "</v7:sessionID> " + 
				"      </v7:executeSQLQuery> " + 
				"   </soapenv:Body> " + 
				"</soapenv:Envelope>";
		
		return SOAPRequest;
	}

	public static String getCloudInventoryCountSOAPXmlContent(String sessionId) {
		String SOAPRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v7=\"urn://oracle.bi.webservices/v7\">\r\n" + 
				"    <soapenv:Header />\r\n" + 
				"    <soapenv:Body>\r\n" + 
				"        <v7:executeSQLQuery>\r\n" + 
				"            <v7:sql><![CDATA[\r\n" + 
				"               SELECT   s_0, s_1, s_2, s_3, s_4, s_5 \r\n" + 
				"			FROM (SELECT unsuppressed 0                                                                                       		s_0,\r\n" + 
				"			           \"Inventory - Inventory Balance Real Time\".\"Inventory Organization\".\"Inventory Organization Code\"   	s_1,\r\n" + 
				"			           \"Inventory - Inventory Balance Real Time\".\"Inventory Organization\".\"Inventory Organization Name\"     	s_2,\r\n" + 
				"			           \"Inventory - Inventory Balance Real Time\".\"Current On-Hand Inventory\".\"Primary Transaction Quantity\" 	s_3, \r\n" + 
				"			           cast(NULL AS DOUBLE)                                                                                 		s_4,\r\n" + 
				"			           cast(NULL AS DOUBLE)                                                                                 		s_5\r\n" + 
				"			    FROM   \"Inventory - Inventory Balance Real Time\" \r\n" + 
				"			    WHERE  ((\"Inventory Organization\".\"Inventory Organization Code\" IS NOT NULL)\r\n" + 
				"			           AND (\"Subinventory\".\"Subinventory Name\" IS NOT NULL) \r\n" + 
				"			           AND (\"- Main\".\"Item Status\" = 'Active')\r\n" + 
				"					   ) \r\n" + 
				"			    UNION ALL \r\n" + 
				"			    SELECT 1																										s_0,\r\n" + 
				"			           \"Inventory - Inventory Balance Real Time\".\"Inventory Organization\".\"Inventory Organization Code\"	s_1,\r\n" + 
				"			           \"Inventory - Inventory Balance Real Time\".\"Inventory Organization\".\"Inventory Organization Name\"	s_2,\r\n" + 
				"			           cast(NULL AS DOUBLE)																						s_3,\r\n" + 
				"			           report_sum(\"Inventory - Inventory Balance Real Time\".\"Current On-Hand Inventory\".\"Primary Transaction Quantity\" BY \"Inventory - Inventory Balance Real Time\".\"Inventory Organization\".\"Inventory Organization Code\") s_4,\r\n" + 
				"			           report_sum(\"Inventory - Inventory Balance Real Time\".\"Current On-Hand Inventory\".\"Primary Transaction Quantity\" BY )                                                                                                 		s_5\r\n" + 
				"			    FROM   \"Inventory - Inventory Balance Real Time\" \r\n" + 
				"			    WHERE  ((\"Inventory Organization\".\"Inventory Organization Code\" IS NOT NULL)\r\n" + 
				"			           AND (\"Subinventory\".\"Subinventory Name\" IS NOT NULL) \r\n" + 
				"			           AND (\"- Main\".\"Item Status\" = 'Active')\r\n" + 
				"					   )\r\n" + 
				"				) djm \r\n" + 
				"			ORDER BY 1, 2 ASC nulls last, 3 ASC nulls last \r\n" + 
				"			FETCH first 75001 ROWS ONLY]]>\r\n" + 
				"			</v7:sql>\r\n" + 
				"            <v7:outputFormat>XML</v7:outputFormat>\r\n" + 
				"            <v7:executionOptions>\r\n" + 
				"                <v7:async />\r\n" + 
				"                <v7:maxRowsPerPage />\r\n" + 
				"                <v7:refresh />\r\n" + 
				"                <v7:presentationInfo />\r\n" + 
				"                <v7:type />\r\n" + 
				"            </v7:executionOptions>\r\n" + 
				"            <v7:sessionID>" + sessionId + "</v7:sessionID>\r\n" + 
				"        </v7:executeSQLQuery>\r\n" + 
				"    </soapenv:Body>\r\n" + 
				"</soapenv:Envelope>";		
		
		return SOAPRequest;
	}
	
}

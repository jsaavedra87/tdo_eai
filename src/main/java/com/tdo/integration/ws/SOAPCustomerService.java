package com.tdo.integration.ws;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.pojo.ClientesJson;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.test.Rowset;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.NullValidator;

@Service("SOAPCustomerService")
public class SOAPCustomerService {
	
	@Autowired  
	HTTPRequestService HTTPRequestService; 
	
	public List<ClientesJson> getCustomers(String request) {
		List<ClientesJson> custFinalList = new ArrayList<ClientesJson>();
		List<ClientesJson> custList = null;
		
		try {
			int customerFetchStart = 0;			
											
			do {
				custList = null;
				String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SALESPARTY_URL + "?invoke=", 
						                                            PayloadProducer.getSalesPartySOAPXmlContent(request, customerFetchStart),
						                                            AppConstants.CLOUD_CREDENTIALS);			
				if(response != null) {
					JSONObject xmlJSONObj = XML.toJSONObject(response, true);
					JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
					custList = new ArrayList<ClientesJson>();
					custList = getCustomersFromJson(jelement);	
					if(custList != null && !custList.isEmpty()) {
						
						if(custList.size() == 1 && custList.get(0).getCliId() == 0 && custList.get(0).getClave() == null && custList.get(0).getCurp() == null && custList.get(0).getNombre() == null) {
							custList = null;
						} else {							
							for(ClientesJson c : custList) {
								if(c.getClave() != null) {
									String accNbr = getCustomerAccount(c);
									if(!"".equals(accNbr)) {
										getCustomerProfile(c, accNbr);
										c.setClave("");
									}
								}
							}
							custFinalList.addAll(custList);
							customerFetchStart += custList.size();
						}											
					}
				}
			} while (custList != null && custList.size() > 0);		
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return custFinalList;
	}
	
	
	private String getCustomerAccount(ClientesJson c) {
		String accountNumber = "";
		String siteNumber = "";
		String response = "";
		
		try {
			JSONObject xmlJSONObj = null;
			JSONObject json = null;
			JsonElement jelement = null;
			JsonObject jobject = null;
			JsonElement result = null;
			int requestTry = 0;
			
			do {
				if(requestTry > 0) {
					Thread.sleep(1000);
				}
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.getSessionIdSOAPXmlContent(), "");
				
				xmlJSONObj = XML.toJSONObject(response, true);
				jelement = new JsonParser().parse(xmlJSONObj.toString());
				jobject = jelement.getAsJsonObject();
				result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
										.get("sawsoap:logonResult").getAsJsonObject();
				String sessionID = result.getAsJsonObject().get("sawsoap:sessionID").toString();
				json = new JSONObject(sessionID);
				String contentId = json.getString("content");
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_INVTRANSACTIONS_URL,
						PayloadProducer.getCustomerAccountSOAPXmlContent(contentId, c.getNombre()), "");
		
				xmlJSONObj = XML.toJSONObject(response, true);
				jelement = new JsonParser().parse(xmlJSONObj.toString());
				jobject = jelement.getAsJsonObject();
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.logOffSessionIdSOAPXmlContent(contentId), "");				
				requestTry+=1;
				
			} while (jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR) && requestTry < 4);
			
			if(!jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR)) {
				result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
						.get("sawsoap:executeSQLQueryResult").getAsJsonObject().get("sawsoap:return").getAsJsonObject();
				String rowSet = result.getAsJsonObject().get("sawsoap:rowset").toString();
				rowSet = rowSet.replace("\\\"", "\"");
				rowSet = rowSet.replace("\"<", "<");
				rowSet = rowSet.replace(">\"", ">");
				rowSet = rowSet.replace("xmlns=\"urn:schemas-microsoft-com:xml-analysis:rowset\"", "");
		
				StringReader sr = new StringReader(rowSet);
				JAXBContext jaxbContext = JAXBContext.newInstance(Rowset.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				Rowset rsList = (Rowset) unmarshaller.unmarshal(sr);
				
				if (rsList != null && rsList.getRow() != null && !rsList.getRow().isEmpty()) {
					
					if(rsList.getRow().get(0).getColumn1() != null) {
						accountNumber = rsList.getRow().get(0).getColumn1();
					}
					if(rsList.getRow().get(0).getColumn3() != null) {
						siteNumber = rsList.getRow().get(0).getColumn3();
					}
					
					c.setCurp(new StringBuffer().append(accountNumber.trim()).append("/").append(siteNumber.trim()).toString());
				}				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
		
		return accountNumber;
	}
	
	@SuppressWarnings("unused")
	private String getCustomerAccountOld(ClientesJson c) {
		String accNbr = "";
		try {
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CUSTOMERACCOUNT_URL + "?invoke=", 
	                PayloadProducer.getCustomerAccountOldSOAPXmlContent(c.getClave()),
	                AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				JsonObject jobject = jelement.getAsJsonObject();
				JsonElement result = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findCustomerAccountResponse").getAsJsonObject().get("ns0:result").getAsJsonObject();
				
				try {
					JsonElement value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
					accNbr = value.getAsJsonObject().get("ns2:AccountNumber").toString();
				}catch(Exception e) {
					return "";
				}
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
		return accNbr;
	}
	
	private ClientesJson getCustomerProfile(ClientesJson c, String accNbr) {
		
		try {
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CUSTOMERPROFILE_URL + "?invoke=", 
	                PayloadProducer.getCustomerProfileSOAPXmlContent(c.getClave(), accNbr),
	                AppConstants.CLOUD_CREDENTIALS);
			
			if(response != null) {
				JSONObject xmlJSONObj = XML.toJSONObject(response, true);
				JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
				JsonObject jobject = jelement.getAsJsonObject();
				JsonElement value = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:getActiveCustomerProfileResponse").getAsJsonObject().get("ns4:result").getAsJsonObject().getAsJsonObject().get("ns3:Value").getAsJsonObject();
				String credLimit = value.getAsJsonObject().get("ns3:CreditLimit").toString();
				String pTerms = value.getAsJsonObject().get("ns3:PaymentTerms").toString();
				String nil = "{\"xsi:nil\":\"true\"}";
				 if(credLimit.contains(nil)){
					 c.setLimite(0);
				 }else {
					 credLimit = credLimit.replaceAll("\"", "");
					 c.setLimite(Float.valueOf(credLimit));					 
				 }
				 
				 if(pTerms.contains(nil)){
					 c.setDiasCredito(0);
				 }else {
					 pTerms = pTerms.replaceAll("\"", "");
					 c.setDiasCredito(Integer.valueOf(pTerms)); 
				 }
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return c;
	}
	
	public List<ClientesJson> getCustomersFromJson(JsonElement jelement){
		
		
		List<ClientesJson> clientesJson = new ArrayList<ClientesJson>();
		try {
			JsonObject jobject = jelement.getAsJsonObject();
			JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findSalesPartyResponse").getAsJsonObject();
			JsonElement bdy = soapEnvelope.getAsJsonObject().get("ns2:result");
	
			if(bdy instanceof JsonArray) {
				JsonArray jsonarray = bdy.getAsJsonArray();
				for (int i = 0; i < jsonarray.size(); i++) {
					JsonElement op = jsonarray.get(i).getAsJsonObject();
					ClientesJson c = getClientFromJSON(op);
					if(c != null) {
					   clientesJson.add(c);
					}
				}
			}else {
				ClientesJson c = getClientFromJSON(bdy);
				clientesJson.add(c);	
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return clientesJson;
	}
	
private static ClientesJson getClientFromJSON(JsonElement root) {

		ClientesJson clientes = new ClientesJson();
		try {
			if(root != null) {
				JsonElement organizationParty = root.getAsJsonObject().get("ns1:OrganizationParty");
				JsonElement organizationProfile = organizationParty.getAsJsonObject().get("ns3:OrganizationProfile");
				JsonElement partySite = organizationParty.getAsJsonObject().get("ns3:PartySite");
			   
			    clientes.setCliId(0);
				clientes.setNombre(NullValidator.isNull(root.getAsJsonObject().get("ns1:PartyName").toString()));
				clientes.setRepresentante(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PreferredContactName").toString()));
//				String partyNumber = NullValidator.isNull(String.valueOf(root.getAsJsonObject().get("ns1:PartyNumber").toString()));
				clientes.setRfc(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:JgzzFiscalCode").toString()));

				if(partySite != null) {
					clientes.setDomicilio(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address1").toString()));
					clientes.setNoExt(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address2").toString()));
					clientes.setNoInt("");
					clientes.setLocalidad("");
					clientes.setCiudad(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:City").toString()));
					clientes.setEstado(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:State").toString()));
					clientes.setPais(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Country").toString()));
					clientes.setCodigoPostal(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:PostalCode").toString()));
					clientes.setColonia(NullValidator.isNull(partySite.getAsJsonObject().get("ns6:Address3").toString()));
					String siteNumber = NullValidator.isNull(partySite.getAsJsonObject().get("ns6:PartySiteNumber").toString());
					clientes.setCurp("/" + siteNumber);
				}else {
					clientes.setCurp("/" + "UKNOWN");
				}
				
				clientes.setTelefono(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PrimaryPhoneNumber").toString()));
				clientes.setCelular("");
				clientes.setMail(NullValidator.isNull(organizationProfile.getAsJsonObject().get("ns3:PrimaryEmailAddress").toString()));
				clientes.setComentario("");
				
				if("A".equals(NullValidator.isNull(root.getAsJsonObject().get("ns1:Status").toString())))
				     clientes.setStatus(1);
				else
					 clientes.setStatus(0);
			
				clientes.setLimite(0); 
				clientes.setPrecio(1); 
				clientes.setDiasCredito(0); 
				clientes.setRetener(false);
				clientes.setDesglosarIEPS(false);
				clientes.setClave(NullValidator.isNull(root.getAsJsonObject().get("ns1:PartyId").toString()));
				clientes.setFoto(null);
				clientes.setHuella(null);
				clientes.setMuestra(null);
				clientes.setUsoCfdi("");
				clientes.setComplementoList(new String[]{});
				
				if("".equals(clientes.getRepresentante())) {
					clientes.setRepresentante("Sin Representante");
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		return clientes;
	}


	/* ORCHESTRATION:
	1. Create Location: Return locationId
	2. Create Customer Organization: Return partyId, partyNumber, partySiteId, partySiteNumber
	3. Create Customer Account: Return customerAccountId
	4. Create Customer Profile: Return customerAccountProfileId
	* 
	*/
	@SuppressWarnings("unused")
	public List<CustomerDTO> insertCustomers(List<CustomerDTO> request) {
		
		if(request != null) {
			if(!request.isEmpty()) {
				for(CustomerDTO c : request) {
 
					try {
						String responseFindCustomer = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_FIND_ORGANIZATION_URL + "?invoke=", 
                                PayloadProducer.findOrganizationByFiscalCodeSOAPXmlContent(c.getName()),
                                AppConstants.CLOUD_CREDENTIALS);
						
						if(responseFindCustomer != null) {
							JSONObject xmlJSONObj = XML.toJSONObject(responseFindCustomer, true);
							JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
							JsonObject jobject = jelement.getAsJsonObject();
							JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:findOrganizationResponse").getAsJsonObject();
							JsonElement result = soapEnvelope.getAsJsonObject().get("ns3:result");
							JsonElement value = result.getAsJsonObject().get("ns2:Value");
							
							
							if(value != null) {
								String locId = null;
								if(value instanceof JsonArray) {
									JsonArray jsonarray = value.getAsJsonArray();
									for (int i = 0; i < jsonarray.size(); i++) {
										JsonElement partySite = jsonarray.get(i).getAsJsonObject().get("ns2:PartySite");
										JsonElement locationId = partySite.getAsJsonObject().get("ns1:LocationId");
										
										if(locationId != null) {
											locId = locationId.toString();
											break;
										}
									}
								}else {
									JsonElement partySite = value.getAsJsonObject().get("ns2:PartySite");
									JsonElement locationId = partySite.getAsJsonObject().get("ns1:LocationId");
									
									if(locationId != null) {
										locId = locationId.toString().replace("\"", "");
									}
								}
								
								if(locId != null) {
									locId = locId.replaceAll("\"", "");
									String responseUdateLocation = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_FIND_LOCATION_URL + "?invoke=", 
			                                PayloadProducer.getLocationUpdateSOAPXmlContent(locId, c),
			                                AppConstants.CLOUD_CREDENTIALS);
									
									c.setMsg("CUSTOMER ALREADY EXISTS. UPDATE OPERATION COMPLETE");
								}else {
									c.setMsg("ERROR: NO LOCATION FOUND");
								}
								
								continue;
							}
							
						}
						
						//1. Create Location: Return locationId
						String responseLocation = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATELOCATION_URL + "?invoke=", 
                                PayloadProducer.getLocationSOAPXmlContent(c),
                                AppConstants.CLOUD_CREDENTIALS);
						
						if(responseLocation != null) {
							JSONObject xmlJSONObj = XML.toJSONObject(responseLocation, true);
							JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
							JsonObject jobject = jelement.getAsJsonObject();
							JsonElement soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createLocationResponse").getAsJsonObject();
							JsonElement result = soapEnvelope.getAsJsonObject().get("ns2:result");
							JsonElement value = result.getAsJsonObject().get("ns1:Value").getAsJsonObject();
							String locationId = NullValidator.isNull(value.getAsJsonObject().get("ns1:LocationId").toString());
							locationId = locationId.replace("\"", "");
							if(!"".equals(locationId)) {
								
								//2. Create Customer Organization: Return partyId, partyNumber, partySiteId, partySiteNumber
								String responseOrganization = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEORGANIZATION_URL + "?invoke=", 
		                                PayloadProducer.getCustomerOrganizationSOAPXmlContent(c, locationId),
		                                AppConstants.CLOUD_CREDENTIALS);
								
								if(responseOrganization != null) {
									xmlJSONObj = XML.toJSONObject(responseOrganization, true);
									jelement = new JsonParser().parse(xmlJSONObj.toString());
									jobject = jelement.getAsJsonObject();
									soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createOrganizationResponse").getAsJsonObject();
									result = soapEnvelope.getAsJsonObject().get("ns3:result");
									value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
									String partyId = NullValidator.isNull(value.getAsJsonObject().get("ns2:PartyId").toString());
									String partyNumber = NullValidator.isNull(value.getAsJsonObject().get("ns2:PartyNumber").toString());
									
									JsonElement partySite = value.getAsJsonObject().get("ns2:PartySite");
									String partySiteId = NullValidator.isNull(partySite.getAsJsonObject().get("ns1:PartySiteId").toString());
									String partySiteNumber = NullValidator.isNull(partySite.getAsJsonObject().get("ns1:PartySiteNumber").toString());
									partyId = partyId.replace("\"", "");
									partyNumber = partyNumber.replace("\"", "");
									partySiteNumber = partySiteNumber.replace("\"", "");
									
									if(!"".equals(partyId)) {
										c.setPartyNumber(partyNumber);
										c.setSiteNumber(partySiteNumber);
										
										//3. Create Customer Account: Return customerAccountId
										String responseAccount = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEACCOUNT_URL + "?invoke=", 
				                                PayloadProducer.getCustomerAccountSOAPXmlContent(c, partyId, partySiteId, partyNumber, AppConstants.SET_ID),
				                                AppConstants.CLOUD_CREDENTIALS);
										
										if(responseAccount != null) {
											xmlJSONObj = XML.toJSONObject(responseAccount, true);
											jelement = new JsonParser().parse(xmlJSONObj.toString());
											jobject = jelement.getAsJsonObject();
											soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createCustomerAccountResponse").getAsJsonObject();
											result = soapEnvelope.getAsJsonObject().get("ns0:result");
											value = result.getAsJsonObject().get("ns2:Value").getAsJsonObject();
											String customerAccountId = NullValidator.isNull(value.getAsJsonObject().get("ns2:CustomerAccountId").toString());
											String accountNumber = NullValidator.isNull(value.getAsJsonObject().get("ns2:AccountNumber").toString());
											customerAccountId = customerAccountId.replace("\"", "");
											accountNumber = accountNumber.replace("\"", "");
											
											if(!"".equals(customerAccountId)) {
												c.setAccountNumber(accountNumber);
												
												//4. Create Customer Profile: Return profileClassName
												String responseProfile = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_CREATEPROFILE_URL + "?invoke=", 
						                                PayloadProducer.getCustomerProfileServiceSOAPXmlContent(partyId, customerAccountId),
						                                AppConstants.CLOUD_CREDENTIALS);
												
												if(responseProfile != null) {
													xmlJSONObj = XML.toJSONObject(responseProfile, true);
													jelement = new JsonParser().parse(xmlJSONObj.toString());
													jobject = jelement.getAsJsonObject();
													soapEnvelope = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:createCustomerProfileResponse").getAsJsonObject();
													result = soapEnvelope.getAsJsonObject().get("ns4:result");
													value = result.getAsJsonObject().get("ns3:Value").getAsJsonObject();
													String profileClassName = NullValidator.isNull(value.getAsJsonObject().get("ns3:ProfileClassName").toString());
													profileClassName = profileClassName.replace("\"", "");
													
													if(!"".equals(customerAccountId)) {
														c.setProfileClassName(profileClassName);														
													}else {
														c.setMsg("Error en la creación del registro de PROFILE. El cliente " + c.getName() + " no pudo ser creado");
														continue;
													}
												}else {
													c.setMsg("Error en la creación del registro de PROFILE. El cliente " + c.getName() + " no pudo ser creado");
													continue;
												}
											}else {
												c.setMsg("Error en la creación del registro de ACCOUNT. El cliente " + c.getName() + " no pudo ser creado");
												continue;
											}
										}else {
											c.setMsg("Error en la creación del registro de ACCOUNT. El cliente " + c.getName() + " no pudo ser creado");
											continue;
										}
									}else {
										c.setMsg("Error en la creación del registro de ORGANIZACIÓN. El cliente " + c.getName() + " no pudo ser creado");
										continue;
									}	
								}else {
									c.setMsg("Error en la creación del registro de ORGANIZACIÓN. El cliente " + c.getName() + " no pudo ser creado");
									continue;
								}
							}else {
								c.setMsg("Error en la creación del registro de DIRECCIONES. El cliente " + c.getName() + " no pudo ser creado");
								continue;
							}
						}
						
					}catch(Exception e) {
						c.setMsg(e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}
		
		return request;
	}

}

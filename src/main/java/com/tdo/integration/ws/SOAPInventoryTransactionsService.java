package com.tdo.integration.ws;

import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tdo.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.test.Row;
import com.tdo.integration.test.Rowset;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.util.NullValidator;

@Service("SOAPInventoryTransactionsService")
public class SOAPInventoryTransactionsService {

	@Autowired
	HTTPRequestService HTTPRequestService;

	public List<InventoryTransactionDTO> getInventoryTransactionTransfers(String request, String defaultOrganization) {
		
		String sucursal = "";
		String dateFrom = "";
		String dateTo = "";
		String[] str = request.split("/");
		
		if (str.length == 3) {
			sucursal = str[0];
			dateFrom = str[1].replace("_", " ");
			dateTo = str[2].replace("_", " ");
		}else if (str.length == 2) {
			sucursal = str[0];
			dateFrom =str[1].replace("_", " ");
		} else {
			return null;
		} 

		List<InventoryTransactionDTO> invList = new ArrayList<InventoryTransactionDTO>();
		
		try {			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
					PayloadProducer.getSessionIdSOAPXmlContent(), "");
			
			JSONObject xmlJSONObj = XML.toJSONObject(response, true);
			JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
			JsonObject jobject = jelement.getAsJsonObject();
			JsonElement result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
					.get("sawsoap:logonResult").getAsJsonObject();
			String sessionID = result.getAsJsonObject().get("sawsoap:sessionID").toString();
			JSONObject json = new JSONObject(sessionID);
			String contentId = json.getString("content");
			
			response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_INVTRANSACTIONS_URL,
					PayloadProducer.getInventoryTransactionTransferSOAPXmlContent(contentId, sucursal, dateFrom, dateTo), "");
	
			xmlJSONObj = XML.toJSONObject(response, true);
			jelement = new JsonParser().parse(xmlJSONObj.toString());
			jobject = jelement.getAsJsonObject();
			
			if(jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR)) {
				return null;
			}
			
			result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
					.get("sawsoap:executeSQLQueryResult").getAsJsonObject().get("sawsoap:return").getAsJsonObject();
			String rowSet = result.getAsJsonObject().get("sawsoap:rowset").toString();
			rowSet = rowSet.replace("\\\"", "\"");
			rowSet = rowSet.replace("\"<", "<");
			rowSet = rowSet.replace(">\"", ">");
			rowSet = rowSet.replace("xmlns=\"urn:schemas-microsoft-com:xml-analysis:rowset\"", "");
	
			StringReader sr = new StringReader(rowSet);
			JAXBContext jaxbContext = JAXBContext.newInstance(Rowset.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Rowset rsList = (Rowset) unmarshaller.unmarshal(sr);

			Map<String, InventoryTransactionDTO> invMap = new HashMap<String, InventoryTransactionDTO>();
			if (rsList != null) {
				if(rsList.getRow() != null) {
					for(Row r:rsList.getRow()) {
						
						InventoryTransactionDTO mapInv = (InventoryTransactionDTO) invMap.get(r.getColumn0());
						if(mapInv == null) {
							InventoryTransactionDTO obj = new InventoryTransactionDTO();
							obj.setItemName(r.getColumn0());
							obj.setInventoryOrganizationCode(r.getColumn3());
							invMap.put(r.getColumn0(), obj);
						}
						
						InventoryTransactionDTO o = new InventoryTransactionDTO();
						o.setItemName(r.getColumn0());
						o.setItemDescription(r.getColumn1());
						o.setUom(r.getColumn2());
						o.setInventoryOrganizationCode(r.getColumn3());
						o.setInventoryOrganizationName(r.getColumn4());
						o.setTransactionDate(r.getColumn5());
						
						if(r.getColumn6() == null) {
							o.setInventoryLocation("");
						}else {
							String loc = r.getColumn6().trim().replace(".", "_");
							o.setInventoryLocation(loc);
						}
						
						if(r.getColumn10() == null) {
							o.setSubInventory("");
						}else {
							o.setSubInventory(r.getColumn10().trim());
						}
						
						o.setCreationDate(r.getColumn7());
						o.setExpirationDate(r.getColumn8());
						o.setTransactionType(r.getColumn11());
						o.setQuantity(r.getColumn12());
						
						String lotNumber = "SL";
						if(r.getColumn9() != null) {
							if(!"".equals(r.getColumn9())) {
								lotNumber = r.getColumn9();
							}
						}
						
						if(!"".equals(o.getSubInventory())) {
							o.setLotNumber(lotNumber + "_" + o.getSubInventory());
						}
						
						if(!"".equals(o.getInventoryLocation())) {
							o.setLotNumber(o.getLotNumber() + "_" + o.getInventoryLocation());
						}
						
						o.setItemType(r.getColumn13());
						o.setTransactionId(r.getColumn14());
						o.setTransactionDate(r.getColumn15());
						o.setItemId(0);
						o.setAverageCost(0);
						invList.add(o);
					}
					
					response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_ITEMCOSTS_URL,
							   PayloadProducer.getItemCostSOAPXmlContent(invMap, defaultOrganization), AppConstants.CLOUD_CREDENTIALS);
			
					if(response != null) {
						xmlJSONObj = XML.toJSONObject(response, true);
						jelement = new JsonParser().parse(xmlJSONObj.toString());
						jobject = jelement.getAsJsonObject();
						result = jobject.get("env:Envelope").getAsJsonObject().get("env:Body").getAsJsonObject().get("ns0:retrieveItemCostResponse").getAsJsonObject().get("ns2:result").getAsJsonObject();
						JsonElement value = result.getAsJsonObject().get("ns1:Value");
						
						if(value instanceof JsonArray) {
							JsonArray jsonarray = value.getAsJsonArray();
							for (int j = 0; j < jsonarray.size(); j++) {
								JsonElement op = jsonarray.get(j).getAsJsonObject();
								String itemNumber = NullValidator.isNull(op.getAsJsonObject().get("ns1:ItemNumber").toString());
								String itemCostStr = NullValidator.isNull(op.getAsJsonObject().get("ns1:ItemCost").toString());
								double itemCost = Double.parseDouble(itemCostStr);
								
								itemCost = Math.round(1000 * itemCost) / 1000d;
								
								for(InventoryTransactionDTO o : invList) {
									if(itemNumber.trim().equals(o.getItemName())) {
										o.setAverageCost(itemCost);
									}
								}
							}
							
						}else {							
							String itemNumber = NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemNumber").toString());
							String itemCostStr = NullValidator.isNull(value.getAsJsonObject().get("ns1:ItemCost").toString());
							double itemCost = Double.parseDouble(itemCostStr);
							
							itemCost = Math.round(1000 * itemCost) / 1000d;
							
							for(InventoryTransactionDTO o : invList) {
								if(itemNumber.trim().equals(o.getItemName())) {
									o.setAverageCost(itemCost);
								}
							}
						}
					}else {
						return null;
					}
				}
			}
			
			response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
					PayloadProducer.logOffSessionIdSOAPXmlContent(contentId), "");
			
		}catch(Exception e) {
			e.printStackTrace();		
		}
		
		return invList;
	}
	
	public double getItemRateAverage(String request) {
		double avgRate = 0.0;
		try {
			String organizationCode = "";
			String item = "";
			String[] str = request.split("/");
			
			if(str.length != 2) {
				return 0.0;
			}else {
				organizationCode = str[0];
				item = str[1];
			}
						
			String response;
			String sessionID;
			String contentId;
			
			JSONObject xmlJSONObj;
			JSONObject json;
			JsonElement jelement;
			JsonObject jobject;
			JsonElement result;			
			
			int trySession = 0;
			
			do {
				if(trySession > 0) {
					Thread.sleep(1000);
				}
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.getSessionIdSOAPXmlContent(), "");
		
				xmlJSONObj = XML.toJSONObject(response, true);
				jelement = new JsonParser().parse(xmlJSONObj.toString());
				jobject = jelement.getAsJsonObject();
				result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
						.get("sawsoap:logonResult").getAsJsonObject();
				sessionID = result.getAsJsonObject().get("sawsoap:sessionID").toString();
				json = new JSONObject(sessionID);
				contentId = json.getString("content");
		
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_INVTRANSACTIONS_URL,
						PayloadProducer.findPurchaseAverageCostSOAPXmlContent(item, contentId, organizationCode), "");
		
				xmlJSONObj = XML.toJSONObject(response, true);
				jelement = new JsonParser().parse(xmlJSONObj.toString());
				jobject = jelement.getAsJsonObject();
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.logOffSessionIdSOAPXmlContent(contentId), "");
				trySession += 1;
				
			}while(jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR) && trySession < 3);
			
			if(jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR)) {
				return 0;
			}
						
			result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
					.get("sawsoap:executeSQLQueryResult").getAsJsonObject().get("sawsoap:return").getAsJsonObject();
			String rowSet = result.getAsJsonObject().get("sawsoap:rowset").toString();
			rowSet = rowSet.replace("\\\"", "\"");
			rowSet = rowSet.replace("\"<", "<");
			rowSet = rowSet.replace(">\"", ">");
			rowSet = rowSet.replace("xmlns=\"urn:schemas-microsoft-com:xml-analysis:rowset\"", "");
	
			StringReader sr = new StringReader(rowSet);
			JAXBContext jaxbContext = JAXBContext.newInstance(Rowset.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Rowset rsList = (Rowset) unmarshaller.unmarshal(sr);
			double quantity = 0;
			double extendedPrice = 0;
			double extendedPriceTotal = 0;
			if (rsList != null) {
				if(rsList.getRow() != null) {
					for(Row r:rsList.getRow()) {
						quantity = quantity + Double.valueOf(r.getColumn3());
						extendedPrice = Double.valueOf(r.getColumn3()) * Double.valueOf(r.getColumn4());
						extendedPriceTotal = extendedPriceTotal + extendedPrice;	
					}
				}
			}
			avgRate = extendedPriceTotal/quantity;
			avgRate = Math.round(1000 * avgRate) / 1000d;
			
			return avgRate;
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public BigDecimal getCloudInventoryCount(String request) {

		BigDecimal cloudCount = new BigDecimal(0);
		
		try {
			String organizationCode = request;
			String response;
			String sessionID;
			String contentId;
			
			JSONObject xmlJSONObj;
			JSONObject json;
			JsonElement jelement;
			JsonObject jobject;
			JsonElement result;			
			
			int trySession = 0;
			
			do {
				if(trySession > 0) {
					Thread.sleep(1000);
				}
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.getSessionIdSOAPXmlContent(), "");
		
				xmlJSONObj = XML.toJSONObject(response, true);
				jelement = new JsonParser().parse(xmlJSONObj.toString());
				jobject = jelement.getAsJsonObject();
				result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
						.get("sawsoap:logonResult").getAsJsonObject();
				sessionID = result.getAsJsonObject().get("sawsoap:sessionID").toString();
				json = new JSONObject(sessionID);
				contentId = json.getString("content");
		
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_INVTRANSACTIONS_URL,
						PayloadProducer.getCloudInventoryCountSOAPXmlContent(contentId), "");
		
				xmlJSONObj = XML.toJSONObject(response, true);
				jelement = new JsonParser().parse(xmlJSONObj.toString());
				jobject = jelement.getAsJsonObject();
				
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.logOffSessionIdSOAPXmlContent(contentId), "");
				
				trySession += 1;
				
			}while(jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR) && trySession < 3);
			
			if(jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR)) {
				return new BigDecimal(0);
			}
						
			result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
					.get("sawsoap:executeSQLQueryResult").getAsJsonObject().get("sawsoap:return").getAsJsonObject();
			String rowSet = result.getAsJsonObject().get("sawsoap:rowset").toString();
			rowSet = rowSet.replace("\\\"", "\"");
			rowSet = rowSet.replace("\"<", "<");
			rowSet = rowSet.replace(">\"", ">");
			rowSet = rowSet.replace("xmlns=\"urn:schemas-microsoft-com:xml-analysis:rowset\"", "");
	
			StringReader sr = new StringReader(rowSet);
			JAXBContext jaxbContext = JAXBContext.newInstance(Rowset.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Rowset rsList = (Rowset) unmarshaller.unmarshal(sr);

			if (rsList != null) {
				if(rsList.getRow() != null) {
					for(Row r:rsList.getRow()) {
						if(r.getColumn1() != null && !r.getColumn1().isEmpty() && organizationCode.equals(r.getColumn1()) && r.getColumn3() != null && !r.getColumn3().isEmpty()) {
							cloudCount = new BigDecimal(r.getColumn3());
							break;
						}
					}
				}
			}
						
			return cloudCount;
		}catch(Exception e) {
			e.printStackTrace();
			return new BigDecimal(0);
		}
	}	
	
}

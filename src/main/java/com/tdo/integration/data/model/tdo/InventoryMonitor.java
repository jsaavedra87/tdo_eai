package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inventorymonitor")
public class InventoryMonitor {

	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int branchId;
	private String branchName;
	private String branchCode;
	private BigDecimal itemCount;
	private BigDecimal lotCount;
	private BigDecimal cloudCount;
	private Date lastUpdate;
	private String status;
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public BigDecimal getItemCount() {
		return itemCount;
	}
	public void setItemCount(BigDecimal itemCount) {
		this.itemCount = itemCount;
	}
	public BigDecimal getLotCount() {
		return lotCount;
	}
	public void setLotCount(BigDecimal lotCount) {
		this.lotCount = lotCount;
	}
	public BigDecimal getCloudCount() {
		return cloudCount;
	}
	public void setCloudCount(BigDecimal cloudCount) {
		this.cloudCount = cloudCount;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
		
}

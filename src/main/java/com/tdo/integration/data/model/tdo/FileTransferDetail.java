package com.tdo.integration.data.model.tdo;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class FileTransferDetail {

	private static final long serialVersionUID = 1L;
//	@Id
//	@Column
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private BigInteger Id;
	private String ProcessId = "";
	private String FileName = "";
	private String SourceReferecne = "";
	private String BusinessUnitId = "";
	private String StatusProcess = "";
	private String StatusSale = "";
	private Date FileSentDate;
	private Date FileStatusLastUpdate;
	private int RecoveryTries;
	
	
}

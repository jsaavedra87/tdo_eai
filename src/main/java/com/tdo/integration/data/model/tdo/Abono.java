package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "abono")
public class Abono {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String sucursal;
	private BigInteger abonoId;
	private BigInteger creditoId;
	private Integer clienteId;
	private BigInteger clienteNubeId;
	private Date fechaAbono;
	private String tipoPagoClave;
	private String tipoPagoNombre;
	private BigDecimal abonoTotal;
	private String comentario;
	private String descripcionCaja;
	private String numeroCuenta;
	private String numeroSitio;
	private BigInteger idVenta;
	private BigInteger idTicket;
	private BigInteger idNotaVenta;
	private BigInteger idResumenCaja;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}	
	public BigInteger getAbonoId() {
		return abonoId;
	}
	public void setAbonoId(BigInteger abonoId) {
		this.abonoId = abonoId;
	}
	public BigInteger getCreditoId() {
		return creditoId;
	}
	public void setCreditoId(BigInteger creditoId) {
		this.creditoId = creditoId;
	}	
	public Integer getClienteId() {
		return clienteId;
	}
	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}
	public BigInteger getClienteNubeId() {
		return clienteNubeId;
	}
	public void setClienteNubeId(BigInteger clienteNubeId) {
		this.clienteNubeId = clienteNubeId;
	}
	public Date getFechaAbono() {
		return fechaAbono;
	}
	public void setFechaAbono(Date fechaAbono) {
		this.fechaAbono = fechaAbono;
	}
	public String getTipoPagoClave() {
		return tipoPagoClave;
	}
	public void setTipoPagoClave(String tipoPagoClave) {
		this.tipoPagoClave = tipoPagoClave;
	}
	public String getTipoPagoNombre() {
		return tipoPagoNombre;
	}
	public void setTipoPagoNombre(String tipoPagoNombre) {
		this.tipoPagoNombre = tipoPagoNombre;
	}
	public BigDecimal getAbonoTotal() {
		return abonoTotal;
	}
	public void setAbonoTotal(BigDecimal abonoTotal) {
		this.abonoTotal = abonoTotal;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}	
	public String getDescripcionCaja() {
		return descripcionCaja;
	}
	public void setDescripcionCaja(String descripcionCaja) {
		this.descripcionCaja = descripcionCaja;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getNumeroSitio() {
		return numeroSitio;
	}
	public void setNumeroSitio(String numeroSitio) {
		this.numeroSitio = numeroSitio;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public BigInteger getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(BigInteger idTicket) {
		this.idTicket = idTicket;
	}
	public BigInteger getIdNotaVenta() {
		return idNotaVenta;
	}
	public void setIdNotaVenta(BigInteger idNotaVenta) {
		this.idNotaVenta = idNotaVenta;
	}
	public BigInteger getIdResumenCaja() {
		return idResumenCaja;
	}
	public void setIdResumenCaja(BigInteger idResumenCaja) {
		this.idResumenCaja = idResumenCaja;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}

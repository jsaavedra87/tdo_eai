package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "articuloImpuesto")
public class ArticuloImpuesto {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private BigInteger idVenta;
	private BigInteger notaCreditoId;
	private int articuloId;
    private String articuloClave;
    private String ventaDetalleImpuestoNombre; 
    private BigDecimal ventaDetalleImpuestoValor; 
    private BigDecimal ventaDetalleImpuestoTotal;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public int getArticuloId() {
		return articuloId;
	}
	public void setArticuloId(int articuloId) {
		this.articuloId = articuloId;
	}	
	public BigInteger getNotaCreditoId() {
		return notaCreditoId;
	}
	public void setNotaCreditoId(BigInteger notaCreditoId) {
		this.notaCreditoId = notaCreditoId;
	}
	public String getArticuloClave() {
		return articuloClave;
	}
	public void setArticuloClave(String articuloClave) {
		this.articuloClave = articuloClave;
	}
	public String getVentaDetalleImpuestoNombre() {
		return ventaDetalleImpuestoNombre;
	}
	public void setVentaDetalleImpuestoNombre(String ventaDetalleImpuestoNombre) {
		this.ventaDetalleImpuestoNombre = ventaDetalleImpuestoNombre;
	}
	public BigDecimal getVentaDetalleImpuestoValor() {
		return ventaDetalleImpuestoValor;
	}
	public void setVentaDetalleImpuestoValor(BigDecimal ventaDetalleImpuestoValor) {
		this.ventaDetalleImpuestoValor = ventaDetalleImpuestoValor;
	}
	public BigDecimal getVentaDetalleImpuestoTotal() {
		return ventaDetalleImpuestoTotal;
	}
	public void setVentaDetalleImpuestoTotal(BigDecimal ventaDetalleImpuestoTotal) {
		this.ventaDetalleImpuestoTotal = ventaDetalleImpuestoTotal;
	}

}

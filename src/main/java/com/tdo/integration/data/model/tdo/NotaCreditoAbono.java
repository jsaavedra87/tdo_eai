package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notacreditoabono")
public class NotaCreditoAbono {

	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)	
	private int id;
	private String sucursal;
	private BigInteger idNotaCredito;
	private BigInteger creditoId;
	private Integer clienteId;
	private BigInteger clienteNubeId;
	private Date fecha;
	private BigDecimal total;
	private Integer status;
	private String descripcionCaja;
	private String numeroCuenta;
	private String numeroSitio;
	private BigInteger idVenta;
	private BigInteger idTicket;
	private BigInteger idNotaVenta;	
	private BigInteger idResumenCaja;
	private BigInteger folio;
	private BigInteger folioNC;
	private String serieFolio;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public BigInteger getIdNotaCredito() {
		return idNotaCredito;
	}
	public void setIdNotaCredito(BigInteger idNotaCredito) {
		this.idNotaCredito = idNotaCredito;
	}
	public BigInteger getCreditoId() {
		return creditoId;
	}
	public void setCreditoId(BigInteger creditoId) {
		this.creditoId = creditoId;
	}
	public Integer getClienteId() {
		return clienteId;
	}
	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}
	public BigInteger getClienteNubeId() {
		return clienteNubeId;
	}
	public void setClienteNubeId(BigInteger clienteNubeId) {
		this.clienteNubeId = clienteNubeId;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDescripcionCaja() {
		return descripcionCaja;
	}
	public void setDescripcionCaja(String descripcionCaja) {
		this.descripcionCaja = descripcionCaja;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getNumeroSitio() {
		return numeroSitio;
	}
	public void setNumeroSitio(String numeroSitio) {
		this.numeroSitio = numeroSitio;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public BigInteger getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(BigInteger idTicket) {
		this.idTicket = idTicket;
	}
	public BigInteger getIdNotaVenta() {
		return idNotaVenta;
	}
	public void setIdNotaVenta(BigInteger idNotaVenta) {
		this.idNotaVenta = idNotaVenta;
	}
	public BigInteger getIdResumenCaja() {
		return idResumenCaja;
	}
	public void setIdResumenCaja(BigInteger idResumenCaja) {
		this.idResumenCaja = idResumenCaja;
	}
	public BigInteger getFolio() {
		return folio;
	}
	public void setFolio(BigInteger folio) {
		this.folio = folio;
	}
	public BigInteger getFolioNC() {
		return folioNC;
	}
	public void setFolioNC(BigInteger folioNC) {
		this.folioNC = folioNC;
	}
	public String getSerieFolio() {
		return serieFolio;
	}
	public void setSerieFolio(String serieFolio) {
		this.serieFolio = serieFolio;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}

package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipoPago")
public class TipoPago {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private BigInteger idVenta;
	private BigInteger idNotaCredito;
    private int articuloId;
    private String articuloClave;  
	private String tipoPagoNombre; 
    private String tipoPagoClave;
    private BigDecimal ventaTipoPagoTotal;
    
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}	
	public BigInteger getIdNotaCredito() {
		return idNotaCredito;
	}
	public void setIdNotaCredito(BigInteger idNotaCredito) {
		this.idNotaCredito = idNotaCredito;
	}
	public int getArticuloId() {
		return articuloId;
	}
	public void setArticuloId(int articuloId) {
		this.articuloId = articuloId;
	}
	public String getArticuloClave() {
		return articuloClave;
	}
	public void setArticuloClave(String articuloClave) {
		this.articuloClave = articuloClave;
	}
	public String getTipoPagoNombre() {
		return tipoPagoNombre;
	}
	public void setTipoPagoNombre(String tipoPagoNombre) {
		this.tipoPagoNombre = tipoPagoNombre;
	}
	public String getTipoPagoClave() {
		return tipoPagoClave;
	}
	public void setTipoPagoClave(String tipoPagoClave) {
		this.tipoPagoClave = tipoPagoClave;
	}
	public BigDecimal getVentaTipoPagoTotal() {
		return ventaTipoPagoTotal;
	}
	public void setVentaTipoPagoTotal(BigDecimal ventaTipoPagoTotal) {
		this.ventaTipoPagoTotal = ventaTipoPagoTotal;
	}
}

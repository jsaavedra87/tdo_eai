package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cancelacionVenta")
public class CancelacionVenta {

	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private BigInteger idVenta;
	private BigInteger idTicket;
	private BigInteger idNotaVenta;
	private BigInteger idCancelaResumen;
	private Integer idCancelaCaja;
	private BigDecimal subTotal;
	private BigDecimal total;
	private String sucursal;
	private Date fechaCancelacion;
	private String cancelaCaja;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public BigInteger getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(BigInteger idTicket) {
		this.idTicket = idTicket;
	}	
	public BigInteger getIdNotaVenta() {
		return idNotaVenta;
	}
	public void setIdNotaVenta(BigInteger idNotaVenta) {
		this.idNotaVenta = idNotaVenta;
	}
	public BigInteger getIdCancelaResumen() {
		return idCancelaResumen;
	}
	public void setIdCancelaResumen(BigInteger idCancelaResumen) {
		this.idCancelaResumen = idCancelaResumen;
	}

	public Integer getIdCancelaCaja() {
		return idCancelaCaja;
	}
	public void setIdCancelaCaja(Integer idCancelaCaja) {
		this.idCancelaCaja = idCancelaCaja;
	}
	public BigDecimal getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public Date getFechaCancelacion() {
		return fechaCancelacion;
	}
	public void setFechaCancelacion(Date fechaCancelacion) {
		this.fechaCancelacion = fechaCancelacion;
	}
	public String getCancelaCaja() {
		return cancelaCaja;
	}
	public void setCancelaCaja(String cancelaCaja) {
		this.cancelaCaja = cancelaCaja;
	}
	
}

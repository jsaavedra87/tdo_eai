package com.tdo.integration.data.model.tdo;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name = "ARINTERFACELINESALL")
public class ArInterfaceLinesAll {

	private static final long serialVersionUID = 1L;
//	@Id
//	@Column
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private BigInteger Id;	
	private String TransactionBatchSourceName = "";
	private String TransactionTypeName = "";
	private String PaymentTerms = "";
	private String TransactionDate = "";
	private String AccountingDate = "";
	private String TransactionNumber = "";
	private String BilltoCustomerAccountNumber = "";
	private String BilltoCustomerSiteNumber = "";
	private String TransactionLineType = "";
	private String TransactionLineDescription = "";
	private String CurrencyCode = "";
	private String CurrencyConversionType = "";
	private String CurrencyConversionDate = "";
	private String CurrencyConversionRate = "";
	private String TransactionLineAmount = "";
	private String TransactionLineQuantity = "";
	private String UnitSellingPrice = "";
	private String LineTransactionsFlexfieldContext = "";
	private String LineTransactionsFlexfieldSegment1 = "";
	private String LineTransactionsFlexfieldSegment2 = "";
	private String LineTransactionsFlexfieldSegment3 = "";
	private String TaxClassificationCode = "";
	private String UnitofMeasureCode = "";
	private String TaxRegimeCode = "";
	private String Tax = "";
	private String TaxStatusCode = "";
	private String TaxRateCode = "";
	private String TaxRate = "";
	private String LinktoTransactionsFlexfieldContext = "";
	private String LinktoTransactionsFlexfieldSegment1 = "";
	private String LinktoTransactionsFlexfieldSegment2 = "";
	private String LinktoTransactionsFlexfieldSegment3 = "";
	private String InventoryItemNumber = "";
	private String OverrideAutoAccountingFlag = "";
	private String BusinessUnitName = "";
	private String End = "";
		
}

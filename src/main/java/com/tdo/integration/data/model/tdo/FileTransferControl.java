package com.tdo.integration.data.model.tdo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "FILETRANSFERCONTROL")
public class FileTransferControl {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int branchId;
	private String branch;
	private int idCC;
	private Date fromDate;
	private Date toDate;
	private Date lastUpdate;
	private String transactionType;
	private String status;
	private String fileBase64;
	private String fileZip;
	private String fileProcessId;
	private String fileStatus;
	
	@Type(type="text")
	private String log;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}	
	public int getIdCC() {
		return idCC;
	}
	public void setIdCC(int idCC) {
		this.idCC = idCC;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	public String getFileBase64() {
		return fileBase64;
	}
	public void setFileBase64(String fileBase64) {
		this.fileBase64 = fileBase64;
	}
	public String getFileZip() {
		return fileZip;
	}
	public void setFileZip(String fileZip) {
		this.fileZip = fileZip;
	}	
	public String getFileProcessId() {
		return fileProcessId;
	}
	public void setFileProcessId(String fileProcessId) {
		this.fileProcessId = fileProcessId;
	}	
	public String getFileStatus() {
		return fileStatus;
	}
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
}

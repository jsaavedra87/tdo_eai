package com.tdo.integration.data.model.tdo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "sales")
public class Sales {
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String transactionId;
	private BigInteger clienteNubeId;
	private Integer idCliente;	
	private String nombreCliente;
	private String curp;
	private BigInteger idVenta;
	private BigInteger idTicket;
	private BigInteger idNotaVenta;
	private Integer idCaja;
	private BigInteger idResumenCaja;
	private BigInteger idCancelaResumenCaja;
	private Integer idCancelaCaja;
	private Date fechaVenta;
	private Date ventaFecha;
	private String ventaCaja;
	private BigDecimal ventaSubtotal;
	private BigDecimal ventaDescuento;
	private BigDecimal ventaTotal;
	private Integer ventaStatus;
	private String ventaMoneda;
	private BigDecimal ventaTipoCambio;
	private BigDecimal monedaSubtotal; 
	private BigDecimal monedaDescuento; 
	private BigDecimal monedaTotal;
	private BigDecimal monedaCambio;
	private String serieFolio;
	private String emisor;
	private String rfcEmisor;
	private String receptor;
	private String rfcReceptor;
	private String moneda;
	private BigDecimal tipoCambio;
	private BigDecimal subTotal;
	private BigDecimal descuento;
	private BigDecimal cambio;
	private BigDecimal total;
	private BigDecimal extSubTotal;
	private BigDecimal extDescuento;
	private BigDecimal extTotal;
	private String formaPago;
	private String metodoPago;
	private Integer diasCredito;
	private String claveCliente;	
	private String nombreVendedor;	
	private String sucursal;
	private Integer precioCliente;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<ArticuloImpuesto> articuloImpuesto;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<TipoPago> tipoPago;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<DetalleLote> detalleLote;
	
	@OneToMany(cascade=CascadeType.ALL)
	List<Articulo> articulo;
	
	//JAvila: Articulos del kit
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(nullable=true)
	List<Articulo> articuloPaquete;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(nullable=true)
	List<DetalleLote> detalleLotePaquete;

	@Transient
	private Factura factura;
	
	@OneToOne(cascade=CascadeType.ALL)
	private NotaCredito notaCredito;
	
	@Transient
	private Empresa empresa;
	
	@Transient
	private Cliente cliente;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public BigInteger getClienteNubeId() {
		return clienteNubeId;
	}
	public void setClienteNubeId(BigInteger clienteNubeId) {
		this.clienteNubeId = clienteNubeId;
	}
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public BigInteger getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(BigInteger idVenta) {
		this.idVenta = idVenta;
	}
	public BigInteger getIdTicket() {
		return idTicket;
	}
	public void setIdTicket(BigInteger idTicket) {
		this.idTicket = idTicket;
	}	
	public BigInteger getIdNotaVenta() {
		return idNotaVenta;
	}
	public void setIdNotaVenta(BigInteger idNotaVenta) {
		this.idNotaVenta = idNotaVenta;
	}
	public Integer getIdCaja() {
		return idCaja;
	}
	public void setIdCaja(Integer idCaja) {
		this.idCaja = idCaja;
	}
	public Date getFechaVenta() {
		return fechaVenta;
	}
	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}
	public Date getVentaFecha() {
		return ventaFecha;
	}
	public void setVentaFecha(Date ventaFecha) {
		this.ventaFecha = ventaFecha;
	}	
	public String getVentaCaja() {
		return ventaCaja;
	}
	public void setVentaCaja(String ventaCaja) {
		this.ventaCaja = ventaCaja;
	}
	public BigDecimal getVentaSubtotal() {
		return ventaSubtotal;
	}
	public void setVentaSubtotal(BigDecimal ventaSubtotal) {
		this.ventaSubtotal = ventaSubtotal;
	}
	public BigDecimal getVentaDescuento() {
		return ventaDescuento;
	}
	public void setVentaDescuento(BigDecimal ventaDescuento) {
		this.ventaDescuento = ventaDescuento;
	}
	public BigDecimal getVentaTotal() {
		return ventaTotal;
	}
	public void setVentaTotal(BigDecimal ventaTotal) {
		this.ventaTotal = ventaTotal;
	}
	public Integer getVentaStatus() {
		return ventaStatus;
	}
	public void setVentaStatus(Integer ventaStatus) {
		this.ventaStatus = ventaStatus;
	}
	public String getVentaMoneda() {
		return ventaMoneda;
	}
	public void setVentaMoneda(String ventaMoneda) {
		this.ventaMoneda = ventaMoneda;
	}
	public BigDecimal getVentaTipoCambio() {
		return ventaTipoCambio;
	}
	public void setVentaTipoCambio(BigDecimal ventaTipoCambio) {
		this.ventaTipoCambio = ventaTipoCambio;
	}	
	public BigDecimal getMonedaSubtotal() {
		return monedaSubtotal;
	}
	public void setMonedaSubtotal(BigDecimal monedaSubtotal) {
		this.monedaSubtotal = monedaSubtotal;
	}
	public BigDecimal getMonedaDescuento() {
		return monedaDescuento;
	}
	public void setMonedaDescuento(BigDecimal monedaDescuento) {
		this.monedaDescuento = monedaDescuento;
	}
	public BigDecimal getMonedaTotal() {
		return monedaTotal;
	}
	public void setMonedaTotal(BigDecimal monedaTotal) {
		this.monedaTotal = monedaTotal;
	}
	public BigDecimal getMonedaCambio() {
		return monedaCambio;
	}
	public void setMonedaCambio(BigDecimal monedaCambio) {
		this.monedaCambio = monedaCambio;
	}
	public String getSerieFolio() {
		return serieFolio;
	}
	public void setSerieFolio(String serieFolio) {
		this.serieFolio = serieFolio;
	}
	public String getEmisor() {
		return emisor;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public String getRfcEmisor() {
		return rfcEmisor;
	}
	public void setRfcEmisor(String rfcEmisor) {
		this.rfcEmisor = rfcEmisor;
	}
	public String getReceptor() {
		return receptor;
	}
	public void setReceptor(String receptor) {
		this.receptor = receptor;
	}
	public String getRfcReceptor() {
		return rfcReceptor;
	}
	public void setRfcReceptor(String rfcReceptor) {
		this.rfcReceptor = rfcReceptor;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}
	public BigDecimal getDescuento() {
		return descuento;
	}
	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}	
	public BigDecimal getCambio() {
		return cambio;
	}
	public void setCambio(BigDecimal cambio) {
		this.cambio = cambio;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getExtSubTotal() {
		return extSubTotal;
	}
	public void setExtSubTotal(BigDecimal extSubTotal) {
		this.extSubTotal = extSubTotal;
	}
	public BigDecimal getExtDescuento() {
		return extDescuento;
	}
	public void setExtDescuento(BigDecimal extDescuento) {
		this.extDescuento = extDescuento;
	}
	public BigDecimal getExtTotal() {
		return extTotal;
	}
	public void setExtTotal(BigDecimal extTotal) {
		this.extTotal = extTotal;
	}
	public String getFormaPago() {
		return formaPago;
	}
	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	public String getMetodoPago() {
		return metodoPago;
	}
	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}
	public List<ArticuloImpuesto> getArticuloImpuesto() {
		return articuloImpuesto;
	}
	public void setArticuloImpuesto(List<ArticuloImpuesto> articuloImpuesto) {
		this.articuloImpuesto = articuloImpuesto;
	}
	public List<TipoPago> getTipoPago() {
		return tipoPago;
	}
	public void setTipoPago(List<TipoPago> tipoPago) {
		this.tipoPago = tipoPago;
	}
	public List<DetalleLote> getDetalleLote() {
		return detalleLote;
	}
	public void setDetalleLote(List<DetalleLote> detalleLote) {
		this.detalleLote = detalleLote;
	}
	public List<Articulo> getArticulo() {
		return articulo;
	}
	public void setArticulo(List<Articulo> articulo) {
		this.articulo = articulo;
	}
	public Factura getFactura() {
		return factura;
	}
	public void setFactura(Factura factura) {
		this.factura = factura;
	}
	public NotaCredito getNotaCredito() {
		return notaCredito;
	}
	public void setNotaCredito(NotaCredito notaCredito) {
		this.notaCredito = notaCredito;
	}
	public Integer getDiasCredito() {
		return diasCredito;
	}
	public void setDiasCredito(Integer diasCredito) {
		this.diasCredito = diasCredito;
	}
	public String getClaveCliente() {
		return claveCliente;
	}
	public void setClaveCliente(String claveCliente) {
		this.claveCliente = claveCliente;
	}
	public String getNombreVendedor() {
		return nombreVendedor;
	}
	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public BigInteger getIdResumenCaja() {
		return idResumenCaja;
	}
	public void setIdResumenCaja(BigInteger idResumenCaja) {
		this.idResumenCaja = idResumenCaja;
	}
	public BigInteger getIdCancelaResumenCaja() {
		return idCancelaResumenCaja;
	}
	public void setIdCancelaResumenCaja(BigInteger idCancelaResumenCaja) {
		this.idCancelaResumenCaja = idCancelaResumenCaja;
	}
	public Integer getIdCancelaCaja() {
		return idCancelaCaja;
	}
	public void setIdCancelaCaja(Integer idCancelaCaja) {
		this.idCancelaCaja = idCancelaCaja;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public List<Articulo> getArticuloPaquete() {
		return articuloPaquete;
	}
	public void setArticuloPaquete(List<Articulo> articuloPaquete) {
		this.articuloPaquete = articuloPaquete;
	}	
	public List<DetalleLote> getDetalleLotePaquete() {
		return detalleLotePaquete;
	}
	public void setDetalleLotePaquete(List<DetalleLote> detalleLotePaquete) {
		this.detalleLotePaquete = detalleLotePaquete;
	}
	public Integer getPrecioCliente() {
		return precioCliente;
	}
	public void setPrecioCliente(Integer precioCliente) {
		this.precioCliente = precioCliente;
	}	
	
}

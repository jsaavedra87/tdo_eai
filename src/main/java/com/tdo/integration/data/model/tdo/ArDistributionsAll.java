package com.tdo.integration.data.model.tdo;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name = "ARDISTRIBUTIONSALL")
public class ArDistributionsAll {

	private static final long serialVersionUID = 1L;
//	@Id
//	@Column
//	@GeneratedValue(strategy=GenerationType.AUTO)
	private BigInteger Id;
	private String SourceReference = "";
	private String AccountClass = "";
	private String Amount = "";
	private String Percent = "";
	private String LineTransactionsFlexfieldContext = "";
	private String LineTransactionsFlexfieldSegment1 = "";
	private String LineTransactionsFlexfieldSegment2 = "";
	private String LineTransactionsFlexfieldSegment3 = "";
	private String AccountingFlexfieldSegment1 = "";
	private String AccountingFlexfieldSegment2 = "";
	private String AccountingFlexfieldSegment3 = "";
	private String BusinessUnitName = "";
	private String End = "";
	
	
}

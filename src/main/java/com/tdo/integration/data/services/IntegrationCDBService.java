package com.tdo.integration.data.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dao.IntegrationTDODataDao;
import com.tdo.integration.data.model.tdo.FileTransferControl;
import com.tdo.integration.data.model.tdo.InventoryMonitor;
import com.tdo.integration.data.model.tdo.InventoryTransaction;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.dto.CreditDTO;
import com.tdo.integration.dto.InventoryMonitorDTO;
import com.tdo.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.dto.SalesDTO;
import com.tdo.integration.pojo.BaseResponse;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.ws.SOAPInventoryTransactionsService;

@Service("salesService")
public class IntegrationCDBService {
	
	@Autowired
	IntegrationTDODataDao integrationTDODataDao;
	
	@Autowired
	SOAPInventoryTransactionsService soapInventoryTransactionsService;
	
	public BaseResponse insertSales(SalesDTO sales) {
		BaseResponse response = new BaseResponse();
		StringBuilder message = new StringBuilder("");
		
		try {
			FileTransferControl ftc = this.getFileTransferControlByIdCC(sales.getFileTransferControl());
			
			if(ftc != null) {
				if(AppConstants.FILE_REQUEST_SUCCESS.equals(ftc.getStatus())) {					
	                response.setCode(200);
	                response.setStatus("SUCCESS");
	                response.setContent("El archivo ha sido enviado anteriormente con éxito. Log anterior: " + ftc.getLog());
	                return response;
	                
				} else if(AppConstants.FILE_REQUEST_NEW.equals(ftc.getStatus()) || AppConstants.FILE_REQUEST_SENT.equals(ftc.getStatus())){					
					message.append("El archivo ha sido enviado anteriormente sin éxito, se obtuvo con estatus de " + ftc.getStatus() + ", se procesará en el siguiente periodo. Log anterior: " + ftc.getLog() + "\r\r");
					this.setFileTransferControl(sales.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());					
	    			response.setCode(0);
	    			response.setStatus("ERROR");
	    			response.setContent(message.toString());
	    			return response;
				}
			}
			
			this.setFileTransferControl(sales.getFileTransferControl(), AppConstants.FILE_REQUEST_SENT, null);
			boolean result = integrationTDODataDao.saveSales(sales);
			
			if(result) {
				response.setCode(200);
				response.setStatus("SUCCESS");
				response.setContent("Las ventas del Corte Caja " + String.valueOf(sales.getFileTransferControl().getIdCC()) + " han sido registradas correctamente.");
				this.setFileTransferControl(sales.getFileTransferControl(), AppConstants.FILE_REQUEST_SUCCESS, response.getContent());
			}else {
				response.setCode(0);
				response.setStatus("ERROR");
				response.setContent("Ha ocurrido un error al registrar las ventas del Corte de Caja "  + String.valueOf(sales.getFileTransferControl().getIdCC()));
				this.setFileTransferControl(sales.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, response.getContent());
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			response.setCode(0);
			response.setStatus("ERROR");
			response.setContent("Ocurrió un error en el servicio web de envío de Ventas Centralizadas: " + e.getMessage());
			this.setFileTransferControl(sales.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, response.getContent());
		}
		return response;	
	}
	
	public BaseResponse updateInventoryMonitor(InventoryMonitorDTO inventoryMonitorDTO) {
		BaseResponse response = new BaseResponse();
		InventoryMonitor inventoryMonitor = new InventoryMonitor();
		BigDecimal cloudCount = new BigDecimal(0);
		boolean result = false;
		
 		try {
 			inventoryMonitor.setBranchId(inventoryMonitorDTO.getBranchId());
 			inventoryMonitor.setBranchName(inventoryMonitorDTO.getBranchName());
 			inventoryMonitor.setBranchCode(inventoryMonitorDTO.getBranchCode());
 			inventoryMonitor.setItemCount(inventoryMonitorDTO.getItemCount());
 			inventoryMonitor.setLotCount(inventoryMonitorDTO.getLotCount());
			
			cloudCount = soapInventoryTransactionsService.getCloudInventoryCount(inventoryMonitorDTO.getBranchCode());
 			
			if(!cloudCount.equals(new BigDecimal(0))) {
				
				if((new BigDecimal(String.format("%.2f", inventoryMonitor.getItemCount()))).compareTo(new BigDecimal(String.format("%.2f",cloudCount))) == 0 
					&& (new BigDecimal(String.format("%.2f", inventoryMonitor.getLotCount()))).compareTo(new BigDecimal(String.format("%.2f",cloudCount))) == 0) {
					inventoryMonitor.setStatus(AppConstants.INVENTORY_STATUS_OK);
				} else {
					inventoryMonitor.setStatus(AppConstants.INVENTORY_STATUS_UNESPECTED);
				}
				
				inventoryMonitor.setCloudCount(cloudCount);
				inventoryMonitor.setLastUpdate(new Date());				
				result = integrationTDODataDao.saveInventoryMonitor(inventoryMonitor);
			} 			 						
			
			if(result) {
				response.setCode(200);
				response.setStatus("SUCCESS");
				response.setContent("El monitoreo del inventario de la sucursal " + inventoryMonitorDTO.getBranchName() + " ha sido actualizado correctamente.");
			}else {
				response.setCode(0);
				response.setStatus("ERROR");
				response.setContent("Ha ocurrido un error al actualizar el monitoreo del inventario de la sucursal "  + inventoryMonitorDTO.getBranchName());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCode(0);
			response.setStatus("ERROR");
			response.setContent("Ocurrió un error en el servicio web de envío de Monitoreo de Inventario: " + e.getMessage());
		} 		
 		
 		return response;
	}

	public List<CreditDTO> getCustomerCreditByName(String value) {
		return integrationTDODataDao.getCustomerCreditByName(value);
	}
	
	
	public List<CreditDTO> updateCustomerCredit(List<CreditDTO> lstCreditDTO) {
		
		BigDecimal absoluteCreditLimit;
		BigDecimal localCreditAmount;
		BigDecimal localPaymentAmount;
		BigDecimal localPaymentCreditNoteAmount;
		BigDecimal localPaymentsAmount;
		BigDecimal localCreditAmountUsed;
        BigDecimal absoluteCreditAmount;
		BigDecimal absolutePaymentAmount;
		BigDecimal absolutePaymentCreditNoteAmount;        
        BigDecimal absolutePaymentsAmount;        	           
        BigDecimal absoluteAvailableCredit;        
		
		integrationTDODataDao.saveOrUpdateCredit(lstCreditDTO);
		
		if(lstCreditDTO != null && lstCreditDTO.size() > 0) {
			for(CreditDTO creditDTO : lstCreditDTO) {
				
				creditDTO.getCliente().setClienteLimite(new BigDecimal(0));				
				absoluteCreditLimit = new BigDecimal(0);
				localCreditAmount = new BigDecimal(0);
				localPaymentAmount = new BigDecimal(0);
				localPaymentCreditNoteAmount = new BigDecimal(0);
				localPaymentsAmount = new BigDecimal(0);
				localCreditAmountUsed = new BigDecimal(0);
				absoluteCreditAmount = new BigDecimal(0);
				absolutePaymentAmount = new BigDecimal(0);
				absolutePaymentCreditNoteAmount = new BigDecimal(0);				
				absolutePaymentsAmount = new BigDecimal(0);				
				absoluteAvailableCredit = new BigDecimal(0);

				absoluteCreditLimit = creditDTO.getCliente().getClienteLimiteAbsoluto() != null ? creditDTO.getCliente().getClienteLimiteAbsoluto() : new BigDecimal(0);				
				absoluteCreditAmount = integrationTDODataDao.getCustomerCreditAmount("", creditDTO.getCliente());
	            
	            if(absoluteCreditAmount.compareTo(new BigDecimal(0)) != 0) {
					
		            absolutePaymentAmount = integrationTDODataDao.getCustomerPaymentAmount("", creditDTO.getCliente());
		            absolutePaymentCreditNoteAmount = integrationTDODataDao.getCustomerCreditNoteAmount("", creditDTO.getCliente());
		            absolutePaymentsAmount = absolutePaymentAmount.add(absolutePaymentCreditNoteAmount);
		            				
					localCreditAmount = integrationTDODataDao.getCustomerCreditAmount(creditDTO.getSucursal(), creditDTO.getCliente());
					
					localPaymentAmount = integrationTDODataDao.getCustomerPaymentAmount(creditDTO.getSucursal(), creditDTO.getCliente());
					localPaymentCreditNoteAmount = integrationTDODataDao.getCustomerCreditNoteAmount(creditDTO.getSucursal(), creditDTO.getCliente());
					localPaymentsAmount = localPaymentAmount.add(localPaymentCreditNoteAmount);
					
					localCreditAmountUsed = localCreditAmount.subtract(localPaymentsAmount);				
		            absoluteAvailableCredit = absoluteCreditLimit.subtract(absoluteCreditAmount.subtract(absolutePaymentsAmount));
		            creditDTO.getCliente().setClienteLimite(localCreditAmountUsed.add(absoluteAvailableCredit));
	            } else {
	            	creditDTO.getCliente().setClienteLimite(absoluteCreditLimit);
	            }
			}
		}
		
		return lstCreditDTO;
	}

	public BaseResponse updateCustomerCreditLimit(CreditDTO creditDTO) {
		BaseResponse response = new BaseResponse();		
        BigDecimal absoluteCreditAmount;
        BigDecimal absoluteCreditAmountUsed;
		BigDecimal absolutePaymentAmount;
		BigDecimal absolutePaymentCreditNoteAmount;
		BigDecimal absoluteDebitAmount;
		
		absoluteCreditAmount = new BigDecimal(0);		
		absoluteCreditAmountUsed = new BigDecimal(0);
		absolutePaymentAmount = new BigDecimal(0);
		absolutePaymentCreditNoteAmount = new BigDecimal(0);
		absoluteDebitAmount = new BigDecimal(0);
		
        absoluteCreditAmount = integrationTDODataDao.getCustomerCreditAmount("", creditDTO.getCliente());        
        absolutePaymentAmount = integrationTDODataDao.getCustomerPaymentAmount("", creditDTO.getCliente());
        absolutePaymentCreditNoteAmount = integrationTDODataDao.getCustomerCreditNoteAmount("", creditDTO.getCliente());
        absoluteDebitAmount = absolutePaymentAmount.add(absolutePaymentCreditNoteAmount);
        absoluteCreditAmountUsed = absoluteCreditAmount.subtract(absoluteDebitAmount);

        if(creditDTO.getCliente().getClienteLimiteAbsoluto().compareTo(absoluteCreditAmountUsed) > 0) {
        	boolean result = integrationTDODataDao.updateCreditLimit(creditDTO);
        	
    		if(result) {
    			response.setStatus("OK");
    			response.setContent("El credito se ha actualizado correctamente.");
    		}else {
    			response.setStatus("ERROR");
    			response.setContent("Ha ocurrido un error al actualizar el credito. Intentelo nuevamente.");
    		}
        } else {
			response.setStatus("ERROR");
			response.setContent("El nuevo limite de credito no puede ser menor al credito utilizado actualmente por el cliente.");        	
        }
		
		return response;	
	}
	
	public BaseResponse saveOrUpdateUdc(Udc udc) throws Exception {
		BaseResponse response = new BaseResponse();

		boolean result=false;
		if(udc!=null) {
			result = integrationTDODataDao.saveOrUpdateUdc(udc);
		}

		if(result) {
			response.setStatus("OK");
			response.setCode(200);
			response.setContent("se ha actualizado correctamente la UDC");
		}else {
			response.setStatus("ERROR");
			response.setContent("Ha ocurrido un error al registrar la UDC");
		}
		return response;	
		
	}
	
	public List<Udc> getUdcByUdcKey(String udcKey) {
		return integrationTDODataDao.getUdcByUdcKey(udcKey);
	}

	@SuppressWarnings("unused")
	public List<InventoryTransactionDTO> getInventoryTransactions(String request) {
		List<InventoryTransaction> lstIT = new ArrayList<InventoryTransaction>();
		List<InventoryTransactionDTO> lstITDTO = new ArrayList<InventoryTransactionDTO>();
		InventoryTransactionDTO itDTO = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String branch = "";
		Date dateFrom = new Date();
		Date dateTo = new Date();
		
		try {
			String[] str = request.split("/");
			
			if (str.length == 3) {
				branch = str[0];
				dateFrom = sdf.parse(str[1].replace("_", " "));
				dateTo = sdf.parse(str[2].replace("_", " "));
				
			}else if (str.length == 2) {
				branch = str[0];
				dateFrom =sdf.parse(str[1].replace("_", " "));
				
			} else {
				return null;
			}
			
			lstIT = integrationTDODataDao.getInventoryTransaction(branch, dateFrom);
			
			if(lstIT != null && lstIT.size() > 0) {				
				for(InventoryTransaction it : lstIT) {
					try {
						itDTO = new InventoryTransactionDTO();
						
						if(it.getCreationDate() != null) {
							itDTO.setCreationDate(getXmlGregorianCalendarFromDate(it.getCreationDate()));
						}
						if(it.getExpirationDate() != null) {
							itDTO.setExpirationDate(getXmlGregorianCalendarFromDate(it.getExpirationDate()));
						}
						if(it.getTransactionDate() != null) {
							itDTO.setTransactionDate(getXmlGregorianCalendarFromDate(it.getTransactionDate()));
						}
						if(it.getTransactionRealDate() != null) {
							itDTO.setTransactionRealDate(getXmlGregorianCalendarFromDate(it.getTransactionRealDate()));
						}
						
						itDTO.setAverageCost(it.getAverageCost());
						itDTO.setInventoryLocation(it.getInventoryLocation());
						itDTO.setInventoryOrganizationCode(it.getInventoryOrganizationCode());
						itDTO.setInventoryOrganizationName(it.getInventoryOrganizationName());
						itDTO.setItemDescription(it.getItemDescription());
						itDTO.setItemId(it.getItemId());
						itDTO.setItemName(it.getItemName());
						itDTO.setItemType(it.getItemType());
						itDTO.setLotNumber(it.getLotNumber());
						itDTO.setQuantity(it.getQuantity());
						itDTO.setSubInventory(it.getSubInventory());						
						itDTO.setTransactionId(it.getTransactionId());						
						itDTO.setTransactionType(it.getTransactionType());
						itDTO.setUom(it.getUom());
						
						lstITDTO.add(itDTO);						
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
		return lstITDTO;
	}
	public void newInventoryTransactionRecord() {
		List<Udc> listUdc = getUdcByUdcKey(AppConstants.KEY_INVENTORY_DAYS_ADD);
		List<Udc> listUdcCedi = getUdcByUdcKey(AppConstants.KEY_CURRENT_CODE_CEDI);
		List<InventoryTransactionDTO> lstITDTO = null;
		InventoryTransaction it = null;
		String branch = "";
		String dateTo = "";
		String request = "";
		String cediCode = "";
		int inventoryDaysAdd = -1;
		
		try {
			inventoryDaysAdd = (listUdc != null && !listUdc.isEmpty() && listUdc.get(0).getUdcValue() != null && !listUdc.get(0).getUdcValue().isEmpty()) ? Integer.valueOf(listUdc.get(0).getUdcValue()) : -1;
			cediCode = (listUdcCedi != null && !listUdcCedi.isEmpty() && listUdcCedi.get(0).getUdcValue() != null && !listUdcCedi.get(0).getUdcValue().isEmpty()) ? listUdcCedi.get(0).getUdcValue() : "";
			dateTo = dateCalculate(new Date(), inventoryDaysAdd).substring(0, 10) + " 00:00:00";			
			request = new StringBuilder(branch).append("/").append(dateTo).toString();			
			lstITDTO = soapInventoryTransactionsService.getInventoryTransactionTransfers(request, cediCode);
			
			if(lstITDTO != null && lstITDTO.size() > 0) {				
				for(InventoryTransactionDTO itDTO : lstITDTO) {
					try {
						it = new InventoryTransaction();
						
						if(itDTO.getCreationDate() != null) {
							it.setCreationDate(itDTO.getCreationDate().toGregorianCalendar().getTime());
						}
						if(itDTO.getExpirationDate() != null) {
							it.setExpirationDate(itDTO.getExpirationDate().toGregorianCalendar().getTime());
						}
						if(itDTO.getTransactionDate() != null) {
							it.setTransactionDate(itDTO.getTransactionDate().toGregorianCalendar().getTime());
						}
						if(itDTO.getTransactionRealDate() != null) {
							it.setTransactionRealDate(itDTO.getTransactionRealDate().toGregorianCalendar().getTime());
						}
						
						it.setAverageCost(itDTO.getAverageCost());
						it.setInventoryLocation(itDTO.getInventoryLocation());
						it.setInventoryOrganizationCode(itDTO.getInventoryOrganizationCode());
						it.setInventoryOrganizationName(itDTO.getInventoryOrganizationName());
						it.setItemDescription(itDTO.getItemDescription());
						it.setItemId(itDTO.getItemId());
						it.setItemName(itDTO.getItemName());
						it.setItemType(itDTO.getItemType());						
						it.setLotNumber(itDTO.getLotNumber());
						it.setQuantity(itDTO.getQuantity());
						it.setSubInventory(itDTO.getSubInventory());						
						it.setTransactionId(itDTO.getTransactionId());						
						it.setTransactionType(itDTO.getTransactionType());
						it.setUom(itDTO.getUom());
						it.setLastUpdate(new Date());
						
						integrationTDODataDao.saveInventoryTransaction(it);						
					} catch (Exception e) {
						e.printStackTrace();
						continue;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static String dateCalculate(Date date, int days) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");		
	    Calendar cal = Calendar.getInstance();
	    String resultDate = sdf.format(new Date());
	    
	    try {
	    	
	        cal.setTime(date);	        
	        cal.add(Calendar.DATE, days);
	        resultDate = sdf.format(cal.getTime());
	        
	    } catch(Exception e){
	    	e.printStackTrace();
	    }
	    
	    return resultDate;
	}
	
	
	public static XMLGregorianCalendar getXmlGregorianCalendarFromDate(Date date){
	    try {
	        GregorianCalendar calendar = new GregorianCalendar();
	        calendar.setTime(date);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	    return null;
	}
	
	public FileTransferControl setFileTransferControl(FileTransferControl ftc, String status, String log) {
		
		try 
		{	
			ftc.setLastUpdate(new Date());
			ftc.setStatus(status);
			ftc.setLog(log);
			
			integrationTDODataDao.saveOrUpdateFileTransferControl(ftc);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ftc;
	}
	
	public FileTransferControl getFileTransferControlByDateRange(FileTransferControl ftc) {
		List<FileTransferControl> lstFTC = null;
		FileTransferControl o = null;
		
		try 
		{
			lstFTC = integrationTDODataDao.getFileTransferControlByFromDate(ftc);
			if(lstFTC != null && lstFTC.size() > 0) {
				o = lstFTC.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}
	
	public FileTransferControl getFileTransferControlByIdCC(FileTransferControl ftc) {
		List<FileTransferControl> lstFTC = null;
		FileTransferControl o = null;
		
		try 
		{
			lstFTC = integrationTDODataDao.getFileTransferControlByIdCC(ftc);
			if(lstFTC != null && lstFTC.size() > 0) {
				o = lstFTC.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}	
}

package com.tdo.integration.data.services;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.sql.RowSet;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.tdo.integration.dao.IntegrationTDODataDao;
import com.tdo.integration.data.model.tdo.CustomerAdditionalSpecs;
import com.tdo.integration.data.model.tdo.FileTransferControl;
import com.tdo.integration.dto.ArLockBoxImportC;
import com.tdo.integration.dto.GLInterface;
import com.tdo.integration.dto.GLInterfaceV3;
import com.tdo.integration.dto.InventoryTransaction;
import com.tdo.integration.dto.InventoryTransactionCost;
import com.tdo.integration.dto.InventoryTransactionHeader;
import com.tdo.integration.dto.InventoryTransactionLots;
import com.tdo.integration.dto.InventoryTransactionV1;
import com.tdo.integration.dto.RaInterface;
import com.tdo.integration.dto.RaInterfaceDistributionsAll;
import com.tdo.integration.dto.RaInterfaceLinesAll;
import com.tdo.integration.pojo.BaseResponse;
import com.tdo.integration.services.HTTPRequestService;
import com.tdo.integration.test.Row;
import com.tdo.integration.test.Rowset;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.ws.PayloadProducer;
import com.tdo.integration.ws.SOAPFileImportService;

@Service("integrationRestServiceTDO")
public class IntegrationRestServiceTDO {
	
	@Autowired
	IntegrationTDODataDao integrationTDODataDao;
	
	@Autowired
	SOAPFileImportService SOAPFileImportService;
	
	@Autowired
	HTTPRequestService HTTPRequestService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BaseResponse processInventoryFile(InventoryTransaction it) {		
		BaseResponse response = new BaseResponse();
		InventoryTransactionV1 data = it.getInventoryTransactionV1();
		StringBuilder message = new StringBuilder("");
		
		try {
			
			FileTransferControl ftc = this.getFileTransferControlByDateRange(it.getFileTransferControl());
			
			if(ftc != null) {
				if(AppConstants.FILE_REQUEST_SUCCESS.equals(ftc.getStatus())) {					
	                response.setCode(200);
	                response.setStatus("SUCCESS");
	                response.setContent("El archivo ha sido enviado anteriormente con éxito. Log anterior: " + ftc.getLog());
	                return response;
	                
				} else if(AppConstants.FILE_REQUEST_NEW.equals(ftc.getStatus()) || AppConstants.FILE_REQUEST_SENT.equals(ftc.getStatus())){					
					message.append("El archivo ha sido enviado anteriormente sin éxito, se obtuvo con estatus de " + ftc.getStatus() + ", se procesará en el siguiente periodo. Log anterior: " + ftc.getLog() + "\r\r");
					this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());					
	    			response.setCode(0);
	    			response.setStatus("ERROR");
	    			response.setContent(message.toString());
	    			return response;
				}
			}
			
			if(AppConstants.FILE_REQUEST_ERROR.equals(it.getFileTransferControl().getStatus())) {
				message.append(it.getFileTransferControl().getLog());									
    			response.setCode(0);
    			response.setStatus("ERROR");
    			response.setContent(message.toString());
    			this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());
    			return response;
			}
			
			this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_NEW, null);
			SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMddHHmmss");
			String stringBranch = String.format("%02d", it.getFileTransferControl().getBranchId());
			String stringDate = sfd.format(it.getFileTransferControl().getFromDate());			
			String stringFileName = new StringBuilder(stringBranch).append("_").append(stringDate).toString();
			
			List<InventoryTransactionHeader> ith = data.getInventoryTransactionHeader();
			List<InventoryTransactionLots> itl = data.getInventoryTransactionLots();
			List<InventoryTransactionCost> itc = data.getInventoryTransactionCost();

			File tempHdr = File.createTempFile("tempHdr", ".tmp"); 
			BufferedWriter bw = new BufferedWriter(new FileWriter(tempHdr));
			 
			ColumnPositionMappingStrategy mappingStrategy= new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(InventoryTransactionHeader.class); 
	        String[] columnsIth = new String[]  
	        		{"ORGANIZATION_NAME","TRANSACTION_GROUP_ID","TRANSACTION_GROUP_SEQ","TRANSACTION_BATCH_ID",
	        		 "TRANSACTION_BATCH_SEQ","PROCESS_FLAG","INVENTORY_ITEM","ITEM_NUMBER","REVISION",
	        		 "INV_LOTSERIAL_INTERFACE_NUM","SUBINVENTORY_CODE","LOCATOR_NAME","LOC_SEGMENT1","LOC_SEGMENT2",
	        		 "LOC_SEGMENT3","LOC_SEGMENT4","LOC_SEGMENT5","LOC_SEGMENT6","LOC_SEGMENT7","LOC_SEGMENT8",
	        		 "LOC_SEGMENT9","LOC_SEGMENT10","LOC_SEGMENT11","LOC_SEGMENT12","LOC_SEGMENT13","LOC_SEGMENT14",
	        		 "LOC_SEGMENT15","LOC_SEGMENT16","LOC_SEGMENT17","LOC_SEGMENT18","LOC_SEGMENT19","LOC_SEGMENT20",
	        		 "TRANSACTION_QUANTITY","TRANSACTION_UOM","TRANSACTION_UNIT_OF_MEASURE","RESERVATION_QUANTITY",
	        		 "TRANSACTION_DATE","TRANSACTION_SOURCE_TYPE_NAME","TRANSACTION_TYPE_NAME","TRANSFER_ORGANIZATION_TYPE",
	        		 "TRANSFER_ORGANIZATION_NAME","TRANSFER_SUBINVENTORY","XFER_LOC_SEGMENT1","XFER_LOC_SEGMENT2",
	        		 "XFER_LOC_SEGMENT3","XFER_LOC_SEGMENT4","XFER_LOC_SEGMENT5","XFER_LOC_SEGMENT6","XFER_LOC_SEGMENT7",
	        		 "XFER_LOC_SEGMENT8","XFER_LOC_SEGMENT9","XFER_LOC_SEGMENT10","XFER_LOC_SEGMENT11","XFER_LOC_SEGMENT12",
	        		 "XFER_LOC_SEGMENT13","XFER_LOC_SEGMENT14","XFER_LOC_SEGMENT15","XFER_LOC_SEGMENT16",
	        		 "XFER_LOC_SEGMENT17","XFER_LOC_SEGMENT18","XFER_LOC_SEGMENT19","XFER_LOC_SEGMENT20","PRIMARY_QUANTITY",
	        		 "SECONDARY_TRANSACTION_QUANTITY","SECONDARY_UOM_CODE","SECONDARY_UNIT_OF_MEASURE","SOURCE_CODE",
	        		 "SOURCE_HEADER_ID","SOURCE_LINE_ID","TRANSACTION_SOURCE_NAME","DSP_SEGMENT1","DSP_SEGMENT2",
	        		 "DSP_SEGMENT3","DSP_SEGMENT4","DSP_SEGMENT5","DSP_SEGMENT6","DSP_SEGMENT7","DSP_SEGMENT8",
	        		 "DSP_SEGMENT9","DSP_SEGMENT10","DSP_SEGMENT11","DSP_SEGMENT12","DSP_SEGMENT13","DSP_SEGMENT14",
	        		 "DSP_SEGMENT15","DSP_SEGMENT16","DSP_SEGMENT17","DSP_SEGMENT18","DSP_SEGMENT19","DSP_SEGMENT20",
	        		 "DSP_SEGMENT21","DSP_SEGMENT22","DSP_SEGMENT23","DSP_SEGMENT24","DSP_SEGMENT25","DSP_SEGMENT26",
	        		 "DSP_SEGMENT27","DSP_SEGMENT28","DSP_SEGMENT29","DSP_SEGMENT30","TRANSACTION_ACTION_NAME",
	        		 "TRANSACTION_MODE","LOCK_FLAG","TRANSACTION_REFERENCE","REASON_NAME","CURRENCY_NAME","CURRENCY_CODE",
	        		 "CURRENCY_CONVERSION_TYPE","CURRENCY_CONVERSION_RATE","CURRENCY_CONVERSION_DATE","TRANSACTION_COST",
	        		 "TRANSFER_COST","NEW_AVERAGE_COST","VALUE_CHANGE","PERCENTAGE_CHANGE","DST_SEGMENT1","DST_SEGMENT2",
	        		 "DST_SEGMENT3","DST_SEGMENT4","DST_SEGMENT5","DST_SEGMENT6","DST_SEGMENT7","DST_SEGMENT8","DST_SEGMENT9",
	        		 "DST_SEGMENT10","DST_SEGMENT11","DST_SEGMENT12","DST_SEGMENT13","DST_SEGMENT14","DST_SEGMENT15",
	        		 "DST_SEGMENT16","DST_SEGMENT17","DST_SEGMENT18","DST_SEGMENT19","DST_SEGMENT20","DST_SEGMENT21",
	        		 "DST_SEGMENT22","DST_SEGMENT23","DST_SEGMENT24","DST_SEGMENT25","DST_SEGMENT26","DST_SEGMENT27",
	        		 "DST_SEGMENT28","DST_SEGMENT29","DST_SEGMENT30","LOCATION_TYPE","EMPLOYEE_CODE","RECEIVING_DOCUMENT",
	        		 "LINE_ITEM_NUM","SHIPMENT_NUMBER","TRANSPORTATION_COST","CONTAINERS","WAYBILL_AIRBILL",
	        		 "EXPECTED_ARRIVAL_DATE","REQUIRED_FLAG","SHIPPABLE_FLAG","SHIPPED_QUANTITY","VALIDATION_REQUIRED",
	        		 "NEGATIVE_REQ_FLAG","OWNING_TP_TYPE","TRANSFER_OWNING_TP_TYPE","OWNING_ORGANIZATION_NAME",
	        		 "XFR_OWNING_ORGANIZATION_NAME","TRANSFER_PERCENTAGE","PLANNING_TP_TYPE","TRANSFER_PLANNING_TP_TYPE",
	        		 "ROUTING_REVISION","ROUTING_REVISION_DATE","ALTERNATE_BOM_DESIGNATOR","ALTERNATE_ROUTING_DESIGNATOR",
	        		 "ORGANIZATION_TYPE","USSGL_TRANSACTION_CODE","WIP_ENTITY_TYPE","SCHEDULE_UPDATE_CODE","SETUP_TEARDOWN_CODE",
	        		 "PRIMARY_SWITCH","MRP_CODE","OPERATION_SEQ_NUM","WIP_SUPPLY_TYPE","RELIEVE_RESERVATIONS_FLAG",
	        		 "RELIEVE_HIGH_LEVEL_RSV_FLAG","TRANSFER_PRICE","BUILD_BREAK_TO_UOM","BUILD_BREAK_TO_UNIT_OF_MEASURE",
	        		 "ATTRIBUTE_CATEGORY","ATTRIBUTE1","ATTRIBUTE2","ATTRIBUTE3","ATTRIBUTE4","ATTRIBUTE5","ATTRIBUTE6",
	        		 "ATTRIBUTE7","ATTRIBUTE8","ATTRIBUTE9","ATTRIBUTE10","ATTRIBUTE11","ATTRIBUTE12","ATTRIBUTE13",
	        		 "ATTRIBUTE14","ATTRIBUTE15","ATTRIBUTE16","ATTRIBUTE17","ATTRIBUTE18","ATTRIBUTE19","ATTRIBUTE20",
	        		 "ATTRIBUTE_NUMBER1","ATTRIBUTE_NUMBER2","ATTRIBUTE_NUMBER3","ATTRIBUTE_NUMBER4","ATTRIBUTE_NUMBER5",
	        		 "ATTRIBUTE_NUMBER6","ATTRIBUTE_NUMBER7","ATTRIBUTE_NUMBER8","ATTRIBUTE_NUMBER9","ATTRIBUTE_NUMBER10",
	        		 "ATTRIBUTE_DATE1","ATTRIBUTE_DATE2","ATTRIBUTE_DATE3","ATTRIBUTE_DATE4","ATTRIBUTE_DATE5",
	        		 "ATTRIBUTE_TIMESTAMP1","ATTRIBUTE_TIMESTAMP2","ATTRIBUTE_TIMESTAMP3","ATTRIBUTE_TIMESTAMP4",
	        		 "ATTRIBUTE_TIMESTAMP5","TRANSACTION_COST_IDENTIFIER","DEFAULT_TAXATION_COUNTRY","DOCUMENT_SUB_TYPE",
	        		 "TRX_BUSINESS_CATEGORY","USER_DEFINED_FISC_CLASS","TAX_INVOICE_NUMBER","TAX_INVOICE_DATE",
	        		 "PRODUCT_CATEGORY","PRODUCT_TYPE","ASSESSABLE_VALUE","TAX_CLASSIFICATION_CODE","EXEMPT_CERTIFICATE_NUMBER",
	        		 "EXEMPT_REASON_CODE","INTENDED_USE","FIRST_PTY_NUMBER","THIRD_PTY_NUMBER","FINAL_DISCHARGE_LOC_CODE",
	        		 "CATEGORY_NAME","OWNING_ORGANIZATION_ID","XFR_OWNING_ORGANIZATION_ID","PRC_BU_NAME","VENDOR_NAME",
	        		 "VENDOR_NUMBER","CONSIGNMENT_AGREEMENT_NUM","USE_CURRENT_COST","EXTERNAL_SYSTEM_PACKING_UNIT",
	        		 "TRANSFER_LOCATOR_NAME"};
			
	        mappingStrategy.setColumnMapping(columnsIth); 
	        StatefulBeanToCsvBuilder<InventoryTransactionHeader> builder=  new StatefulBeanToCsvBuilder(bw); 
            StatefulBeanToCsv beanWriter =  builder.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();beanWriter.write(ith); 
            bw.flush();
            FileInputStream streamHdr = new FileInputStream(tempHdr);
            FileInputStream streamHdr2 = new FileInputStream(tempHdr);
            bw.close();
            tempHdr.delete();

                        
            File tempLot = File.createTempFile("tempLot", ".tmp"); 
			bw = new BufferedWriter(new FileWriter(tempLot));
			
			mappingStrategy = new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(InventoryTransactionLots.class); 
	        String[] columnsItl = new String[] {"LOT_INTERFACE_NBR","INV_SERIAL_NBR","SOURCE_CODE","SOURCE_LINE_ID","LOT_NUMBER",
	        		"DESCRIPTION","LOT_EXPIRATION_DATE","TRANSACTION_QUANTITY","PRIMARY_QUANTITY","ORIGINATION_TYPE","ORIGINATION_DATE",
	        		"STATUS_CODE","RETEST_DATE","EXPIRATION_ACTION_NAME","EXPIRATION_ACTION_CODE","EXPIRATION_ACTION_DATE",
	        		"HOLD_DATE","MATURITY_DATE","DATE_CODE","GRADE_CODE","CHANGE_DATE","AGE","REASON_CODE","REASON_NAME",
	        		"PROCESS_FLAG","SUPPLIER_LOT_NUMBER","TERRITORY_CODE","TERRITORY_SHORT_NAME","ITEM_SIZE","COLOR","LOT_VOLUME",
	        		"VOLUME_UOM_NAME","VOLUME_UOM","PLACE_OF_ORIGIN","BEST_BY_DATE","LOT_LENGTH","LENGTH_UOM","LENGTH_UOM_NAME",
	        		"RECYCLED_CONTENT","LOT_THICKNESS","THICKNESS_UOM","LOT_WIDTH","WIDTH_UOM","WIDTH_UOM_NAME","CURL_WRINKLE_FOLD",
	        		"VENDOR_NAME","PRODUCT_CODE","PRODUCT_TRANSACTION_ID","SECONDARY_TRANSACTION_QUANTITY","SUBLOT_NUM",
	        		"PARENT_LOT_NUMBER","PARENT_OBJECT_TYPE","PARENT_OBJECT_NUMBER","PARENT_OBJECT_TYPE2","PARENT_OBJECT_NUMBER2",
	        		"LOT_ATTRIBUTE_CATEGORY","C_ATTRIBUTE1","C_ATTRIBUTE2","C_ATTRIBUTE3","C_ATTRIBUTE4","C_ATTRIBUTE5",
	        		"C_ATTRIBUTE6","C_ATTRIBUTE7","C_ATTRIBUTE8","C_ATTRIBUTE9","C_ATTRIBUTE10","C_ATTRIBUTE11","C_ATTRIBUTE12",
	        		"C_ATTRIBUTE13","C_ATTRIBUTE14","C_ATTRIBUTE15","C_ATTRIBUTE16","C_ATTRIBUTE17","C_ATTRIBUTE18","C_ATTRIBUTE19",
	        		"C_ATTRIBUTE20","D_ATTRIBUTE1","D_ATTRIBUTE2","D_ATTRIBUTE3","D_ATTRIBUTE4","D_ATTRIBUTE5","D_ATTRIBUTE6",
	        		"D_ATTRIBUTE7","D_ATTRIBUTE8","D_ATTRIBUTE9","D_ATTRIBUTE10","N_ATTRIBUTE1","N_ATTRIBUTE2","N_ATTRIBUTE3",
	        		"N_ATTRIBUTE4","N_ATTRIBUTE5","N_ATTRIBUTE6","N_ATTRIBUTE7","N_ATTRIBUTE8","N_ATTRIBUTE9","N_ATTRIBUTE10",
	        		"T_ATTRIBUTE1","T_ATTRIBUTE2","T_ATTRIBUTE3","T_ATTRIBUTE4","T_ATTRIBUTE5","ATTRIBUTE_CATEGORY","ATTRIBUTE1",
	        		"ATTRIBUTE2","ATTRIBUTE3","ATTRIBUTE4","ATTRIBUTE5","ATTRIBUTE6","ATTRIBUTE7","ATTRIBUTE8","ATTRIBUTE9",
	        		"ATTRIBUTE10","ATTRIBUTE11","ATTRIBUTE12","ATTRIBUTE13","ATTRIBUTE14","ATTRIBUTE15","ATTRIBUTE16","ATTRIBUTE17",
	        		"ATTRIBUTE18","ATTRIBUTE19","ATTRIBUTE20","ATTRIBUTE_NUMBER1","ATTRIBUTE_NUMBER2","ATTRIBUTE_NUMBER3",
	        		"ATTRIBUTE_NUMBER4","ATTRIBUTE_NUMBER5","ATTRIBUTE_NUMBER6","ATTRIBUTE_NUMBER7","ATTRIBUTE_NUMBER8",
	        		"ATTRIBUTE_NUMBER9","ATTRIBUTE_NUMBER10","ATTRIBUTE_DATE1","ATTRIBUTE_DATE2","ATTRIBUTE_DATE3","ATTRIBUTE_DATE4",
	        		"ATTRIBUTE_DATE5","ATTRIBUTE_TIMESTAMP1","ATTRIBUTE_TIMESTAMP2","ATTRIBUTE_TIMESTAMP3","ATTRIBUTE_TIMESTAMP4",
	        		"ATTRIBUTE_TIMESTAMP5"};
            
	        mappingStrategy.setColumnMapping(columnsItl); 
	        StatefulBeanToCsvBuilder<InventoryTransactionHeader> builderLot=  new StatefulBeanToCsvBuilder(bw); 
            beanWriter =  builderLot.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();
            beanWriter.write(itl);
            bw.flush();
            FileInputStream streamLot = new FileInputStream(tempLot);
            FileInputStream streamLot2 = new FileInputStream(tempLot);
            bw.close();
            tempLot.delete();

            File tempCost = File.createTempFile("tempCost", ".tmp"); 
			bw = new BufferedWriter(new FileWriter(tempCost));
			
			mappingStrategy= new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(InventoryTransactionCost.class); 
	        String[] columnsItc = new String[] {"TRANSACTION_COST_IDENTIFIER", "COST_COMPONENT_CODE", "COST"};
            
	        mappingStrategy.setColumnMapping(columnsItc); 
	        StatefulBeanToCsvBuilder<InventoryTransactionCost> builderCost =  new StatefulBeanToCsvBuilder(bw); 
            beanWriter =  builderCost.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();
            beanWriter.write(itc); 
            bw.flush();
            FileInputStream streamCost = new FileInputStream(tempCost);
            FileInputStream streamCost2 = new FileInputStream(tempCost);
            bw.close();
            tempCost.delete();   
                        
            //ARCHIVO BASE64
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(out);
            ZipEntry entry = new ZipEntry("InvTransactionsInterface.csv");
            zipOut.putNextEntry(entry);
            int number = 0;
            byte[] buffer = new byte[2048];
            while ((number = streamHdr.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamHdr.close();
            
            entry = new ZipEntry("InvTransactionLotsInterface.csv");
            zipOut.putNextEntry(entry);
            number = 0;
            buffer = new byte[2048];
            while ((number = streamLot.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamLot.close();
                        
            entry = new ZipEntry("CstTransCostInterface.csv");
            zipOut.putNextEntry(entry);
            number = 0;
            buffer = new byte[2048];
            while ((number = streamCost.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamCost.close();
            zipOut.close();
            
            byte[] bytes = out.toByteArray();
            String content = Base64.encodeBase64String(bytes);
            String fileNameBase64 = "Inv_" + stringFileName + ".zip";
            String fileNameZip = "Inv_" + stringFileName + ".zip";
            out.close();

            it.getFileTransferControl().setFileBase64(fileNameBase64);
            it.getFileTransferControl().setFileZip(fileNameZip);
            this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_SENT, null);

            if(SOAPFileImportService.uploadInvBulkFile(content, fileNameBase64, message)){
                response.setCode(200);
                response.setStatus("SUCCESS");
                response.setContent(AppConstants.GLSuccess_Msg + fileNameBase64 + " Message: " + message);
                this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_SUCCESS, response.getContent());
            }else{
    			response.setCode(0);
    			response.setStatus("ERROR");
    			response.setContent("Ocurrió un error al enviar el archivo de Inventarios: " + fileNameBase64 + " Message: " + message);
                this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, response.getContent());
            }
            
            try {
	            //ARCHIVO ZIP
	            String zipFileName = "c:\\temp\\" + fileNameZip;
	            FileOutputStream fos = new FileOutputStream(zipFileName);
	            ZipOutputStream zipFos = new ZipOutputStream(fos);
	            ZipEntry entryFos = new ZipEntry("InvTransactionsInterface.csv");
	            zipFos.putNextEntry(entryFos);
	            int numberFos = 0;
	            byte[] bufferFos = new byte[2048];
	            while ((numberFos = streamHdr2.read(bufferFos)) != -1) {
	                zipFos.write(bufferFos, 0, numberFos);
	            } 
	            streamHdr2.close();
	            
	            entryFos = new ZipEntry("InvTransactionLotsInterface.csv");
	            zipFos.putNextEntry(entryFos);
	            numberFos = 0;
	            bufferFos = new byte[2048];
	            while ((numberFos = streamLot2.read(bufferFos)) != -1) {
	            	zipFos.write(bufferFos, 0, numberFos);
	            } 
	            streamLot2.close();
	
	            entryFos = new ZipEntry("CstTransCostInterface.csv");
	            zipFos.putNextEntry(entryFos);
	            numberFos = 0;
	            bufferFos = new byte[2048];
	            while ((numberFos = streamCost2.read(bufferFos)) != -1) {
	            	zipFos.write(bufferFos, 0, numberFos);
	            } 
	            streamCost2.close();
	            zipFos.close();
            } catch(Exception ex) {
            	ex.printStackTrace();
            }
            
		}catch(Exception e) {
			e.printStackTrace();
			response.setCode(0);
			response.setStatus("ERROR");
			response.setContent("Ocurrió un error en el servicio web de envío de Inventario: " + e.getMessage());
			this.setFileTransferControl(it.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, "Ocurrió un error en el servicio web de envío de Inventario: " + e.getMessage());
		}
		return response;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public BaseResponse processGlFile(GLInterface glInterface ) {
		BaseResponse response = new BaseResponse();
		StringBuilder message = new StringBuilder("");

		List <GLInterfaceV3> data = glInterface.getListGLInterface();
		String data_acces_set =  glInterface.getDataAccesSet();
		String ledger_id = glInterface.getLedgerId();

		
		try {
			FileTransferControl ftc = this.getFileTransferControlByIdCC(glInterface.getFileTransferControl());					
			
			if(ftc != null) {				
				if(AppConstants.FILE_REQUEST_SUCCESS.equals(ftc.getStatus())) {					
	                response.setCode(200);
	                response.setStatus("SUCCESS");
	                response.setContent("El archivo ha sido enviado anteriormente con éxito. Log anterior: " + ftc.getLog());
	                return response;	               
				} else if(AppConstants.FILE_REQUEST_NEW.equals(ftc.getStatus()) || AppConstants.FILE_REQUEST_SENT.equals(ftc.getStatus())){					
					message.append("El archivo ha sido enviado anteriormente sin éxito, se obtuvo con estatus de " + ftc.getStatus() + ", se procesará en el siguiente periodo. Log anterior: " + ftc.getLog() + "\r\r");
					this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());					
	    			response.setCode(0);
	    			response.setStatus("ERROR");
	    			response.setContent(message.toString());
	    			return response;
				}
			}
			
			if(!AppConstants.FILE_REQUEST_ERROR.equals(glInterface.getFileTransferControl().getStatus())) {
				this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_NEW, null);
			}
			
			SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMddHHmmss");			
			String stringBranch = String.format("%02d", glInterface.getFileTransferControl().getBranchId());
			String stringDate = sfd.format(glInterface.getFileTransferControl().getFromDate());
			String stringCC = String.valueOf(glInterface.getFileTransferControl().getIdCC());
			String stringFileName = new StringBuilder(stringBranch).append("_").append("CC").append(stringCC).append("_").append(stringDate).toString();
			
	        File tempHdr = File.createTempFile("GlInterface_V3", ".tmp"); 
			BufferedWriter bw = new BufferedWriter(new FileWriter(tempHdr));	
			
			ColumnPositionMappingStrategy mappingStrategy= new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(GLInterfaceV3.class); 
	        String[] columns = new String[]  
	        		{"Status_Code","Ledger_ID","Effective_Date_of_Transaction","Journal_Source","Journal_Category",
	    	                "Currency_Code","Journal_Entry_Creation_Date","Actual_Flag","Segment1","Segment2","Segment3",
	    	                "Segment4","Segment5","Segment6","Segment7","Segment8","Segment9","Segment10","Segment11",
	    	                "Segment12","Segment13","Segment14","Segment15","Segment16","Segment17","Segment18","Segment19",
	    	                "Segment20","Segment21","Segment22","Segment23","Segment24","Segment25","Segment26","Segment27",
	    	                "Segment28","Segment29","Segment30","Entered_Debit_Amount","Entered_Credit_Amount",
	    	                "Converted_Debit_Amount","Converted_Credit_Amount","REFERENCE1","REFERENCE2","REFERENCE3",
	    	                "REFERENCE4","REFERENCE5","REFERENCE6","REFERENCE7","REFERENCE8","REFERENCE9","REFERENCE10",
	    	                "Reference_column_1","Reference_column_2","Reference_column_3","Reference_column_4",
	    	                "Reference_column_5","Reference_column_6","Reference_column_7","Reference_column_8",
	    	                "Reference_column_9","Reference_column_10","Statistical_Amount","Currency_Conversion_Type",
	    	                "Currency_Conversion_Date","Currency_Conversion_Rate","Interface_Group_Identifier",
	    	                "Context_field_for_Journal_Entry_Line_DFF","ATTRIBUTE1","ATTRIBUTE2","ATTRIBUTE3","ATTRIBUTE4",
	    	                "ATTRIBUTE5","ATTRIBUTE6","ATTRIBUTE7","ATTRIBUTE8","ATTRIBUTE9","ATTRIBUTE10","ATTRIBUTE11",
	    	                "ATTRIBUTE12","ATTRIBUTE13","ATTRIBUTE14","ATTRIBUTE15","ATTRIBUTE16","ATTRIBUTE17","ATTRIBUTE18",
	    	                "ATTRIBUTE19","ATTRIBUTE20","Context_field_for_Captured_Information_DFF","Average_Journal_Flag",
	    	                "Clearing_Company","Ledger_Name","Encumbrance_Type_ID","Reconciliation_Reference","end"};
	        mappingStrategy.setColumnMapping(columns); 
	        StatefulBeanToCsvBuilder<GLInterfaceV3> builder=  new StatefulBeanToCsvBuilder(bw); 
            StatefulBeanToCsv beanWriter =  builder.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();
            beanWriter.write(data); 
            bw.flush();
            FileInputStream streamHdr = new FileInputStream(tempHdr);
            FileInputStream streamHdr2 = new FileInputStream(tempHdr);
            bw.close();
            tempHdr.delete();
                      
            //ARCHIVO BASE64
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(out);
            
            ZipEntry entry = new ZipEntry("GlInterface.csv");
            zipOut.putNextEntry(entry);
            int number = 0;
            byte[] buffer = new byte[4096];
            while ((number = streamHdr.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamHdr.close();   
            zipOut.close();
            
            byte[] bytes = out.toByteArray();
            String content = Base64.encodeBase64String(bytes);
            String fileNameBase64 = "GL_" + stringFileName + ".zip";
            String fileNameZip = "GL_" + stringFileName + ".zip";
            out.close();

            glInterface.getFileTransferControl().setFileBase64(fileNameBase64);
            glInterface.getFileTransferControl().setFileZip(fileNameZip);
            
			if(!AppConstants.FILE_REQUEST_ERROR.equals(glInterface.getFileTransferControl().getStatus())) {
				
				this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_SENT, null);
	            if(SOAPFileImportService.uploadGLBulkFile(content, fileNameBase64, data_acces_set, ledger_id, message)){
	                response.setCode(200);
	                response.setStatus("SUCCESS");
	                response.setContent(AppConstants.GLSuccess_Msg + fileNameBase64 + " Message: " + message);
	                this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_SUCCESS, response.getContent());                
	            }else{
	    			response.setCode(0);
	    			response.setStatus("ERROR");
	    			response.setContent("Ocurrió un error al enviar el archivo de GL: " + fileNameBase64 + " Message: " + message);
	    			this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, response.getContent());
	            }
	            
			} else {				
				message.append(glInterface.getFileTransferControl().getLog());				
    			response.setCode(0);
    			response.setStatus("ERROR");
    			response.setContent(message.toString());
    			this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());    			
			}
            
          //ARCHIVO ZIP
          String zipFileName = "c:\\temp\\" + fileNameZip;
          FileOutputStream fos = new FileOutputStream(zipFileName);           
		  ZipOutputStream zipFos = new ZipOutputStream(fos);
		  
		  ZipEntry entryFos = new ZipEntry("GlInterface.csv");
		  zipFos.putNextEntry(entryFos);
		  int numberFos = 0;
		  byte[] bufferFos = new byte[4096];
		  while ((numberFos = streamHdr2.read(bufferFos)) != -1) {
		      zipFos.write(bufferFos, 0, numberFos);
		  } 
		  streamHdr2.close();   
		  zipFos.close();
		  
		}catch(Exception e) {
			e.printStackTrace();
			response.setCode(0);
			response.setStatus("ERROR");
			response.setContent("Ocurrió un error en el servicio web de envío de GL: " + e.getMessage());
			this.setFileTransferControl(glInterface.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, "Ocurrió un error en el servicio web de envío de GL: " + e.getMessage());
		}
		return response;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BaseResponse processARFile(RaInterface data) {
		StringBuilder message = new StringBuilder("");
		StringBuilder fileStatus = new StringBuilder("");
		StringBuilder fileProcessId = new StringBuilder("");
		BaseResponse response = new BaseResponse();
		
		try {
			FileTransferControl ftc = this.getFileTransferControlByIdCC(data.getFileTransferControl());
			
			if(ftc != null) {
				if(AppConstants.FILE_REQUEST_SUCCESS.equals(ftc.getStatus())) {					
	                response.setCode(200);
	                response.setStatus("SUCCESS");
	                response.setContent("El archivo ha sido enviado anteriormente con éxito. Log anterior: " + ftc.getLog());
	                return response;
	                
				} else if(AppConstants.FILE_REQUEST_NEW.equals(ftc.getStatus()) || AppConstants.FILE_REQUEST_SENT.equals(ftc.getStatus())){					
					message.append("El archivo ha sido enviado anteriormente sin éxito, se obtuvo con estatus de " + ftc.getStatus() + ", se procesará en el siguiente periodo. Log anterior: " + ftc.getLog() + "\r\r");
					this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());					
	    			response.setCode(0);
	    			response.setStatus("ERROR");
	    			response.setContent(message.toString());
	    			return response;
				}
			}
			
			if(!AppConstants.FILE_REQUEST_ERROR.equals(data.getFileTransferControl().getStatus())) {
				this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_NEW, null);
			}
						
			SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMddHHmmss");			
			String stringBranch = String.format("%02d", data.getFileTransferControl().getBranchId());
			String stringDate = sfd.format(data.getFileTransferControl().getFromDate());
			String stringCC = String.valueOf(data.getFileTransferControl().getIdCC());
			String stringFileName = new StringBuilder(stringBranch).append("_").append("CC").append(stringCC).append("_").append(stringDate).toString();			

			List<RaInterfaceLinesAll> raInterfaceLinesAll = data.getRaInterfaceLinesAll();
			List<RaInterfaceDistributionsAll> raInterfaceDistributionsAll = data.getRaInterfaceDistributionsAll();
			
			String ID = data.getBusinessUnitId();			
			File tempHdr = File.createTempFile("tempIntAll", ".tmp"); 
			BufferedWriter bw = new BufferedWriter(new FileWriter(tempHdr));
			
			ColumnPositionMappingStrategy mappingStrategy= new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(RaInterfaceLinesAll.class); 
	        String[] columns = new String[]  
	        		{"Business_Unit_Identifier","Transaction_Batch_Source_Name","Transaction_Type_Name","Payment_Terms","Transaction_Date","Accounting_Date",
	        				"Transaction_Number","Original_System_Bill_to_Customer_Reference","Original_System_Bill_to_Customer_Address_Reference",
	        				"Original_System_Bill_to_Customer_Contact_Reference","Original_System_Ship_to_Customer_Reference",
	        				"Original_System_Ship_to_Customer_Address_Reference","Original_System_Ship_to_Customer_Contact_Reference",
	        				"Original_System_Ship_to_Customer_Account_Reference","Original_System_Ship_to_Customer_Account_Address_Reference",
	        				"Original_System_Ship_to_Customer_Account_Contact_Reference","Original_System_Sold_to_Customer_Reference",
	        				"Original_System_Sold_to_Customer_Account_Reference","Bill_to_Customer_Account_Number","Bill_to_Customer_Site_Number",
	        				"Bill_to_Contact_Party_Number","Ship_to_Customer_Account_Number","Ship_to_Customer_Site_Number","Ship_to_Contact_Party_Number",
	        				"Sold_to_Customer_Account_Number","Transaction_Line_Type","Transaction_Line_Description","Currency_Code","Currency_Conversion_Type",
	        				"Currency_Conversion_Date","Currency_Conversion_Rate","Transaction_Line_Amount","Transaction_Line_Quantity","Customer_Ordered_Quantity",
	        				"Unit_Selling_Price","Unit_Standard_Price","Line_Transactions_Flexfield_Context","Line_Transactions_Flexfield_Segment_1",
	        				"Line_Transactions_Flexfield_Segment_2","Line_Transactions_Flexfield_Segment_3","Line_Transactions_Flexfield_Segment_4",
	        				"Line_Transactions_Flexfield_Segment_5","Line_Transactions_Flexfield_Segment_6","Line_Transactions_Flexfield_Segment_7",
	        				"Line_Transactions_Flexfield_Segment_8","Line_Transactions_Flexfield_Segment_9","Line_Transactions_Flexfield_Segment_10",
	        				"Line_Transactions_Flexfield_Segment_11","Line_Transactions_Flexfield_Segment_12","Line_Transactions_Flexfield_Segment_13",
	        				"Line_Transactions_Flexfield_Segment_14","Line_Transactions_Flexfield_Segment_15","Primary_Salesperson_Number","Tax_Classification_Code",
	        				"Legal_Entity_Identifier","Accounted_Amount_in_Ledger_Currency","Sales_Order_Number","Sales_Order_Date","Actual_Ship_Date",
	        				"Warehouse_Code","Unit_of_Measure_Code","Unit_of_Measure_Name","Invoicing_Rule_Name","Revenue_Scheduling_Rule_Name","Number_of_Revenue_Periods",
	        				"Revenue_Scheduling_Rule_Start_Date","Revenue_Scheduling_Rule_End_Date","Reason_Code_Meaning","Last_Period_to_Credit",
	        				"Transaction_Business_Category_Code","Product_Fiscal_Classification_Code","Product_Category_Code","Product_Type","Line_Intended_Use_Code",
	        				"Assessable_Value","Document_Sub_Type","Default_Taxation_Country","User_Defined_Fiscal_Classification","Tax_Invoice_Number",
	        				"Tax_Invoice_Date","Tax_Regime_Code","Tax","Tax_Status_Code","Tax_Rate_Code","Tax_Jurisdiction_Code","First_Party_Registration_Number",
	        				"Third_Party_Registration_Number","Final_Discharge_Location","Taxable_Amount","Taxable_Flag","Tax_Exemption_Flag","Tax_Exemption_Reason_Code",
	        				"Tax_Exemption_Reason_Code_Meaning","Tax_Exemption_Certificate_Number","Line_Amount_Includes_Tax_Flag","Tax_Precedence",
	        				"Credit_Method_To_Be_Used_For_Lines_With_Revenue_Scheduling_Rules","Credit_Method_To_Be_Used_For_Transactions_With_Split_Payment_Terms",
	        				"Reason_Code","Tax_Rate","FOB_Point","Carrier","Shipping_Reference","Sales_Order_Line_Number","Sales_Order_Source",
	        				"Sales_Order_Revision_Number","Purchase_Order_Number","Purchase_Order_Revision_Number","Purchase_Order_Date","Agreement_Name","Memo_Line_Name",
	        				"Document_Number","Original_System_Batch_Name","Link_to_Transactions_Flexfield_Context","Link_to_Transactions_Flexfield_Segment_1",
	        				"Link_to_Transactions_Flexfield_Segment_2","Link_to_Transactions_Flexfield_Segment_3","Link_to_Transactions_Flexfield_Segment_4",
	        				"Link_to_Transactions_Flexfield_Segment_5","Link_to_Transactions_Flexfield_Segment_6","Link_to_Transactions_Flexfield_Segment_7",
	        				"Link_to_Transactions_Flexfield_Segment_8","Link_to_Transactions_Flexfield_Segment_9","Link_to_Transactions_Flexfield_Segment_10",
	        				"Link_to_Transactions_Flexfield_Segment_11","Link_to_Transactions_Flexfield_Segment_12","Link_to_Transactions_Flexfield_Segment_13",
	        				"Link_to_Transactions_Flexfield_Segment_14","Link_to_Transactions_Flexfield_Segment_15","Reference_Transactions_Flexfield_Context",
	        				"Reference_Transactions_Flexfield_Segment_1","Reference_Transactions_Flexfield_Segment_2","Reference_Transactions_Flexfield_Segment_3",
	        				"Reference_Transactions_Flexfield_Segment_4","Reference_Transactions_Flexfield_Segment_5","Reference_Transactions_Flexfield_Segment_6",
	        				"Reference_Transactions_Flexfield_Segment_7","Reference_Transactions_Flexfield_Segment_8","Reference_Transactions_Flexfield_Segment_9",
	        				"Reference_Transactions_Flexfield_Segment_10","Reference_Transactions_Flexfield_Segment_11","Reference_Transactions_Flexfield_Segment_12",
	        				"Reference_Transactions_Flexfield_Segment_13","Reference_Transactions_Flexfield_Segment_14","Reference_Transactions_Flexfield_Segment_15",
	        				"Link_To_Parent_Line_Context","Link_To_Parent_Line_Segment_1","Link_To_Parent_Line_Segment_2","Link_To_Parent_Line_Segment_3",
	        				"Link_To_Parent_Line_Segment_4","Link_To_Parent_Line_Segment_5","Link_To_Parent_Line_Segment_6","Link_To_Parent_Line_Segment_7",
	        				"Link_To_Parent_Line_Segment_8","Link_To_Parent_Line_Segment_9","Link_To_Parent_Line_Segment_10","Link_To_Parent_Line_Segment_11",
	        				"Link_To_Parent_Line_Segment_12","Link_To_Parent_Line_Segment_13","Link_To_Parent_Line_Segment_14","Link_To_Parent_Line_Segment_15",
	        				"Receipt_Method_Name","Printing_Option","Related_Batch_Source_Name","Related_Transaction_Number","Inventory_Item_Number",
	        				"Inventory_Item_Segment_2","Inventory_Item_Segment_3","Inventory_Item_Segment_4","Inventory_Item_Segment_5","Inventory_Item_Segment_6",
	        				"Inventory_Item_Segment_7","Inventory_Item_Segment_8","Inventory_Item_Segment_9","Inventory_Item_Segment_10","Inventory_Item_Segment_11",
	        				"Inventory_Item_Segment_12","Inventory_Item_Segment_13","Inventory_Item_Segment_14","Inventory_Item_Segment_15","Inventory_Item_Segment_16",
	        				"Inventory_Item_Segment_17","Inventory_Item_Segment_18","Inventory_Item_Segment_19","Inventory_Item_Segment_20",
	        				"Bill_To_Customer_Bank_Account_Name","Reset_Transaction_Date_Flag","Payment_Server_Order_Number","Last_Transaction_on_Debit_Authorization",
	        				"Approval_Code","Address_Verification_Code","Transaction_Line_Translated_Description","Consolidated_Billing_Number","Promised_Commitment_Amount",
	        				"Payment_Set_Identifier","Original_Accounting_Date","Invoiced_Line_Accounting_Level","Override_AutoAccounting_Flag","Historical_Flag",
	        				"Deferral_Exclusion_Flag","Payment_Attributes","Invoice_Billing_Date","Invoice_Lines_Flexfield_Context","Invoice_Lines_Flexfield_Segment_1",
	        				"Invoice_Lines_Flexfield_Segment_2","Invoice_Lines_Flexfield_Segment_3","Invoice_Lines_Flexfield_Segment_4","Invoice_Lines_Flexfield_Segment_5",
	        				"Invoice_Lines_Flexfield_Segment_6","Invoice_Lines_Flexfield_Segment_7","Invoice_Lines_Flexfield_Segment_8","Invoice_Lines_Flexfield_Segment_9",
	        				"Invoice_Lines_Flexfield_Segment_10","Invoice_Lines_Flexfield_Segment_11","Invoice_Lines_Flexfield_Segment_12",
	        				"Invoice_Lines_Flexfield_Segment_13","Invoice_Lines_Flexfield_Segment_14","Invoice_Lines_Flexfield_Segment_15",
	        				"Invoice_Transactions_Flexfield_Context","Invoice_Transactions_Flexfield_Segment_1","Invoice_Transactions_Flexfield_Segment_2",
	        				"Invoice_Transactions_Flexfield_Segment_3","Invoice_Transactions_Flexfield_Segment_4","Invoice_Transactions_Flexfield_Segment_5",
	        				"Invoice_Transactions_Flexfield_Segment_6","Invoice_Transactions_Flexfield_Segment_7","Invoice_Transactions_Flexfield_Segment_8",
	        				"Invoice_Transactions_Flexfield_Segment_9","Invoice_Transactions_Flexfield_Segment_10","Invoice_Transactions_Flexfield_Segment_11",
	        				"Invoice_Transactions_Flexfield_Segment_12","Invoice_Transactions_Flexfield_Segment_13","Invoice_Transactions_Flexfield_Segment_14",
	        				"Invoice_Transactions_Flexfield_Segment_15","Receivables_Transaction_Region_Information_Flexfield_Context",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_1","Receivables_Transaction_Region_Information_Flexfield_Segment_2",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_3","Receivables_Transaction_Region_Information_Flexfield_Segment_4",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_5","Receivables_Transaction_Region_Information_Flexfield_Segment_6",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_7","Receivables_Transaction_Region_Information_Flexfield_Segment_8",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_9","Receivables_Transaction_Region_Information_Flexfield_Segment_10",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_11","Receivables_Transaction_Region_Information_Flexfield_Segment_12",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_13","Receivables_Transaction_Region_Information_Flexfield_Segment_14",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_15","Receivables_Transaction_Region_Information_Flexfield_Segment_16",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_17","Receivables_Transaction_Region_Information_Flexfield_Segment_18",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_19","Receivables_Transaction_Region_Information_Flexfield_Segment_20",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_21","Receivables_Transaction_Region_Information_Flexfield_Segment_22",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_23","Receivables_Transaction_Region_Information_Flexfield_Segment_24",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_25","Receivables_Transaction_Region_Information_Flexfield_Segment_26",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_27","Receivables_Transaction_Region_Information_Flexfield_Segment_28",
	        				"Receivables_Transaction_Region_Information_Flexfield_Segment_29","Receivables_Transaction_Region_Information_Flexfield_Segment_30",
	        				"Line_Global_Descriptive_Flexfield_Attribute_Category","Line_Global_Descriptive_Flexfield_Segment_1",
	        				"Line_Global_Descriptive_Flexfield_Segment_2","Line_Global_Descriptive_Flexfield_Segment_3","Line_Global_Descriptive_Flexfield_Segment_4",
	        				"Line_Global_Descriptive_Flexfield_Segment_5","Line_Global_Descriptive_Flexfield_Segment_6","Line_Global_Descriptive_Flexfield_Segment_7",
	        				"Line_Global_Descriptive_Flexfield_Segment_8","Line_Global_Descriptive_Flexfield_Segment_9","Line_Global_Descriptive_Flexfield_Segment_10",
	        				"Line_Global_Descriptive_Flexfield_Segment_11","Line_Global_Descriptive_Flexfield_Segment_12","Line_Global_Descriptive_Flexfield_Segment_13",
	        				"Line_Global_Descriptive_Flexfield_Segment_14","Line_Global_Descriptive_Flexfield_Segment_15","Line_Global_Descriptive_Flexfield_Segment_16",
	        				"Line_Global_Descriptive_Flexfield_Segment_17","Line_Global_Descriptive_Flexfield_Segment_18","Line_Global_Descriptive_Flexfield_Segment_19",
	        				"Line_Global_Descriptive_Flexfield_Segment_20","Business_Unit_Name","Comments","Notes_from_Source","Credit_Card_Token_Number",
	        				"Credit_Card_Expiration_Date","First_Name_of_the_Credit_Card_Holder","Last_Name_of_the_Credit_Card_Holder","Credit_Card_Issuer_Code",
	        				"Masked_Credit_Card_Number","Credit_Card_Authorization_Request_Identifier","Credit_Card_Voice_Authorization_Code",
	        				"Receivables_Transaction_Region_Information_Flexfield_Number_Segment_1","Receivables_Transaction_Region_Information_Flexfield_Number_Segment_2",
	        				"Receivables_Transaction_Region_Information_Flexfield_Number_Segment_3","Receivables_Transaction_Region_Information_Flexfield_Number_Segment_4",
	        				"Receivables_Transaction_Region_Information_Flexfield_Number_Segment_5","Receivables_Transaction_Region_Information_Flexfield_Number_Segment_6",
	        				"Receivables_Transaction_Region_Information_Flexfield_Number_Segment_7","Receivables_Transaction_Region_Information_Flexfield_Number_Segment_8",
	        				"Receivables_Transaction_Region_Information_Flexfield_Number_Segment_9","Receivables_Transaction_Region_Information_Flexfield_Number_Segment_10",
	        				"Receivables_Transaction_Region_Information_Flexfield_Number_Segment_11","Receivables_Transaction_Region_Information_Flexfield_Number_Segment_12",
	        				"Receivables_Transaction_Region_Information_Flexfield_Date_Segment_1","Receivables_Transaction_Region_Information_Flexfield_Date_Segment_2",
	        				"Receivables_Transaction_Region_Information_Flexfield_Date_Segment_3","Receivables_Transaction_Region_Information_Flexfield_Date_Segment_4",
	        				"Receivables_Transaction_Region_Information_Flexfield_Date_Segment_5","Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_1",
	        				"Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_2","Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_3",
	        				"Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_4","Receivables_Transaction_Line_Region_Information_Flexfield_Number_Segment_5",
	        				"Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_1","Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_2",
	        				"Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_3","Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_4",
	        				"Receivables_Transaction_Line_Region_Information_Flexfield_Date_Segment_5","Freight_Charge","Insurance_Charge","Packing_Charge","Miscellaneous_Charge",
	        				"Commercial_Discount","Enforce_Chronological_Document_Sequencing","End"
	        				};


            mappingStrategy.setColumnMapping(columns); 
	        StatefulBeanToCsvBuilder<RaInterfaceLinesAll> builder=  new StatefulBeanToCsvBuilder(bw); 
            StatefulBeanToCsv beanWriter =  builder.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();
            beanWriter.write(raInterfaceLinesAll); 
            bw.flush();
            FileInputStream streamHdr = new FileInputStream(tempHdr);
            FileInputStream streamHdr2 = new FileInputStream(tempHdr);
            bw.close();
            tempHdr.delete();
            
            File tempDist = File.createTempFile("tempIntDist", ".tmp"); 
			bw = new BufferedWriter(new FileWriter(tempDist));
			
            mappingStrategy= new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(RaInterfaceDistributionsAll.class); 
	        columns = new String[]  
	        		{"Business_Unit_Identifier","Account_Class","Amount","Percent","Accounted_Amount_in_Ledger_Currency",
	        	     "Line_Transactions_Flexfield_Context","Line_Transactions_Flexfield_Segment_1",
	        	     "Line_Transactions_Flexfield_Segment_2","Line_Transactions_Flexfield_Segment_3",
	        	     "Line_Transactions_Flexfield_Segment_4","Line_Transactions_Flexfield_Segment_5",
	        	     "Line_Transactions_Flexfield_Segment_6","Line_Transactions_Flexfield_Segment_7",
	        	     "Line_Transactions_Flexfield_Segment_8","Line_Transactions_Flexfield_Segment_9",
	        	     "Line_Transactions_Flexfield_Segment_10","Line_Transactions_Flexfield_Segment_11",
	        	     "Line_Transactions_Flexfield_Segment_12","Line_Transactions_Flexfield_Segment_13",
	        	     "Line_Transactions_Flexfield_Segment_14","Line_Transactions_Flexfield_Segment_15",
	        	     "Accounting_Flexfield_Segment_1","Accounting_Flexfield_Segment_2","Accounting_Flexfield_Segment_3",
	        	     "Accounting_Flexfield_Segment_4","Accounting_Flexfield_Segment_5","Accounting_Flexfield_Segment_6",
	        	     "Accounting_Flexfield_Segment_7","Accounting_Flexfield_Segment_8","Accounting_Flexfield_Segment_9",
	        	     "Accounting_Flexfield_Segment_10","Accounting_Flexfield_Segment_11","Accounting_Flexfield_Segment_12",
	        	     "Accounting_Flexfield_Segment_13","Accounting_Flexfield_Segment_14","Accounting_Flexfield_Segment_15",
	        	     "Accounting_Flexfield_Segment_16","Accounting_Flexfield_Segment_17","Accounting_Flexfield_Segment_18",
	        	     "Accounting_Flexfield_Segment_19","Accounting_Flexfield_Segment_20","Accounting_Flexfield_Segment_21",
	        	     "Accounting_Flexfield_Segment_22","Accounting_Flexfield_Segment_23","Accounting_Flexfield_Segment_24",
	        	     "Accounting_Flexfield_Segment_25","Accounting_Flexfield_Segment_26","Accounting_Flexfield_Segment_27",
	        	     "Accounting_Flexfield_Segment_28","Accounting_Flexfield_Segment_29","Accounting_Flexfield_Segment_30",
	        	     "Comments","Interim_Tax_Segment_1","Interim_Tax_Segment_2","Interim_Tax_Segment_3",
	        	     "Interim_Tax_Segment_4","Interim_Tax_Segment_5","Interim_Tax_Segment_6","Interim_Tax_Segment_7",
	        	     "Interim_Tax_Segment_8","Interim_Tax_Segment_9","Interim_Tax_Segment_10","Interim_Tax_Segment_11",
	        	     "Interim_Tax_Segment_12","Interim_Tax_Segment_13","Interim_Tax_Segment_14","Interim_Tax_Segment_15",
	        	     "Interim_Tax_Segment_16","Interim_Tax_Segment_17","Interim_Tax_Segment_18","Interim_Tax_Segment_19",
	        	     "Interim_Tax_Segment_20","Interim_Tax_Segment_21","Interim_Tax_Segment_22","Interim_Tax_Segment_23",
	        	     "Interim_Tax_Segment_24","Interim_Tax_Segment_25","Interim_Tax_Segment_26","Interim_Tax_Segment_27",
	        	     "Interim_Tax_Segment_28","Interim_Tax_Segment_29","Interim_Tax_Segment_30",
	        	     "Interface_Distributions_Flexfield_Context","Interface_Distributions_Flexfield__Segment_1",
	        	     "Interface_Distributions_Flexfield__Segment_2","Interface_Distributions_Flexfield__Segment_3",
	        	     "Interface_Distributions_Flexfield_Segment_4","Interface_Distributions_Flexfield_Segment_5",
	        	     "Interface_Distributions_Flexfield_Segment_6","Interface_Distributions_Flexfield_Segment_7",
	        	     "Interface_Distributions_Flexfield_Segment_8","Interface_Distributions_Flexfield_Segment_9",
	        	     "Interface_Distributions_Flexfield_Segment_10","Interface_Distributions_Flexfield_Segment_11",
	        	     "Interface_Distributions_Flexfield_Segment_12","Interface_Distributions_Flexfield_Segment_13",
	        	     "Interface_Distributions_Flexfield_Segment_14","Interface_Distributions_Flexfield_Segment_15",
	        	     "Business_Unit_Name","End"};

            mappingStrategy.setColumnMapping(columns); 
	        StatefulBeanToCsvBuilder<RaInterfaceDistributionsAll> builderDist =  new StatefulBeanToCsvBuilder(bw); 
            beanWriter =  builderDist.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();
            beanWriter.write(raInterfaceDistributionsAll); 
            bw.flush();
            FileInputStream streamDist = new FileInputStream(tempDist);
            FileInputStream streamDist2 = new FileInputStream(tempDist);
            bw.close();
            tempDist.delete();
            
            //ARCHIVO BASE64
            ByteArrayOutputStream out = new ByteArrayOutputStream();            
            ZipOutputStream zipOut = new ZipOutputStream(out);            
            ZipEntry entry = new ZipEntry("RaInterfaceLinesAll.csv");
            zipOut.putNextEntry(entry);
            int number = 0;
            byte[] buffer = new byte[4096];
            while ((number = streamHdr.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamHdr.close();
            
            entry = new ZipEntry("RaInterfaceDistributionsAll.csv");
            zipOut.putNextEntry(entry);
            number = 0;
            buffer = new byte[4096];
            while ((number = streamDist.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamDist.close();            
            zipOut.close();
            
            byte[] bytes = out.toByteArray();
            String content = Base64.encodeBase64String(bytes);
            String fileNameBase64 = "AR_" + stringFileName + ".zip";
            String fileNameZip = "AR_" + stringFileName + ".zip";
            out.close();

            data.getFileTransferControl().setFileBase64(fileNameBase64);
            data.getFileTransferControl().setFileZip(fileNameZip);
            
			if(!AppConstants.FILE_REQUEST_ERROR.equals(data.getFileTransferControl().getStatus())) {
				
	            this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_SENT, null);
	            if(SOAPFileImportService.uploadARBulkFile(content, fileNameBase64, ID, message, fileStatus, fileProcessId)){
	                response.setCode(200);
	                response.setStatus("SUCCESS");
	                response.setContent(AppConstants.GLSuccess_Msg + fileNameBase64 + " Message: " + message);
	                data.getFileTransferControl().setFileProcessId(String.valueOf(fileProcessId));
	                data.getFileTransferControl().setFileStatus(String.valueOf(fileStatus));
	                this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_SUCCESS, response.getContent());
	            }else{
	    			response.setCode(0);
	    			response.setStatus("ERROR");
	    			response.setContent("Ocurrió un error al enviar el archivo de AR: " + fileNameBase64 + " Message: " + message);
	                data.getFileTransferControl().setFileProcessId(String.valueOf(fileProcessId));
	                data.getFileTransferControl().setFileStatus(String.valueOf(fileStatus));
	                this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, response.getContent());
	            }
	            
			} else {				
				message.append(data.getFileTransferControl().getLog());				
    			response.setCode(0);
    			response.setStatus("ERROR");
    			response.setContent(message.toString());
    			this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, message.toString());    			
			}
            
            //ARCHIVO ZIP            
            String zipFileName = "c:\\temp\\" + fileNameZip;            
            File file = new File(zipFileName);
            
            if(AppConstants.FILE_REQUEST_SUCCESS.equals(data.getFileTransferControl().getStatus()) || !file.exists()) {
                FileOutputStream fos = new FileOutputStream(zipFileName);
                ZipOutputStream zipFos = new ZipOutputStream(fos);            
                ZipEntry entryFos = new ZipEntry("RaInterfaceLinesAll.csv");
                zipFos.putNextEntry(entryFos);
                int numberFos = 0;
                byte[] bufferFos = new byte[4096];
                while ((numberFos = streamHdr2.read(bufferFos)) != -1) {
                	zipFos.write(bufferFos, 0, numberFos);
                } 
                streamHdr2.close();
                
                entryFos = new ZipEntry("RaInterfaceDistributionsAll.csv");
                zipFos.putNextEntry(entryFos);
                numberFos = 0;
                bufferFos = new byte[4096];
                while ((numberFos = streamDist2.read(bufferFos)) != -1) {
                	zipFos.write(bufferFos, 0, numberFos);
                } 
                streamDist2.close();            
                zipFos.close();	
            }
            
		}catch(Exception e) {
			e.printStackTrace();
			response.setCode(0);
			response.setStatus("ERROR");
			response.setContent("Ocurrió un error en el servicio web de envío de AR: " + e.getMessage());
			this.setFileTransferControl(data.getFileTransferControl(), AppConstants.FILE_REQUEST_ERROR, "Ocurrió un error en el servicio web de envío de AR: " + e.getMessage());
		}
		return response;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BaseResponse processARLockBoxImportCFile(List<ArLockBoxImportC> data) {
		BaseResponse response = new BaseResponse();
		
		Calendar now = Calendar.getInstance();
		String year = String.valueOf(now.get(Calendar.YEAR));
		String month = String.valueOf(now.get(Calendar.MONTH) + 1);
		String day = String.valueOf(now.get(Calendar.DAY_OF_MONTH));
		String hour = String.valueOf(now.get(Calendar.HOUR_OF_DAY));
		String minute = String.valueOf(now.get(Calendar.MINUTE));
		String uniqueKey = year + month + day + hour + minute;
		
		try {
			
	        File tempHdr = File.createTempFile("ArLockBoxImportC", ".tmp"); 
			BufferedWriter bw = new BufferedWriter(new FileWriter(tempHdr));	
			
			ColumnPositionMappingStrategy mappingStrategy= new ColumnPositionMappingStrategy(); 
	        mappingStrategy.setType(ArLockBoxImportC.class); 
	        String[] columns = new String[]  
	        		{"Record_Type", "Batch_Name", "Item_Number", "Remittance_Amount", "Transit_Routing_Number", "Customer_Bank_Account",
	        		"Receipt_Number", "Receipt_Date", "Currency", "Conversion_Rate_Type", "Conversion_Rate", "Customer_Account_Number",
	        		"Customer_Site", "Customer_Bank_Branch", "Customer_Bank", "Receipt_Method", "Remittance_Bank_Branch",
	        		"Remittance_Bank", "Lockbox_Number", "Deposit_Date", "Deposit_Time", "Anticipated_Clearing_Date", 
	        		"Transaction_Reference_1","Transaction_Installment_1", "Transaction_Reference_Date_1", "Transaction_Currency_1",
	        		"Cross_Currency_Rate_1", "Applied_Amount_1", "Allocated_Receipt_Amount_1", "Customer_Reference_1",
	        		"Transaction_Reference_2", "Transaction_Installment_2", "Transaction_Reference_Date_2", "Transaction_Currency_2",
	        		"Cross_Currency_Rate_2", "Applied_Amount_2", "Allocated_Receipt_Amount_2", "Customer_Reference_2",
	        		"Transaction_Reference_3", "Transaction_Installment_3", "Transaction_Reference_Date_3", "Transaction_Currency_3",
	        		"Cross_Currency_Rate_3", "Applied_Amount_3", "Allocated_Receipt_Amount_3", "Customer_Reference_3",
	        		"Transaction_Reference_4", "Transaction_Installment_4", "Transaction_Reference_Date_4", "Transaction_Currency_4",
	        		"Cross_Currency_Rate_4", "Applied_Amount_4", "Allocated_Receipt_Amount_4", "Customer_Reference_4",
	        		"Transaction_Reference_5", "Transaction_Installment_5", "Transaction_Reference_Date_5", "Transaction_Currency_5",
	        		"Cross_Currency_Rate_5", "Applied_Amount_5", "Allocated_Receipt_Amount_5", "Customer_Reference_5",
	        		"Transaction_Reference_6", "Transaction_Installment_6", "Transaction_Reference_Date_6", "Transaction_Currency_6",
	        		"Cross_Currency_Rate_6", "Applied_Amount_6", "Allocated_Receipt_Amount_6", "Customer_Reference_6",
	        		"Transaction_Reference_7", "Transaction_Installment_7", "Transaction_Reference_Date_7", "Transaction_Currency_7",
	        		"Cross_Currency_Rate_7", "Applied_Amount_7", "Allocated_Receipt_Amount_7", "Customer_Reference_7",
	        		"Transaction_Reference_8", "Transaction_Installment_8", "Transaction_Reference_Date_8", "Transaction_Currency_8",
	        		"Cross_Currency_Rate_8", "Applied_Amount_8", "Allocated_Receipt_Amount_8", "Customer_Reference_8", "Comments",
	        		"Payment_Interface_Flexfield_Segment_1", "Payment_Interface_Flexfield_Segment_2", "Payment_Interface_Flexfield_Segment_3",
	        		"Payment_Interface_Flexfield_Segment_4", "Payment_Interface_Flexfield_Segment_5", "Payment_Interface_Flexfield_Segment_6",
	        		"Payment_Interface_Flexfield_Segment_7", "Payment_Interface_Flexfield_Segment_8", "Payment_Interface_Flexfield_Segment_9",
	        		"Payment_Interface_Flexfield_Segment_10", "Payment_Interface_Flexfield_Segment_11", "Payment_Interface_Flexfield_Segment_12",
	        		"Payment_Interface_Flexfield_Segment_13", "Payment_Interface_Flexfield_Segment_14", "Payment_Interface_Flexfield_Segment_15"};
	        	        
	        mappingStrategy.setColumnMapping(columns); 
	        StatefulBeanToCsvBuilder<ArLockBoxImportC> builder=  new StatefulBeanToCsvBuilder(bw); 
            StatefulBeanToCsv beanWriter =  builder.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).withEscapechar(CSVWriter.NO_ESCAPE_CHARACTER).withMappingStrategy(mappingStrategy).withSeparator(',').build();
            beanWriter.write(data); 
            bw.flush();
            FileInputStream streamHdr = new FileInputStream(tempHdr);
            bw.close();
            tempHdr.delete();
            
            //Generate ZIP
            String zipFileName = "c:\\temp\\ARPayment_" + uniqueKey + ".zip";
            FileOutputStream out = new FileOutputStream(zipFileName); 
            ZipOutputStream zipOut = new ZipOutputStream(out);
            
            ZipEntry entry = new ZipEntry("ArLockBoxImportC.csv");
            zipOut.putNextEntry(entry);
            int number = 0;
            byte[] buffer = new byte[4096];
            while ((number = streamHdr.read(buffer)) != -1) {
                zipOut.write(buffer, 0, number);
            } 
            streamHdr.close();   
            zipOut.close();
            out.close();
            
            response.setCode(200);
            response.setStatus("SUCCESS");
            response.setContent(AppConstants.GLSuccess_Msg + zipFileName);
		}catch(Exception e) {
			e.printStackTrace();
			response.setCode(0);
			response.setStatus("ERROR");
			response.setContent(e.getMessage());
		}
		return response;
	}
	
	public BaseResponse registerCustomerAdditionalSpecs(CustomerAdditionalSpecs specs) {
		
		BaseResponse response = new BaseResponse();
		try {
			CustomerAdditionalSpecs result = integrationTDODataDao.saveCustomerAdditionalSpecs(specs);
			if(result != null) {
				response.setStatus("OK");
				response.setContent(String.valueOf(result.getId()));
			}
		}catch(Exception e) {
			response.setStatus("ERROR");
			response.setContent(e.getMessage());
		}
		return response;
	}
	
	public FileTransferControl setFileTransferControl(FileTransferControl ftc, String status, String log) {
		
		try 
		{	
			ftc.setLastUpdate(new Date());
			ftc.setStatus(status);
			ftc.setLog(log);
			
			integrationTDODataDao.saveOrUpdateFileTransferControl(ftc);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ftc;
	}
	
	public FileTransferControl getFileTransferControlByDateRange(FileTransferControl ftc) {
		List<FileTransferControl> lstFTC = null;
		FileTransferControl o = null;
		
		try 
		{
			lstFTC = integrationTDODataDao.getFileTransferControlByFromDate(ftc);
			if(lstFTC != null && lstFTC.size() > 0) {
				o = lstFTC.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}
	
	public FileTransferControl getFileTransferControlByIdCC(FileTransferControl ftc) {
		List<FileTransferControl> lstFTC = null;
		FileTransferControl o = null;
		
		try 
		{
			lstFTC = integrationTDODataDao.getFileTransferControlByIdCC(ftc);
			if(lstFTC != null && lstFTC.size() > 0) {
				o = lstFTC.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return o;
	}	
	
	public String getRandomStr() {
		byte[] array = new byte[7]; // length is bounded by 7
	    new Random().nextBytes(array);
	    String generatedString = new String(array, Charset.forName("UTF-8"));
	    return generatedString;
	}

	public BaseResponse processARFind(String request) {
		BaseResponse baseResponse = new BaseResponse();
		String[] str = request.split("/");
		String transactionNumber = "";		
		String lineNumber = "";
		String itemCode = "";
		
		try {
			if (str.length == 2) {
				transactionNumber = str[0];
				itemCode = str[1];
			} else {
				return null;
			}
			
			String response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
					PayloadProducer.getSessionIdSOAPXmlContent(), "");
			
			JSONObject xmlJSONObj = XML.toJSONObject(response, true);
			JsonElement jelement = new JsonParser().parse(xmlJSONObj.toString());
			JsonObject jobject = jelement.getAsJsonObject();
			JsonElement result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
					.get("sawsoap:logonResult").getAsJsonObject();
			String sessionID = result.getAsJsonObject().get("sawsoap:sessionID").toString();
			JSONObject json = new JSONObject(sessionID);
			String contentId = json.getString("content");
			
			response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_INVTRANSACTIONS_URL,
					PayloadProducer.getFindTransactionNumberSOAPXmlContent(contentId, transactionNumber, itemCode), "");
			
			xmlJSONObj = XML.toJSONObject(response, true);
			jelement = new JsonParser().parse(xmlJSONObj.toString());
			jobject = jelement.getAsJsonObject();

			if(jobject.toString().contains(AppConstants.SOAP_AUTHENTICATION_ERROR)) {
				return null;
			}
			
			result = jobject.get("soap:Envelope").getAsJsonObject().get("soap:Body").getAsJsonObject()
					.get("sawsoap:executeSQLQueryResult").getAsJsonObject().get("sawsoap:return").getAsJsonObject();
			String rowSet = result.getAsJsonObject().get("sawsoap:rowset").toString();
			rowSet = rowSet.replace("\\\"", "\"");
			rowSet = rowSet.replace("\"<", "<");
			rowSet = rowSet.replace(">\"", ">");
			rowSet = rowSet.replace("xmlns=\"urn:schemas-microsoft-com:xml-analysis:rowset\"", "");
			rowSet = rowSet.replace("Column5", "Column6");
			rowSet = rowSet.replace(" ", "");
	
			StringReader sr = new StringReader(rowSet);
			JAXBContext jaxbContext = JAXBContext.newInstance(Rowset.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			Rowset rsList = (Rowset) unmarshaller.unmarshal(sr);

			if (rsList != null && rsList.getRow() != null && !rsList.getRow().isEmpty()) {
				lineNumber =  rsList.getRow().get(0).getColumn4();
			}
			
		    if(lineNumber != null && !lineNumber.isEmpty()){
		        baseResponse.setCode(200);
		        baseResponse.setStatus("SUCCESS");
		        baseResponse.setContent(lineNumber);
		    }else{
				baseResponse.setCode(0);
				baseResponse.setStatus("ERROR");
				baseResponse.setContent("WS: No fue posible recuperar el dato Line Number para " + request);
		    }
		 
		    try {
				response = HTTPRequestService.httpXmlRequest(AppConstants.SOAP_SESSIONID_URL,
						PayloadProducer.logOffSessionIdSOAPXmlContent(contentId), "");
				
			} catch (Exception e) { }
			
		} catch (Exception e) {
			e.printStackTrace();
			baseResponse.setCode(0);
			baseResponse.setStatus("ERROR");
			baseResponse.setContent("WS: Ocurrió un error en el web service de ARFind: " + e.getMessage());			
		}

		return baseResponse;
	}

}

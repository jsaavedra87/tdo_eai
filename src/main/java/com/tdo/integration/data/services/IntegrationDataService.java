package com.tdo.integration.data.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tdo.integration.dao.IntegrationDataDao;

@Service("integrationDataService")
public class IntegrationDataService {
	
	@Autowired
	IntegrationDataDao integrationDataDao;

}

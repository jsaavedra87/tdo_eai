package com.tdo.integration.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.stereotype.Service;

@Service("HTTPRequestService")
public class HTTPRequestService {
	
	@SuppressWarnings({ "restriction", "static-access" })
	public String httpXmlRequest(String destUrl, String body, String authStr) {
		
		try {			
			URL url = new URL(destUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if (conn == null) {
				return null;
			}
			conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setFollowRedirects(true);
			conn.setAllowUserInteraction(false);

			//J.Avila - 20Mar2019
			if(!"".equals(authStr)) {
				byte[] authBytes = authStr.getBytes("UTF-8");
				String auth = com.sun.org.apache.xml.internal.security.utils.Base64.encode(authBytes);
				conn.setRequestProperty("Authorization", "Basic " + auth);
			}

			String response = null;
			try {
				OutputStream out = conn.getOutputStream();
				OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
				if(body != null) {
					writer.write(body);
				}else {
					return null;
				}
				writer.close();
				out.close();
				response = readFullyAsString(conn.getInputStream(), "UTF-8"); 
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if(conn != null) {
					conn.disconnect();
				}
			}
			
			return response;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public String readFullyAsString(InputStream inputStream, String encoding) throws IOException {
        return readFully(inputStream).toString(encoding);
    }

    private ByteArrayOutputStream readFully(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }

}

package com.tdo.integration.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.tdo.integration.dao.IntegrationTDODataDao;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.data.services.IntegrationCDBService;
import com.tdo.integration.dto.CreditNoteDTO;
import com.tdo.integration.dto.CreditNoteDetailDTO;
import com.tdo.integration.dto.CreditNoteResponseDTO;
import com.tdo.integration.pojo.BaseResponse;
import com.tdo.integration.util.AppConstants;

@Service("ncService")
public class NCService {

	@Autowired 
	IntegrationTDODataDao integrationTDODataDao;
	Properties props;
	@Autowired
	IntegrationCDBService integrationCDBService;

	private static final String SEPARATOR="|";

	private static final String FILEEXTENSION =".txt";
	private static final String HEADER_TYPE="H";
	private static final String DETAIL_TYPE="D";
	private static final String PREFIX_FILENAME="NC";
	private static final String COUNTRY ="MEX";
	private static final String _002 = "002";
	private static final String TASA ="Tasa";
	private static final String _003 ="003";
	
	public void sendCreditNotes(String value) {
		try {
//			List<CreditNoteDTO> creditNotes = getCreditNoteListService();
//			String res =  restService.sendRequestList(props.getProperty("restCreditNotes"), creditNotes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public BaseResponse processCreditNoteService(CreditNoteDTO o) {
		
		BaseResponse response = new BaseResponse();		
		try {
			generateFileCreditNotes(o);
			response.setStatus("OK");
			response.setCode(200);
			response.setContent("se ha enviado a timbrar la NC correctamente");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@SuppressWarnings("unused")
	public void generateFileCreditNotes(CreditNoteDTO o) throws Exception{
		
		String fileName="";
		String fileStoreLocation="";
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		String datetxt=sdf.format(date);
		datetxt=datetxt.replaceAll("/","");
		SimpleDateFormat dsdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");        
        GregorianCalendar gc = new GregorianCalendar();
        String dateString = dsdf.format(gc.getTime());
		
		DecimalFormat fourDecimalFormatter = new DecimalFormat("##.0000");
		int counter=0;
		List<Udc> udcList= integrationCDBService.getUdcByUdcKey(AppConstants.NC_LOCATION);
			if(udcList!=null) {
				Udc udc = udcList.get(0);
				 fileStoreLocation=udc.getUdcValue();
			}
			CreditNoteDTO creditNoteDTO = o;
				
				fileName = PREFIX_FILENAME+creditNoteDTO.getSucursal()+"_"+creditNoteDTO.getFolioNC().toString()+"_"+datetxt+FILEEXTENSION;
		        File newFile = new File(fileStoreLocation + File.separator + fileName);
		        String formaPago="";
		        String metodoPago ="PUE"; 
		        if(!StringUtils.isEmpty(creditNoteDTO.getFormaPago())) {
		        	formaPago= creditNoteDTO.getFormaPago().substring(0, 2);
 		     	   }
		        
		        if(!StringUtils.isEmpty(creditNoteDTO.getMetodoPago())) {
 		     	   metodoPago=creditNoteDTO.getMetodoPago().substring(0, 3);
		        }
		        
		        //FIXED NC Type 
		        creditNoteDTO.setTipoComprobante("E");
		        //FIXED Regimen
		        creditNoteDTO.setRegimen("601");
//			    try (Writer writer = new BufferedWriter(new FileWriter(newFile))) {
			    try (Writer writer =   new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(newFile), StandardCharsets.UTF_8))) {
			    	
			    	String header= HEADER_TYPE +
			    				   SEPARATOR +
			    				   creditNoteDTO.getFolioNC() +
			    		     	   SEPARATOR + 
			    		     	   creditNoteDTO.getTotal() +
			    		     	   SEPARATOR +
			    		     	   creditNoteDTO.getSubtotal() +
			    		     	   SEPARATOR +
			    		     	   creditNoteDTO.getTipoComprobante() +
			    		     	   SEPARATOR+
			    		     	   "01" +// formaPago +
			    		     	   SEPARATOR +
			    		     	   "" +//condiciones de pago = null
			    		     	   SEPARATOR +
			    		     	   metodoPago +
			    		     	   SEPARATOR +
			    		     	   creditNoteDTO.getFolioNC() +
			    		     	   SEPARATOR +
			    		     	    dateString +// creditNoteDTO.getFecha() +
			    				   SEPARATOR +
			    				   //descuento importe =null
   		     	   				   SEPARATOR +
   		     	   				   //fecha de vencimiento = null
   		     	   				   SEPARATOR +
   		     	   				   creditNoteDTO.getRegimen()+
					   			   SEPARATOR +
					   			   creditNoteDTO.getCodPosE() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getRfcE() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getNombreE() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getColoniaE() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getDomicilioE() +
			    				   SEPARATOR +
			    				   "" + 
			    				   SEPARATOR +
			    				   "" + 
			    				   SEPARATOR +
			    				   creditNoteDTO.getCodPosE() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getCiudadE() +//localidad
			    				   SEPARATOR +
			    				   creditNoteDTO.getLocalidadE() +  //23 localidadE=municipio
			    				   SEPARATOR +
			    				   creditNoteDTO.getEstadoE() + //24
			    				   SEPARATOR +
			    				   COUNTRY +
			    				   SEPARATOR +
			    				   creditNoteDTO.getNombreC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getRfcC() +   //27
			    				   SEPARATOR +
			    				   creditNoteDTO.getColoniaC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getDomicilioC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getNumExtC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getNumIntC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getCodPosC() +  //32
			    				   SEPARATOR +
			    				   creditNoteDTO.getLocalidadC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getEstadoC() +  //34
			    				   SEPARATOR +
			    				   COUNTRY + //35
			    				   SEPARATOR +
			    				   creditNoteDTO.getCiudadC() +//localidad receptor
			    				   SEPARATOR +
			    				   creditNoteDTO.getMoneda() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getTipoCambio() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getLetra() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getClienteEmail() +
			    				   SEPARATOR +
			    				   //orden de compra 41
			    				   SEPARATOR +
			    				   //numero de copias //42
			    				   SEPARATOR +
			    				   //skipped fields 43-45
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				    creditNoteDTO.getNumCliente() +//num cliente
			    				   SEPARATOR +
			    				   creditNoteDTO.getNombreC() + //47
			    				   SEPARATOR +
			    				   creditNoteDTO.getRfcC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getColoniaC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getDomicilioC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getNumExtC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getNumIntC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getCodPosC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getCiudadC() +
			    				   SEPARATOR +
			    				   creditNoteDTO.getEstadoC() +
			    				   SEPARATOR +
			    				   COUNTRY + //56
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   _002 + //58
			    				   SEPARATOR +
			    				   TASA +//59
			    				   SEPARATOR +
			    				   //suma valores Detalle.22 donde detalle.21 = 0.16 
			    				   Math.round(creditNoteDTO.getSumIva002_16()*100)/100d +
			    				   SEPARATOR +
			    				   creditNoteDTO.getSumBase002_16() + //61
			    				   SEPARATOR +
			    				   "0.16" +  //62
			    				   SEPARATOR +
			    				   _002  +  //63
			    				   SEPARATOR +
			    				   TASA + //64
			    				   SEPARATOR +
			    				   //65 suma Detalle.22 cuando detalle.21 =0
			    				   Math.round(creditNoteDTO.getSumIva002_0()*100)/100d  +
			    				   SEPARATOR +
			    				   Math.round(creditNoteDTO.getSumBase002_0()*100)/100d  +
			    				   //66 suma Detalle.19 cuando detalle.21 =0.00
			    				   SEPARATOR +
			    				   "0.00" + //67
			    				   SEPARATOR +
			    				   _003 + 
			    				   SEPARATOR +
			    				   TASA +
			    				   SEPARATOR +
			    				   //70 suma Detalle.22 cuando detalle.21 = 0.06
			    				   Math.round(creditNoteDTO.getSumIeps003_06()*100)/100d  +
			    				   SEPARATOR +
			    				   Math.round(creditNoteDTO.getSumBase003_06()*100)/100d +
			    				   //71 sum detalle.19 cuando detalle.21 = 0.06
			    				   SEPARATOR +
			    				   "0.06" + 
			    				   SEPARATOR +
			    				   _003 +  
			    				   SEPARATOR +
			    				   TASA +
			    				   SEPARATOR +
			    				    //75 suma detalle.22 cuando detalle.21 = 0.07
			    				   Math.round(creditNoteDTO.getSumIeps003_07()*100)/100d  +
			    				   SEPARATOR +
			    				   Math.round(creditNoteDTO.getSumBase003_07()*100)/100d  +
			    				   //76 suma detalle.19 cuando detalle.21 = 0.07
			    				   SEPARATOR +
			    				   "0.07" + //77
			    				   SEPARATOR +
			    				   _003 +
			    				   SEPARATOR +
			    				   TASA +
			    				   SEPARATOR +
			    				   //80 Suma detalle.22 cuando detalle.21 = 0.09
			    				   Math.round(creditNoteDTO.getSumIeps003_09()*100)/100d  +
			    				   SEPARATOR +
			    				   //81 suma detalle.19 cuando detalle.21 = 0.09
			    				   Math.round(creditNoteDTO.getSumBase003_09()*100)/100d  +
			    				   SEPARATOR +
			    				   "0.09"+
			    				   SEPARATOR +
			    				   "01"+
			    				   SEPARATOR +
			    				   creditNoteDTO.getUuidFactura() + //84
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   "G02" + //87
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   SEPARATOR +
			    				   SEPARATOR
			    		     	   ;
			    	header.replaceAll("null","");
			        writer.write(header);
//			        writer.write(System.lineSeparator());
			        
			        for (CreditNoteDetailDTO detail : creditNoteDTO.getDetail()) {
			        	double precioUnitario = 0;
			        	String fechaPedimento ="";
			        	if(detail.getFechaPedimento()!=null) {
			        		Date datePedimento = Calendar.getInstance().getTime();  
			        		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
			        		fechaPedimento = dateFormat.format(datePedimento);  
			        	}
			        	if( detail.getPrecioUnitario()!=null) {
			        		precioUnitario=detail.getPrecioUnitario().doubleValue();
			        		fourDecimalFormatter.format(precioUnitario);
			        	}
			        	String detailStr ="";
			        		   detailStr = DETAIL_TYPE +
			        				       SEPARATOR +
			        				       creditNoteDTO.getFolioNC() +
			        				       SEPARATOR +
			        				       detail.getNumLinea() +//numero de linea 
			        				       SEPARATOR +
			        				       detail.getImporte() + 
			        				       SEPARATOR +
			        				       "0" +//numero de Lote 
			        				       SEPARATOR +
			        				       detail.getDescripcion() +
			        				       SEPARATOR +
			        				       detail.getUnidadMedida() +//unidad de medida
			        				       SEPARATOR +
			        				       fourDecimalFormatter.format(precioUnitario) +
			        				       SEPARATOR +
			        				       detail.getCantidad() +
			        	       			   SEPARATOR +
			        	       			   detail.getTipoCambio() +
			        	       		       SEPARATOR +
			        	       		       detail.getClave() + //numero Articulo
			        	       		       SEPARATOR +
			        	       		       fechaPedimento +
			        	       		       SEPARATOR +
			        	       		       detail.getNombreAduana()+
			        	       		       SEPARATOR +//aduana 13
			        	       		       detail.getNumerodocAduanero() +
			        	       		       SEPARATOR +
			        	       		       "" + //fecha caducidad lote
			        	       		       SEPARATOR +
			        	       		       detail.getClaveProdServ() +
			        	       		       SEPARATOR +
			        	       		       detail.getClaveUnidad() +
			        	       		       SEPARATOR +
			        	       		       detail.getTipoImpuestoTrasladado()+
			        	       		       SEPARATOR +
			        	       		       detail.getBaseImpuestoTrasladado() +  
			        	       		       SEPARATOR +
			        	       		       TASA +
			        	       		       SEPARATOR +
			        	       		       detail.getTasaImpuestoTrasladado() +
			        	       		       SEPARATOR +
			        	       		       Math.round((detail.getTasaImpuestoTrasladado().doubleValue() * detail.getBaseImpuestoTrasladado().doubleValue())*100)/100d +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR +
			        	       		       SEPARATOR;
			            detailStr.replaceAll("null","");	   
 			            writer.write(System.lineSeparator());
			        	writer.write(detailStr);
					}
			    } catch (IOException e) {
			        e.printStackTrace();
			     }
			
	    }
	
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	public List<CreditNoteResponseDTO> getTxtResponse() throws IOException{
		
			List<CreditNoteResponseDTO> responseList = new ArrayList();
			List<Udc> udcList= integrationTDODataDao.getUdcByUdcKey(AppConstants.RESPONSE_TXT_PATH);
			if(udcList!=null) {
				Udc udc = udcList.get(0);
				final File folderFile = new File(udc.getUdcValue());
					boolean resultado;
			       if ((resultado = folderFile.exists())) {
			            File[] files = folderFile.listFiles();
			            for (File file : files) {
			            	if(!file.getName().contains(AppConstants.PROCCESEDFILE_SUFFIX)) {
			            	CreditNoteResponseDTO cndto = new CreditNoteResponseDTO();
			            	Map<String,String> respTxtMap = new HashMap<String,String>();
			                BufferedReader br = new BufferedReader(new FileReader(file));
			                StringBuilder out = new StringBuilder();
			                String response = br.readLine();
			                String nc =file.getName();
			                String sucursal = nc.substring(2,4);
			                int end = nc.indexOf("_"+2);
			                nc=nc.substring(5,end);
			                nc=nc.replace(AppConstants.NC_PREFIX,"");
			                	cndto.setFolio(nc);
			                	cndto.setSucursal(sucursal);
			                	respTxtMap.put(nc, response);
			                	cndto.setResponseMap(respTxtMap);
			                if(out.length()>0) {
			                }
			                br.close();
			               responseList.add(cndto);
//			               File processedFile = new File(file.getName()+AppConstants.PROCCESEDFILE_SUFFIX);
//			               file.renameTo(processedFile);
			            }
			            }
			        }
			}      
		   return responseList;   
	}
	
}

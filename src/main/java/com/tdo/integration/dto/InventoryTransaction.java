package com.tdo.integration.dto;

import com.tdo.integration.data.model.tdo.FileTransferControl;

public class InventoryTransaction {
	
	private InventoryTransactionV1 inventoryTransactionV1;
	private FileTransferControl fileTransferControl;
	
	public InventoryTransactionV1 getInventoryTransactionV1() {
		return inventoryTransactionV1;
	}
	public void setInventoryTransactionV1(InventoryTransactionV1 inventoryTransactionV1) {
		this.inventoryTransactionV1 = inventoryTransactionV1;
	}
	public FileTransferControl getFileTransferControl() {
		return fileTransferControl;
	}
	public void setFileTransferControl(FileTransferControl fileTransferControl) {
		this.fileTransferControl = fileTransferControl;
	}

}

package com.tdo.integration.dto;

import java.math.BigDecimal;
import java.util.Date;

public class InventoryMonitorDTO {

	private int branchId;
	private String branchName;
	private String branchCode;
	private BigDecimal itemCount;
	private BigDecimal lotCount;
	private BigDecimal cloudCount;
	private Date lastUpdate;
	private String status;
	
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public BigDecimal getItemCount() {
		return itemCount;
	}
	public void setItemCount(BigDecimal itemCount) {
		this.itemCount = itemCount;
	}
	public BigDecimal getLotCount() {
		return lotCount;
	}
	public void setLotCount(BigDecimal lotCount) {
		this.lotCount = lotCount;
	}
	public BigDecimal getCloudCount() {
		return cloudCount;
	}
	public void setCloudCount(BigDecimal cloudCount) {
		this.cloudCount = cloudCount;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
		
}

package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class InventoryTransactionCost {
	
	@JsonProperty("TRANSACTION_COST_IDENTIFIER")
	private String TRANSACTION_COST_IDENTIFIER;
	@JsonProperty("COST_COMPONENT_CODE")
	private String COST_COMPONENT_CODE;
	@JsonProperty("COST")
	private String COST;
	
	public String getTRANSACTION_COST_IDENTIFIER() {
		return TRANSACTION_COST_IDENTIFIER;
	}
	public void setTRANSACTION_COST_IDENTIFIER(String tRANSACTION_COST_IDENTIFIER) {
		TRANSACTION_COST_IDENTIFIER = tRANSACTION_COST_IDENTIFIER;
	}
	public String getCOST_COMPONENT_CODE() {
		return COST_COMPONENT_CODE;
	}
	public void setCOST_COMPONENT_CODE(String cOST_COMPONENT_CODE) {
		COST_COMPONENT_CODE = cOST_COMPONENT_CODE;
	}
	public String getCOST() {
		return COST;
	}
	public void setCOST(String cOST) {
		COST = cOST;
	}
}

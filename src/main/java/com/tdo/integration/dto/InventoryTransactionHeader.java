package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class InventoryTransactionHeader {
	
	@JsonProperty("ORGANIZATION_NAME")
	private String ORGANIZATION_NAME;
	@JsonProperty("TRANSACTION_GROUP_ID")
	private String TRANSACTION_GROUP_ID;
	@JsonProperty("TRANSACTION_GROUP_SEQ")
	private String TRANSACTION_GROUP_SEQ;
	@JsonProperty("TRANSACTION_BATCH_ID")
	private String TRANSACTION_BATCH_ID;
	@JsonProperty("TRANSACTION_BATCH_SEQ")
	private String TRANSACTION_BATCH_SEQ;
	@JsonProperty("PROCESS_FLAG")
	private String PROCESS_FLAG;
	@JsonProperty("INVENTORY_ITEM")
	private String INVENTORY_ITEM;
	@JsonProperty("ITEM_NUMBER")
	private String ITEM_NUMBER;
	@JsonProperty("REVISION")
	private String REVISION;
	@JsonProperty("INV_LOTSERIAL_INTERFACE_NUM")
	private String INV_LOTSERIAL_INTERFACE_NUM;
	@JsonProperty("SUBINVENTORY_CODE")
	private String SUBINVENTORY_CODE;
	@JsonProperty("LOCATOR_NAME")
	private String LOCATOR_NAME;
	@JsonProperty("LOC_SEGMENT1")
	private String LOC_SEGMENT1;
	@JsonProperty("LOC_SEGMENT2")
	private String LOC_SEGMENT2;
	@JsonProperty("LOC_SEGMENT3")
	private String LOC_SEGMENT3;
	@JsonProperty("LOC_SEGMENT4")
	private String LOC_SEGMENT4;
	@JsonProperty("LOC_SEGMENT5")
	private String LOC_SEGMENT5;
	@JsonProperty("LOC_SEGMENT6")
	private String LOC_SEGMENT6;
	@JsonProperty("LOC_SEGMENT7")
	private String LOC_SEGMENT7;
	@JsonProperty("LOC_SEGMENT8")
	private String LOC_SEGMENT8;
	@JsonProperty("LOC_SEGMENT9")
	private String LOC_SEGMENT9;
	@JsonProperty("LOC_SEGMENT10")
	private String LOC_SEGMENT10;
	@JsonProperty("LOC_SEGMENT11")
	private String LOC_SEGMENT11;
	@JsonProperty("LOC_SEGMENT12")
	private String LOC_SEGMENT12;
	@JsonProperty("LOC_SEGMENT13")
	private String LOC_SEGMENT13;
	@JsonProperty("LOC_SEGMENT14")
	private String LOC_SEGMENT14;
	@JsonProperty("LOC_SEGMENT15")
	private String LOC_SEGMENT15;
	@JsonProperty("LOC_SEGMENT16")
	private String LOC_SEGMENT16;
	@JsonProperty("LOC_SEGMENT17")
	private String LOC_SEGMENT17;
	@JsonProperty("LOC_SEGMENT18")
	private String LOC_SEGMENT18;
	@JsonProperty("LOC_SEGMENT19")
	private String LOC_SEGMENT19;
	@JsonProperty("LOC_SEGMENT20")
	private String LOC_SEGMENT20;
	@JsonProperty("TRANSACTION_QUANTITY")
	private String TRANSACTION_QUANTITY;
	@JsonProperty("TRANSACTION_UOM")
	private String TRANSACTION_UOM;
	@JsonProperty("TRANSACTION_UNIT_OF_MEASURE")
	private String TRANSACTION_UNIT_OF_MEASURE;
	@JsonProperty("RESERVATION_QUANTITY")
	private String RESERVATION_QUANTITY;
	@JsonProperty("TRANSACTION_DATE")
	private String TRANSACTION_DATE;
	@JsonProperty("TRANSACTION_SOURCE_TYPE_NAME")
	private String TRANSACTION_SOURCE_TYPE_NAME;
	@JsonProperty("TRANSACTION_TYPE_NAME")
	private String TRANSACTION_TYPE_NAME;
	@JsonProperty("TRANSFER_ORGANIZATION_TYPE")
	private String TRANSFER_ORGANIZATION_TYPE;
	@JsonProperty("TRANSFER_ORGANIZATION_NAME")
	private String TRANSFER_ORGANIZATION_NAME;
	@JsonProperty("TRANSFER_SUBINVENTORY")
	private String TRANSFER_SUBINVENTORY;
	@JsonProperty("XFER_LOC_SEGMENT1")
	private String XFER_LOC_SEGMENT1;
	@JsonProperty("XFER_LOC_SEGMENT2")
	private String XFER_LOC_SEGMENT2;
	@JsonProperty("XFER_LOC_SEGMENT3")
	private String XFER_LOC_SEGMENT3;
	@JsonProperty("XFER_LOC_SEGMENT4")
	private String XFER_LOC_SEGMENT4;
	@JsonProperty("XFER_LOC_SEGMENT5")
	private String XFER_LOC_SEGMENT5;
	@JsonProperty("XFER_LOC_SEGMENT6")
	private String XFER_LOC_SEGMENT6;
	@JsonProperty("XFER_LOC_SEGMENT7")
	private String XFER_LOC_SEGMENT7;
	@JsonProperty("XFER_LOC_SEGMENT8")
	private String XFER_LOC_SEGMENT8;
	@JsonProperty("XFER_LOC_SEGMENT9")
	private String XFER_LOC_SEGMENT9;
	@JsonProperty("XFER_LOC_SEGMENT10")
	private String XFER_LOC_SEGMENT10;
	@JsonProperty("XFER_LOC_SEGMENT11")
	private String XFER_LOC_SEGMENT11;
	@JsonProperty("XFER_LOC_SEGMENT12")
	private String XFER_LOC_SEGMENT12;
	@JsonProperty("XFER_LOC_SEGMENT13")
	private String XFER_LOC_SEGMENT13;
	@JsonProperty("XFER_LOC_SEGMENT14")
	private String XFER_LOC_SEGMENT14;
	@JsonProperty("XFER_LOC_SEGMENT15")
	private String XFER_LOC_SEGMENT15;
	@JsonProperty("XFER_LOC_SEGMENT16")
	private String XFER_LOC_SEGMENT16;
	@JsonProperty("XFER_LOC_SEGMENT17")
	private String XFER_LOC_SEGMENT17;
	@JsonProperty("XFER_LOC_SEGMENT18")
	private String XFER_LOC_SEGMENT18;
	@JsonProperty("XFER_LOC_SEGMENT19")
	private String XFER_LOC_SEGMENT19;
	@JsonProperty("XFER_LOC_SEGMENT20")
	private String XFER_LOC_SEGMENT20;
	@JsonProperty("PRIMARY_QUANTITY")
	private String PRIMARY_QUANTITY;
	@JsonProperty("SECONDARY_TRANSACTION_QUANTITY")
	private String SECONDARY_TRANSACTION_QUANTITY;
	@JsonProperty("SECONDARY_UOM_CODE")
	private String SECONDARY_UOM_CODE;
	@JsonProperty("SECONDARY_UNIT_OF_MEASURE")
	private String SECONDARY_UNIT_OF_MEASURE;
	@JsonProperty("SOURCE_CODE")
	private String SOURCE_CODE;
	@JsonProperty("SOURCE_HEADER_ID")
	private String SOURCE_HEADER_ID;
	@JsonProperty("SOURCE_LINE_ID")
	private String SOURCE_LINE_ID;
	@JsonProperty("TRANSACTION_SOURCE_NAME")
	private String TRANSACTION_SOURCE_NAME;
	@JsonProperty("DSP_SEGMENT1")
	private String DSP_SEGMENT1;
	@JsonProperty("DSP_SEGMENT2")
	private String DSP_SEGMENT2;
	@JsonProperty("DSP_SEGMENT3")
	private String DSP_SEGMENT3;
	@JsonProperty("DSP_SEGMENT4")
	private String DSP_SEGMENT4;
	@JsonProperty("DSP_SEGMENT5")
	private String DSP_SEGMENT5;
	@JsonProperty("DSP_SEGMENT6")
	private String DSP_SEGMENT6;
	@JsonProperty("DSP_SEGMENT7")
	private String DSP_SEGMENT7;
	@JsonProperty("DSP_SEGMENT8")
	private String DSP_SEGMENT8;
	@JsonProperty("DSP_SEGMENT9")
	private String DSP_SEGMENT9;
	@JsonProperty("DSP_SEGMENT10")
	private String DSP_SEGMENT10;
	@JsonProperty("DSP_SEGMENT11")
	private String DSP_SEGMENT11;
	@JsonProperty("DSP_SEGMENT12")
	private String DSP_SEGMENT12;
	@JsonProperty("DSP_SEGMENT13")
	private String DSP_SEGMENT13;
	@JsonProperty("DSP_SEGMENT14")
	private String DSP_SEGMENT14;
	@JsonProperty("DSP_SEGMENT15")
	private String DSP_SEGMENT15;
	@JsonProperty("DSP_SEGMENT16")
	private String DSP_SEGMENT16;
	@JsonProperty("DSP_SEGMENT17")
	private String DSP_SEGMENT17;
	@JsonProperty("DSP_SEGMENT18")
	private String DSP_SEGMENT18;
	@JsonProperty("DSP_SEGMENT19")
	private String DSP_SEGMENT19;
	@JsonProperty("DSP_SEGMENT20")
	private String DSP_SEGMENT20;
	@JsonProperty("DSP_SEGMENT21")
	private String DSP_SEGMENT21;
	@JsonProperty("DSP_SEGMENT22")
	private String DSP_SEGMENT22;
	@JsonProperty("DSP_SEGMENT23")
	private String DSP_SEGMENT23;
	@JsonProperty("DSP_SEGMENT24")
	private String DSP_SEGMENT24;
	@JsonProperty("DSP_SEGMENT25")
	private String DSP_SEGMENT25;
	@JsonProperty("DSP_SEGMENT26")
	private String DSP_SEGMENT26;
	@JsonProperty("DSP_SEGMENT27")
	private String DSP_SEGMENT27;
	@JsonProperty("DSP_SEGMENT28")
	private String DSP_SEGMENT28;
	@JsonProperty("DSP_SEGMENT29")
	private String DSP_SEGMENT29;
	@JsonProperty("DSP_SEGMENT30")
	private String DSP_SEGMENT30;
	@JsonProperty("TRANSACTION_ACTION_NAME")
	private String TRANSACTION_ACTION_NAME;
	@JsonProperty("TRANSACTION_MODE")
	private String TRANSACTION_MODE;
	@JsonProperty("LOCK_FLAG")
	private String LOCK_FLAG;
	@JsonProperty("TRANSACTION_REFERENCE")
	private String TRANSACTION_REFERENCE;
	@JsonProperty("REASON_NAME")
	private String REASON_NAME;
	@JsonProperty("CURRENCY_NAME")
	private String CURRENCY_NAME;
	@JsonProperty("CURRENCY_CODE")
	private String CURRENCY_CODE;
	@JsonProperty("CURRENCY_CONVERSION_TYPE")
	private String CURRENCY_CONVERSION_TYPE;
	@JsonProperty("CURRENCY_CONVERSION_RATE")
	private String CURRENCY_CONVERSION_RATE;
	@JsonProperty("CURRENCY_CONVERSION_DATE")
	private String CURRENCY_CONVERSION_DATE;
	@JsonProperty("TRANSACTION_COST")
	private String TRANSACTION_COST;
	@JsonProperty("TRANSFER_COST")
	private String TRANSFER_COST;
	@JsonProperty("NEW_AVERAGE_COST")
	private String NEW_AVERAGE_COST;
	@JsonProperty("VALUE_CHANGE")
	private String VALUE_CHANGE;
	@JsonProperty("PERCENTAGE_CHANGE")
	private String PERCENTAGE_CHANGE;
	@JsonProperty("DST_SEGMENT1")
	private String DST_SEGMENT1;
	@JsonProperty("DST_SEGMENT2")
	private String DST_SEGMENT2;
	@JsonProperty("DST_SEGMENT3")
	private String DST_SEGMENT3;
	@JsonProperty("DST_SEGMENT4")
	private String DST_SEGMENT4;
	@JsonProperty("DST_SEGMENT5")
	private String DST_SEGMENT5;
	@JsonProperty("DST_SEGMENT6")
	private String DST_SEGMENT6;
	@JsonProperty("DST_SEGMENT7")
	private String DST_SEGMENT7;
	@JsonProperty("DST_SEGMENT8")
	private String DST_SEGMENT8;
	@JsonProperty("DST_SEGMENT9")
	private String DST_SEGMENT9;
	@JsonProperty("DST_SEGMENT10")
	private String DST_SEGMENT10;
	@JsonProperty("DST_SEGMENT11")
	private String DST_SEGMENT11;
	@JsonProperty("DST_SEGMENT12")
	private String DST_SEGMENT12;
	@JsonProperty("DST_SEGMENT13")
	private String DST_SEGMENT13;
	@JsonProperty("DST_SEGMENT14")
	private String DST_SEGMENT14;
	@JsonProperty("DST_SEGMENT15")
	private String DST_SEGMENT15;
	@JsonProperty("DST_SEGMENT16")
	private String DST_SEGMENT16;
	@JsonProperty("DST_SEGMENT17")
	private String DST_SEGMENT17;
	@JsonProperty("DST_SEGMENT18")
	private String DST_SEGMENT18;
	@JsonProperty("DST_SEGMENT19")
	private String DST_SEGMENT19;
	@JsonProperty("DST_SEGMENT20")
	private String DST_SEGMENT20;
	@JsonProperty("DST_SEGMENT21")
	private String DST_SEGMENT21;
	@JsonProperty("DST_SEGMENT22")
	private String DST_SEGMENT22;
	@JsonProperty("DST_SEGMENT23")
	private String DST_SEGMENT23;
	@JsonProperty("DST_SEGMENT24")
	private String DST_SEGMENT24;
	@JsonProperty("DST_SEGMENT25")
	private String DST_SEGMENT25;
	@JsonProperty("DST_SEGMENT26")
	private String DST_SEGMENT26;
	@JsonProperty("DST_SEGMENT27")
	private String DST_SEGMENT27;
	@JsonProperty("DST_SEGMENT28")
	private String DST_SEGMENT28;
	@JsonProperty("DST_SEGMENT29")
	private String DST_SEGMENT29;
	@JsonProperty("DST_SEGMENT30")
	private String DST_SEGMENT30;
	@JsonProperty("LOCATION_TYPE")
	private String LOCATION_TYPE;
	@JsonProperty("EMPLOYEE_CODE")
	private String EMPLOYEE_CODE;
	@JsonProperty("RECEIVING_DOCUMENT")
	private String RECEIVING_DOCUMENT;
	@JsonProperty("LINE_ITEM_NUM")
	private String LINE_ITEM_NUM;
	@JsonProperty("SHIPMENT_NUMBER")
	private String SHIPMENT_NUMBER;
	@JsonProperty("TRANSPORTATION_COST")
	private String TRANSPORTATION_COST;
	@JsonProperty("CONTAINERS")
	private String CONTAINERS;
	@JsonProperty("WAYBILL_AIRBILL")
	private String WAYBILL_AIRBILL;
	@JsonProperty("EXPECTED_ARRIVAL_DATE")
	private String EXPECTED_ARRIVAL_DATE;
	@JsonProperty("REQUIRED_FLAG")
	private String REQUIRED_FLAG;
	@JsonProperty("SHIPPABLE_FLAG")
	private String SHIPPABLE_FLAG;
	@JsonProperty("SHIPPED_QUANTITY")
	private String SHIPPED_QUANTITY;
	@JsonProperty("VALIDATION_REQUIRED")
	private String VALIDATION_REQUIRED;
	@JsonProperty("NEGATIVE_REQ_FLAG")
	private String NEGATIVE_REQ_FLAG;
	@JsonProperty("OWNING_TP_TYPE")
	private String OWNING_TP_TYPE;
	@JsonProperty("TRANSFER_OWNING_TP_TYPE")
	private String TRANSFER_OWNING_TP_TYPE;
	@JsonProperty("OWNING_ORGANIZATION_NAME")
	private String OWNING_ORGANIZATION_NAME;
	@JsonProperty("XFR_OWNING_ORGANIZATION_NAME")
	private String XFR_OWNING_ORGANIZATION_NAME;
	@JsonProperty("TRANSFER_PERCENTAGE")
	private String TRANSFER_PERCENTAGE;
	@JsonProperty("PLANNING_TP_TYPE")
	private String PLANNING_TP_TYPE;
	@JsonProperty("TRANSFER_PLANNING_TP_TYPE")
	private String TRANSFER_PLANNING_TP_TYPE;
	@JsonProperty("ROUTING_REVISION")
	private String ROUTING_REVISION;
	@JsonProperty("ROUTING_REVISION_DATE")
	private String ROUTING_REVISION_DATE;
	@JsonProperty("ALTERNATE_BOM_DESIGNATOR")
	private String ALTERNATE_BOM_DESIGNATOR;
	@JsonProperty("ALTERNATE_ROUTING_DESIGNATOR")
	private String ALTERNATE_ROUTING_DESIGNATOR;
	@JsonProperty("ORGANIZATION_TYPE")
	private String ORGANIZATION_TYPE;
	@JsonProperty("USSGL_TRANSACTION_CODE")
	private String USSGL_TRANSACTION_CODE;
	@JsonProperty("WIP_ENTITY_TYPE")
	private String WIP_ENTITY_TYPE;
	@JsonProperty("SCHEDULE_UPDATE_CODE")
	private String SCHEDULE_UPDATE_CODE;
	@JsonProperty("SETUP_TEARDOWN_CODE")
	private String SETUP_TEARDOWN_CODE;
	@JsonProperty("PRIMARY_SWITCH")
	private String PRIMARY_SWITCH;
	@JsonProperty("MRP_CODE")
	private String MRP_CODE;
	@JsonProperty("OPERATION_SEQ_NUM")
	private String OPERATION_SEQ_NUM;
	@JsonProperty("WIP_SUPPLY_TYPE")
	private String WIP_SUPPLY_TYPE;
	@JsonProperty("RELIEVE_RESERVATIONS_FLAG")
	private String RELIEVE_RESERVATIONS_FLAG;
	@JsonProperty("RELIEVE_HIGH_LEVEL_RSV_FLAG")
	private String RELIEVE_HIGH_LEVEL_RSV_FLAG;
	@JsonProperty("TRANSFER_PRICE")
	private String TRANSFER_PRICE;
	@JsonProperty("BUILD_BREAK_TO_UOM")
	private String BUILD_BREAK_TO_UOM;
	@JsonProperty("BUILD_BREAK_TO_UNIT_OF_MEASURE")
	private String BUILD_BREAK_TO_UNIT_OF_MEASURE;
	@JsonProperty("ATTRIBUTE_CATEGORY")
	private String ATTRIBUTE_CATEGORY;
	@JsonProperty("ATTRIBUTE1")
	private String ATTRIBUTE1;
	@JsonProperty("ATTRIBUTE2")
	private String ATTRIBUTE2;
	@JsonProperty("ATTRIBUTE3")
	private String ATTRIBUTE3;
	@JsonProperty("ATTRIBUTE4")
	private String ATTRIBUTE4;
	@JsonProperty("ATTRIBUTE5")
	private String ATTRIBUTE5;
	@JsonProperty("ATTRIBUTE6")
	private String ATTRIBUTE6;
	@JsonProperty("ATTRIBUTE7")
	private String ATTRIBUTE7;
	@JsonProperty("ATTRIBUTE8")
	private String ATTRIBUTE8;
	@JsonProperty("ATTRIBUTE9")
	private String ATTRIBUTE9;
	@JsonProperty("ATTRIBUTE10")
	private String ATTRIBUTE10;
	@JsonProperty("ATTRIBUTE11")
	private String ATTRIBUTE11;
	@JsonProperty("ATTRIBUTE12")
	private String ATTRIBUTE12;
	@JsonProperty("ATTRIBUTE13")
	private String ATTRIBUTE13;
	@JsonProperty("ATTRIBUTE14")
	private String ATTRIBUTE14;
	@JsonProperty("ATTRIBUTE15")
	private String ATTRIBUTE15;
	@JsonProperty("ATTRIBUTE16")
	private String ATTRIBUTE16;
	@JsonProperty("ATTRIBUTE17")
	private String ATTRIBUTE17;
	@JsonProperty("ATTRIBUTE18")
	private String ATTRIBUTE18;
	@JsonProperty("ATTRIBUTE19")
	private String ATTRIBUTE19;
	@JsonProperty("ATTRIBUTE20")
	private String ATTRIBUTE20;
	@JsonProperty("ATTRIBUTE_NUMBER1")
	private String ATTRIBUTE_NUMBER1;
	@JsonProperty("ATTRIBUTE_NUMBER2")
	private String ATTRIBUTE_NUMBER2;
	@JsonProperty("ATTRIBUTE_NUMBER3")
	private String ATTRIBUTE_NUMBER3;
	@JsonProperty("ATTRIBUTE_NUMBER4")
	private String ATTRIBUTE_NUMBER4;
	@JsonProperty("ATTRIBUTE_NUMBER5")
	private String ATTRIBUTE_NUMBER5;
	@JsonProperty("ATTRIBUTE_NUMBER6")
	private String ATTRIBUTE_NUMBER6;
	@JsonProperty("ATTRIBUTE_NUMBER7")
	private String ATTRIBUTE_NUMBER7;
	@JsonProperty("ATTRIBUTE_NUMBER8")
	private String ATTRIBUTE_NUMBER8;
	@JsonProperty("ATTRIBUTE_NUMBER9")
	private String ATTRIBUTE_NUMBER9;
	@JsonProperty("ATTRIBUTE_NUMBER10")
	private String ATTRIBUTE_NUMBER10;
	@JsonProperty("ATTRIBUTE_DATE1")
	private String ATTRIBUTE_DATE1;
	@JsonProperty("ATTRIBUTE_DATE2")
	private String ATTRIBUTE_DATE2;
	@JsonProperty("ATTRIBUTE_DATE3")
	private String ATTRIBUTE_DATE3;
	@JsonProperty("ATTRIBUTE_DATE4")
	private String ATTRIBUTE_DATE4;
	@JsonProperty("ATTRIBUTE_DATE5")
	private String ATTRIBUTE_DATE5;
	@JsonProperty("ATTRIBUTE_TIMESTAMP1")
	private String ATTRIBUTE_TIMESTAMP1;
	@JsonProperty("ATTRIBUTE_TIMESTAMP2")
	private String ATTRIBUTE_TIMESTAMP2;
	@JsonProperty("ATTRIBUTE_TIMESTAMP3")
	private String ATTRIBUTE_TIMESTAMP3;
	@JsonProperty("ATTRIBUTE_TIMESTAMP4")
	private String ATTRIBUTE_TIMESTAMP4;
	@JsonProperty("ATTRIBUTE_TIMESTAMP5")
	private String ATTRIBUTE_TIMESTAMP5;
	@JsonProperty("TRANSACTION_COST_IDENTIFIER")
	private String TRANSACTION_COST_IDENTIFIER;
	@JsonProperty("DEFAULT_TAXATION_COUNTRY")
	private String DEFAULT_TAXATION_COUNTRY;
	@JsonProperty("DOCUMENT_SUB_TYPE")
	private String DOCUMENT_SUB_TYPE;
	@JsonProperty("TRX_BUSINESS_CATEGORY")
	private String TRX_BUSINESS_CATEGORY;
	@JsonProperty("USER_DEFINED_FISC_CLASS")
	private String USER_DEFINED_FISC_CLASS;
	@JsonProperty("TAX_INVOICE_NUMBER")
	private String TAX_INVOICE_NUMBER;
	@JsonProperty("TAX_INVOICE_DATE")
	private String TAX_INVOICE_DATE;
	@JsonProperty("PRODUCT_CATEGORY")
	private String PRODUCT_CATEGORY;
	@JsonProperty("PRODUCT_TYPE")
	private String PRODUCT_TYPE;
	@JsonProperty("ASSESSABLE_VALUE")
	private String ASSESSABLE_VALUE;
	@JsonProperty("TAX_CLASSIFICATION_CODE")
	private String TAX_CLASSIFICATION_CODE;
	@JsonProperty("EXEMPT_CERTIFICATE_NUMBER")
	private String EXEMPT_CERTIFICATE_NUMBER;
	@JsonProperty("EXEMPT_REASON_CODE")
	private String EXEMPT_REASON_CODE;
	@JsonProperty("INTENDED_USE")
	private String INTENDED_USE;
	@JsonProperty("FIRST_PTY_NUMBER")
	private String FIRST_PTY_NUMBER;
	@JsonProperty("THIRD_PTY_NUMBER")
	private String THIRD_PTY_NUMBER;
	@JsonProperty("FINAL_DISCHARGE_LOC_CODE")
	private String FINAL_DISCHARGE_LOC_CODE;
	@JsonProperty("CATEGORY_NAME")
	private String CATEGORY_NAME;
	@JsonProperty("OWNING_ORGANIZATION_ID")
	private String OWNING_ORGANIZATION_ID;
	@JsonProperty("XFR_OWNING_ORGANIZATION_ID")
	private String XFR_OWNING_ORGANIZATION_ID;
	@JsonProperty("PRC_BU_NAME")
	private String PRC_BU_NAME;
	@JsonProperty("VENDOR_NAME")
	private String VENDOR_NAME;
	@JsonProperty("VENDOR_NUMBER")
	private String VENDOR_NUMBER;
	@JsonProperty("CONSIGNMENT_AGREEMENT_NUM")
	private String CONSIGNMENT_AGREEMENT_NUM;
	@JsonProperty("USE_CURRENT_COST")
	private String USE_CURRENT_COST;
	@JsonProperty("EXTERNAL_SYSTEM_PACKING_UNIT")
	private String EXTERNAL_SYSTEM_PACKING_UNIT;
	@JsonProperty("TRANSFER_LOCATOR_NAME")
	private String TRANSFER_LOCATOR_NAME;
	public String getORGANIZATION_NAME() {
		return ORGANIZATION_NAME;
	}
	public void setORGANIZATION_NAME(String oRGANIZATION_NAME) {
		ORGANIZATION_NAME = oRGANIZATION_NAME;
	}
	public String getTRANSACTION_GROUP_ID() {
		return TRANSACTION_GROUP_ID;
	}
	public void setTRANSACTION_GROUP_ID(String tRANSACTION_GROUP_ID) {
		TRANSACTION_GROUP_ID = tRANSACTION_GROUP_ID;
	}
	public String getTRANSACTION_GROUP_SEQ() {
		return TRANSACTION_GROUP_SEQ;
	}
	public void setTRANSACTION_GROUP_SEQ(String tRANSACTION_GROUP_SEQ) {
		TRANSACTION_GROUP_SEQ = tRANSACTION_GROUP_SEQ;
	}
	public String getTRANSACTION_BATCH_ID() {
		return TRANSACTION_BATCH_ID;
	}
	public void setTRANSACTION_BATCH_ID(String tRANSACTION_BATCH_ID) {
		TRANSACTION_BATCH_ID = tRANSACTION_BATCH_ID;
	}
	public String getTRANSACTION_BATCH_SEQ() {
		return TRANSACTION_BATCH_SEQ;
	}
	public void setTRANSACTION_BATCH_SEQ(String tRANSACTION_BATCH_SEQ) {
		TRANSACTION_BATCH_SEQ = tRANSACTION_BATCH_SEQ;
	}
	public String getPROCESS_FLAG() {
		return PROCESS_FLAG;
	}
	public void setPROCESS_FLAG(String pROCESS_FLAG) {
		PROCESS_FLAG = pROCESS_FLAG;
	}
	public String getINVENTORY_ITEM() {
		return INVENTORY_ITEM;
	}
	public void setINVENTORY_ITEM(String iNVENTORY_ITEM) {
		INVENTORY_ITEM = iNVENTORY_ITEM;
	}
	public String getITEM_NUMBER() {
		return ITEM_NUMBER;
	}
	public void setITEM_NUMBER(String iTEM_NUMBER) {
		ITEM_NUMBER = iTEM_NUMBER;
	}
	public String getREVISION() {
		return REVISION;
	}
	public void setREVISION(String rEVISION) {
		REVISION = rEVISION;
	}
	public String getINV_LOTSERIAL_INTERFACE_NUM() {
		return INV_LOTSERIAL_INTERFACE_NUM;
	}
	public void setINV_LOTSERIAL_INTERFACE_NUM(String iNV_LOTSERIAL_INTERFACE_NUM) {
		INV_LOTSERIAL_INTERFACE_NUM = iNV_LOTSERIAL_INTERFACE_NUM;
	}
	public String getSUBINVENTORY_CODE() {
		return SUBINVENTORY_CODE;
	}
	public void setSUBINVENTORY_CODE(String sUBINVENTORY_CODE) {
		SUBINVENTORY_CODE = sUBINVENTORY_CODE;
	}
	public String getLOCATOR_NAME() {
		return LOCATOR_NAME;
	}
	public void setLOCATOR_NAME(String lOCATOR_NAME) {
		LOCATOR_NAME = lOCATOR_NAME;
	}
	public String getLOC_SEGMENT1() {
		return LOC_SEGMENT1;
	}
	public void setLOC_SEGMENT1(String lOC_SEGMENT1) {
		LOC_SEGMENT1 = lOC_SEGMENT1;
	}
	public String getLOC_SEGMENT2() {
		return LOC_SEGMENT2;
	}
	public void setLOC_SEGMENT2(String lOC_SEGMENT2) {
		LOC_SEGMENT2 = lOC_SEGMENT2;
	}
	public String getLOC_SEGMENT3() {
		return LOC_SEGMENT3;
	}
	public void setLOC_SEGMENT3(String lOC_SEGMENT3) {
		LOC_SEGMENT3 = lOC_SEGMENT3;
	}
	public String getLOC_SEGMENT4() {
		return LOC_SEGMENT4;
	}
	public void setLOC_SEGMENT4(String lOC_SEGMENT4) {
		LOC_SEGMENT4 = lOC_SEGMENT4;
	}
	public String getLOC_SEGMENT5() {
		return LOC_SEGMENT5;
	}
	public void setLOC_SEGMENT5(String lOC_SEGMENT5) {
		LOC_SEGMENT5 = lOC_SEGMENT5;
	}
	public String getLOC_SEGMENT6() {
		return LOC_SEGMENT6;
	}
	public void setLOC_SEGMENT6(String lOC_SEGMENT6) {
		LOC_SEGMENT6 = lOC_SEGMENT6;
	}
	public String getLOC_SEGMENT7() {
		return LOC_SEGMENT7;
	}
	public void setLOC_SEGMENT7(String lOC_SEGMENT7) {
		LOC_SEGMENT7 = lOC_SEGMENT7;
	}
	public String getLOC_SEGMENT8() {
		return LOC_SEGMENT8;
	}
	public void setLOC_SEGMENT8(String lOC_SEGMENT8) {
		LOC_SEGMENT8 = lOC_SEGMENT8;
	}
	public String getLOC_SEGMENT9() {
		return LOC_SEGMENT9;
	}
	public void setLOC_SEGMENT9(String lOC_SEGMENT9) {
		LOC_SEGMENT9 = lOC_SEGMENT9;
	}
	public String getLOC_SEGMENT10() {
		return LOC_SEGMENT10;
	}
	public void setLOC_SEGMENT10(String lOC_SEGMENT10) {
		LOC_SEGMENT10 = lOC_SEGMENT10;
	}
	public String getLOC_SEGMENT11() {
		return LOC_SEGMENT11;
	}
	public void setLOC_SEGMENT11(String lOC_SEGMENT11) {
		LOC_SEGMENT11 = lOC_SEGMENT11;
	}
	public String getLOC_SEGMENT12() {
		return LOC_SEGMENT12;
	}
	public void setLOC_SEGMENT12(String lOC_SEGMENT12) {
		LOC_SEGMENT12 = lOC_SEGMENT12;
	}
	public String getLOC_SEGMENT13() {
		return LOC_SEGMENT13;
	}
	public void setLOC_SEGMENT13(String lOC_SEGMENT13) {
		LOC_SEGMENT13 = lOC_SEGMENT13;
	}
	public String getLOC_SEGMENT14() {
		return LOC_SEGMENT14;
	}
	public void setLOC_SEGMENT14(String lOC_SEGMENT14) {
		LOC_SEGMENT14 = lOC_SEGMENT14;
	}
	public String getLOC_SEGMENT15() {
		return LOC_SEGMENT15;
	}
	public void setLOC_SEGMENT15(String lOC_SEGMENT15) {
		LOC_SEGMENT15 = lOC_SEGMENT15;
	}
	public String getLOC_SEGMENT16() {
		return LOC_SEGMENT16;
	}
	public void setLOC_SEGMENT16(String lOC_SEGMENT16) {
		LOC_SEGMENT16 = lOC_SEGMENT16;
	}
	public String getLOC_SEGMENT17() {
		return LOC_SEGMENT17;
	}
	public void setLOC_SEGMENT17(String lOC_SEGMENT17) {
		LOC_SEGMENT17 = lOC_SEGMENT17;
	}
	public String getLOC_SEGMENT18() {
		return LOC_SEGMENT18;
	}
	public void setLOC_SEGMENT18(String lOC_SEGMENT18) {
		LOC_SEGMENT18 = lOC_SEGMENT18;
	}
	public String getLOC_SEGMENT19() {
		return LOC_SEGMENT19;
	}
	public void setLOC_SEGMENT19(String lOC_SEGMENT19) {
		LOC_SEGMENT19 = lOC_SEGMENT19;
	}
	public String getLOC_SEGMENT20() {
		return LOC_SEGMENT20;
	}
	public void setLOC_SEGMENT20(String lOC_SEGMENT20) {
		LOC_SEGMENT20 = lOC_SEGMENT20;
	}
	public String getTRANSACTION_QUANTITY() {
		return TRANSACTION_QUANTITY;
	}
	public void setTRANSACTION_QUANTITY(String tRANSACTION_QUANTITY) {
		TRANSACTION_QUANTITY = tRANSACTION_QUANTITY;
	}
	public String getTRANSACTION_UOM() {
		return TRANSACTION_UOM;
	}
	public void setTRANSACTION_UOM(String tRANSACTION_UOM) {
		TRANSACTION_UOM = tRANSACTION_UOM;
	}
	public String getTRANSACTION_UNIT_OF_MEASURE() {
		return TRANSACTION_UNIT_OF_MEASURE;
	}
	public void setTRANSACTION_UNIT_OF_MEASURE(String tRANSACTION_UNIT_OF_MEASURE) {
		TRANSACTION_UNIT_OF_MEASURE = tRANSACTION_UNIT_OF_MEASURE;
	}
	public String getRESERVATION_QUANTITY() {
		return RESERVATION_QUANTITY;
	}
	public void setRESERVATION_QUANTITY(String rESERVATION_QUANTITY) {
		RESERVATION_QUANTITY = rESERVATION_QUANTITY;
	}
	public String getTRANSACTION_DATE() {
		return TRANSACTION_DATE;
	}
	public void setTRANSACTION_DATE(String tRANSACTION_DATE) {
		TRANSACTION_DATE = tRANSACTION_DATE;
	}
	public String getTRANSACTION_SOURCE_TYPE_NAME() {
		return TRANSACTION_SOURCE_TYPE_NAME;
	}
	public void setTRANSACTION_SOURCE_TYPE_NAME(String tRANSACTION_SOURCE_TYPE_NAME) {
		TRANSACTION_SOURCE_TYPE_NAME = tRANSACTION_SOURCE_TYPE_NAME;
	}
	public String getTRANSACTION_TYPE_NAME() {
		return TRANSACTION_TYPE_NAME;
	}
	public void setTRANSACTION_TYPE_NAME(String tRANSACTION_TYPE_NAME) {
		TRANSACTION_TYPE_NAME = tRANSACTION_TYPE_NAME;
	}
	public String getTRANSFER_ORGANIZATION_TYPE() {
		return TRANSFER_ORGANIZATION_TYPE;
	}
	public void setTRANSFER_ORGANIZATION_TYPE(String tRANSFER_ORGANIZATION_TYPE) {
		TRANSFER_ORGANIZATION_TYPE = tRANSFER_ORGANIZATION_TYPE;
	}
	public String getTRANSFER_ORGANIZATION_NAME() {
		return TRANSFER_ORGANIZATION_NAME;
	}
	public void setTRANSFER_ORGANIZATION_NAME(String tRANSFER_ORGANIZATION_NAME) {
		TRANSFER_ORGANIZATION_NAME = tRANSFER_ORGANIZATION_NAME;
	}
	public String getTRANSFER_SUBINVENTORY() {
		return TRANSFER_SUBINVENTORY;
	}
	public void setTRANSFER_SUBINVENTORY(String tRANSFER_SUBINVENTORY) {
		TRANSFER_SUBINVENTORY = tRANSFER_SUBINVENTORY;
	}
	public String getXFER_LOC_SEGMENT1() {
		return XFER_LOC_SEGMENT1;
	}
	public void setXFER_LOC_SEGMENT1(String xFER_LOC_SEGMENT1) {
		XFER_LOC_SEGMENT1 = xFER_LOC_SEGMENT1;
	}
	public String getXFER_LOC_SEGMENT2() {
		return XFER_LOC_SEGMENT2;
	}
	public void setXFER_LOC_SEGMENT2(String xFER_LOC_SEGMENT2) {
		XFER_LOC_SEGMENT2 = xFER_LOC_SEGMENT2;
	}
	public String getXFER_LOC_SEGMENT3() {
		return XFER_LOC_SEGMENT3;
	}
	public void setXFER_LOC_SEGMENT3(String xFER_LOC_SEGMENT3) {
		XFER_LOC_SEGMENT3 = xFER_LOC_SEGMENT3;
	}
	public String getXFER_LOC_SEGMENT4() {
		return XFER_LOC_SEGMENT4;
	}
	public void setXFER_LOC_SEGMENT4(String xFER_LOC_SEGMENT4) {
		XFER_LOC_SEGMENT4 = xFER_LOC_SEGMENT4;
	}
	public String getXFER_LOC_SEGMENT5() {
		return XFER_LOC_SEGMENT5;
	}
	public void setXFER_LOC_SEGMENT5(String xFER_LOC_SEGMENT5) {
		XFER_LOC_SEGMENT5 = xFER_LOC_SEGMENT5;
	}
	public String getXFER_LOC_SEGMENT6() {
		return XFER_LOC_SEGMENT6;
	}
	public void setXFER_LOC_SEGMENT6(String xFER_LOC_SEGMENT6) {
		XFER_LOC_SEGMENT6 = xFER_LOC_SEGMENT6;
	}
	public String getXFER_LOC_SEGMENT7() {
		return XFER_LOC_SEGMENT7;
	}
	public void setXFER_LOC_SEGMENT7(String xFER_LOC_SEGMENT7) {
		XFER_LOC_SEGMENT7 = xFER_LOC_SEGMENT7;
	}
	public String getXFER_LOC_SEGMENT8() {
		return XFER_LOC_SEGMENT8;
	}
	public void setXFER_LOC_SEGMENT8(String xFER_LOC_SEGMENT8) {
		XFER_LOC_SEGMENT8 = xFER_LOC_SEGMENT8;
	}
	public String getXFER_LOC_SEGMENT9() {
		return XFER_LOC_SEGMENT9;
	}
	public void setXFER_LOC_SEGMENT9(String xFER_LOC_SEGMENT9) {
		XFER_LOC_SEGMENT9 = xFER_LOC_SEGMENT9;
	}
	public String getXFER_LOC_SEGMENT10() {
		return XFER_LOC_SEGMENT10;
	}
	public void setXFER_LOC_SEGMENT10(String xFER_LOC_SEGMENT10) {
		XFER_LOC_SEGMENT10 = xFER_LOC_SEGMENT10;
	}
	public String getXFER_LOC_SEGMENT11() {
		return XFER_LOC_SEGMENT11;
	}
	public void setXFER_LOC_SEGMENT11(String xFER_LOC_SEGMENT11) {
		XFER_LOC_SEGMENT11 = xFER_LOC_SEGMENT11;
	}
	public String getXFER_LOC_SEGMENT12() {
		return XFER_LOC_SEGMENT12;
	}
	public void setXFER_LOC_SEGMENT12(String xFER_LOC_SEGMENT12) {
		XFER_LOC_SEGMENT12 = xFER_LOC_SEGMENT12;
	}
	public String getXFER_LOC_SEGMENT13() {
		return XFER_LOC_SEGMENT13;
	}
	public void setXFER_LOC_SEGMENT13(String xFER_LOC_SEGMENT13) {
		XFER_LOC_SEGMENT13 = xFER_LOC_SEGMENT13;
	}
	public String getXFER_LOC_SEGMENT14() {
		return XFER_LOC_SEGMENT14;
	}
	public void setXFER_LOC_SEGMENT14(String xFER_LOC_SEGMENT14) {
		XFER_LOC_SEGMENT14 = xFER_LOC_SEGMENT14;
	}
	public String getXFER_LOC_SEGMENT15() {
		return XFER_LOC_SEGMENT15;
	}
	public void setXFER_LOC_SEGMENT15(String xFER_LOC_SEGMENT15) {
		XFER_LOC_SEGMENT15 = xFER_LOC_SEGMENT15;
	}
	public String getXFER_LOC_SEGMENT16() {
		return XFER_LOC_SEGMENT16;
	}
	public void setXFER_LOC_SEGMENT16(String xFER_LOC_SEGMENT16) {
		XFER_LOC_SEGMENT16 = xFER_LOC_SEGMENT16;
	}
	public String getXFER_LOC_SEGMENT17() {
		return XFER_LOC_SEGMENT17;
	}
	public void setXFER_LOC_SEGMENT17(String xFER_LOC_SEGMENT17) {
		XFER_LOC_SEGMENT17 = xFER_LOC_SEGMENT17;
	}
	public String getXFER_LOC_SEGMENT18() {
		return XFER_LOC_SEGMENT18;
	}
	public void setXFER_LOC_SEGMENT18(String xFER_LOC_SEGMENT18) {
		XFER_LOC_SEGMENT18 = xFER_LOC_SEGMENT18;
	}
	public String getXFER_LOC_SEGMENT19() {
		return XFER_LOC_SEGMENT19;
	}
	public void setXFER_LOC_SEGMENT19(String xFER_LOC_SEGMENT19) {
		XFER_LOC_SEGMENT19 = xFER_LOC_SEGMENT19;
	}
	public String getXFER_LOC_SEGMENT20() {
		return XFER_LOC_SEGMENT20;
	}
	public void setXFER_LOC_SEGMENT20(String xFER_LOC_SEGMENT20) {
		XFER_LOC_SEGMENT20 = xFER_LOC_SEGMENT20;
	}
	public String getPRIMARY_QUANTITY() {
		return PRIMARY_QUANTITY;
	}
	public void setPRIMARY_QUANTITY(String pRIMARY_QUANTITY) {
		PRIMARY_QUANTITY = pRIMARY_QUANTITY;
	}
	public String getSECONDARY_TRANSACTION_QUANTITY() {
		return SECONDARY_TRANSACTION_QUANTITY;
	}
	public void setSECONDARY_TRANSACTION_QUANTITY(String sECONDARY_TRANSACTION_QUANTITY) {
		SECONDARY_TRANSACTION_QUANTITY = sECONDARY_TRANSACTION_QUANTITY;
	}
	public String getSECONDARY_UOM_CODE() {
		return SECONDARY_UOM_CODE;
	}
	public void setSECONDARY_UOM_CODE(String sECONDARY_UOM_CODE) {
		SECONDARY_UOM_CODE = sECONDARY_UOM_CODE;
	}
	public String getSECONDARY_UNIT_OF_MEASURE() {
		return SECONDARY_UNIT_OF_MEASURE;
	}
	public void setSECONDARY_UNIT_OF_MEASURE(String sECONDARY_UNIT_OF_MEASURE) {
		SECONDARY_UNIT_OF_MEASURE = sECONDARY_UNIT_OF_MEASURE;
	}
	public String getSOURCE_CODE() {
		return SOURCE_CODE;
	}
	public void setSOURCE_CODE(String sOURCE_CODE) {
		SOURCE_CODE = sOURCE_CODE;
	}
	public String getSOURCE_HEADER_ID() {
		return SOURCE_HEADER_ID;
	}
	public void setSOURCE_HEADER_ID(String sOURCE_HEADER_ID) {
		SOURCE_HEADER_ID = sOURCE_HEADER_ID;
	}
	public String getSOURCE_LINE_ID() {
		return SOURCE_LINE_ID;
	}
	public void setSOURCE_LINE_ID(String sOURCE_LINE_ID) {
		SOURCE_LINE_ID = sOURCE_LINE_ID;
	}
	public String getTRANSACTION_SOURCE_NAME() {
		return TRANSACTION_SOURCE_NAME;
	}
	public void setTRANSACTION_SOURCE_NAME(String tRANSACTION_SOURCE_NAME) {
		TRANSACTION_SOURCE_NAME = tRANSACTION_SOURCE_NAME;
	}
	public String getDSP_SEGMENT1() {
		return DSP_SEGMENT1;
	}
	public void setDSP_SEGMENT1(String dSP_SEGMENT1) {
		DSP_SEGMENT1 = dSP_SEGMENT1;
	}
	public String getDSP_SEGMENT2() {
		return DSP_SEGMENT2;
	}
	public void setDSP_SEGMENT2(String dSP_SEGMENT2) {
		DSP_SEGMENT2 = dSP_SEGMENT2;
	}
	public String getDSP_SEGMENT3() {
		return DSP_SEGMENT3;
	}
	public void setDSP_SEGMENT3(String dSP_SEGMENT3) {
		DSP_SEGMENT3 = dSP_SEGMENT3;
	}
	public String getDSP_SEGMENT4() {
		return DSP_SEGMENT4;
	}
	public void setDSP_SEGMENT4(String dSP_SEGMENT4) {
		DSP_SEGMENT4 = dSP_SEGMENT4;
	}
	public String getDSP_SEGMENT5() {
		return DSP_SEGMENT5;
	}
	public void setDSP_SEGMENT5(String dSP_SEGMENT5) {
		DSP_SEGMENT5 = dSP_SEGMENT5;
	}
	public String getDSP_SEGMENT6() {
		return DSP_SEGMENT6;
	}
	public void setDSP_SEGMENT6(String dSP_SEGMENT6) {
		DSP_SEGMENT6 = dSP_SEGMENT6;
	}
	public String getDSP_SEGMENT7() {
		return DSP_SEGMENT7;
	}
	public void setDSP_SEGMENT7(String dSP_SEGMENT7) {
		DSP_SEGMENT7 = dSP_SEGMENT7;
	}
	public String getDSP_SEGMENT8() {
		return DSP_SEGMENT8;
	}
	public void setDSP_SEGMENT8(String dSP_SEGMENT8) {
		DSP_SEGMENT8 = dSP_SEGMENT8;
	}
	public String getDSP_SEGMENT9() {
		return DSP_SEGMENT9;
	}
	public void setDSP_SEGMENT9(String dSP_SEGMENT9) {
		DSP_SEGMENT9 = dSP_SEGMENT9;
	}
	public String getDSP_SEGMENT10() {
		return DSP_SEGMENT10;
	}
	public void setDSP_SEGMENT10(String dSP_SEGMENT10) {
		DSP_SEGMENT10 = dSP_SEGMENT10;
	}
	public String getDSP_SEGMENT11() {
		return DSP_SEGMENT11;
	}
	public void setDSP_SEGMENT11(String dSP_SEGMENT11) {
		DSP_SEGMENT11 = dSP_SEGMENT11;
	}
	public String getDSP_SEGMENT12() {
		return DSP_SEGMENT12;
	}
	public void setDSP_SEGMENT12(String dSP_SEGMENT12) {
		DSP_SEGMENT12 = dSP_SEGMENT12;
	}
	public String getDSP_SEGMENT13() {
		return DSP_SEGMENT13;
	}
	public void setDSP_SEGMENT13(String dSP_SEGMENT13) {
		DSP_SEGMENT13 = dSP_SEGMENT13;
	}
	public String getDSP_SEGMENT14() {
		return DSP_SEGMENT14;
	}
	public void setDSP_SEGMENT14(String dSP_SEGMENT14) {
		DSP_SEGMENT14 = dSP_SEGMENT14;
	}
	public String getDSP_SEGMENT15() {
		return DSP_SEGMENT15;
	}
	public void setDSP_SEGMENT15(String dSP_SEGMENT15) {
		DSP_SEGMENT15 = dSP_SEGMENT15;
	}
	public String getDSP_SEGMENT16() {
		return DSP_SEGMENT16;
	}
	public void setDSP_SEGMENT16(String dSP_SEGMENT16) {
		DSP_SEGMENT16 = dSP_SEGMENT16;
	}
	public String getDSP_SEGMENT17() {
		return DSP_SEGMENT17;
	}
	public void setDSP_SEGMENT17(String dSP_SEGMENT17) {
		DSP_SEGMENT17 = dSP_SEGMENT17;
	}
	public String getDSP_SEGMENT18() {
		return DSP_SEGMENT18;
	}
	public void setDSP_SEGMENT18(String dSP_SEGMENT18) {
		DSP_SEGMENT18 = dSP_SEGMENT18;
	}
	public String getDSP_SEGMENT19() {
		return DSP_SEGMENT19;
	}
	public void setDSP_SEGMENT19(String dSP_SEGMENT19) {
		DSP_SEGMENT19 = dSP_SEGMENT19;
	}
	public String getDSP_SEGMENT20() {
		return DSP_SEGMENT20;
	}
	public void setDSP_SEGMENT20(String dSP_SEGMENT20) {
		DSP_SEGMENT20 = dSP_SEGMENT20;
	}
	public String getDSP_SEGMENT21() {
		return DSP_SEGMENT21;
	}
	public void setDSP_SEGMENT21(String dSP_SEGMENT21) {
		DSP_SEGMENT21 = dSP_SEGMENT21;
	}
	public String getDSP_SEGMENT22() {
		return DSP_SEGMENT22;
	}
	public void setDSP_SEGMENT22(String dSP_SEGMENT22) {
		DSP_SEGMENT22 = dSP_SEGMENT22;
	}
	public String getDSP_SEGMENT23() {
		return DSP_SEGMENT23;
	}
	public void setDSP_SEGMENT23(String dSP_SEGMENT23) {
		DSP_SEGMENT23 = dSP_SEGMENT23;
	}
	public String getDSP_SEGMENT24() {
		return DSP_SEGMENT24;
	}
	public void setDSP_SEGMENT24(String dSP_SEGMENT24) {
		DSP_SEGMENT24 = dSP_SEGMENT24;
	}
	public String getDSP_SEGMENT25() {
		return DSP_SEGMENT25;
	}
	public void setDSP_SEGMENT25(String dSP_SEGMENT25) {
		DSP_SEGMENT25 = dSP_SEGMENT25;
	}
	public String getDSP_SEGMENT26() {
		return DSP_SEGMENT26;
	}
	public void setDSP_SEGMENT26(String dSP_SEGMENT26) {
		DSP_SEGMENT26 = dSP_SEGMENT26;
	}
	public String getDSP_SEGMENT27() {
		return DSP_SEGMENT27;
	}
	public void setDSP_SEGMENT27(String dSP_SEGMENT27) {
		DSP_SEGMENT27 = dSP_SEGMENT27;
	}
	public String getDSP_SEGMENT28() {
		return DSP_SEGMENT28;
	}
	public void setDSP_SEGMENT28(String dSP_SEGMENT28) {
		DSP_SEGMENT28 = dSP_SEGMENT28;
	}
	public String getDSP_SEGMENT29() {
		return DSP_SEGMENT29;
	}
	public void setDSP_SEGMENT29(String dSP_SEGMENT29) {
		DSP_SEGMENT29 = dSP_SEGMENT29;
	}
	public String getDSP_SEGMENT30() {
		return DSP_SEGMENT30;
	}
	public void setDSP_SEGMENT30(String dSP_SEGMENT30) {
		DSP_SEGMENT30 = dSP_SEGMENT30;
	}
	public String getTRANSACTION_ACTION_NAME() {
		return TRANSACTION_ACTION_NAME;
	}
	public void setTRANSACTION_ACTION_NAME(String tRANSACTION_ACTION_NAME) {
		TRANSACTION_ACTION_NAME = tRANSACTION_ACTION_NAME;
	}
	public String getTRANSACTION_MODE() {
		return TRANSACTION_MODE;
	}
	public void setTRANSACTION_MODE(String tRANSACTION_MODE) {
		TRANSACTION_MODE = tRANSACTION_MODE;
	}
	public String getLOCK_FLAG() {
		return LOCK_FLAG;
	}
	public void setLOCK_FLAG(String lOCK_FLAG) {
		LOCK_FLAG = lOCK_FLAG;
	}
	public String getTRANSACTION_REFERENCE() {
		return TRANSACTION_REFERENCE;
	}
	public void setTRANSACTION_REFERENCE(String tRANSACTION_REFERENCE) {
		TRANSACTION_REFERENCE = tRANSACTION_REFERENCE;
	}
	public String getREASON_NAME() {
		return REASON_NAME;
	}
	public void setREASON_NAME(String rEASON_NAME) {
		REASON_NAME = rEASON_NAME;
	}
	public String getCURRENCY_NAME() {
		return CURRENCY_NAME;
	}
	public void setCURRENCY_NAME(String cURRENCY_NAME) {
		CURRENCY_NAME = cURRENCY_NAME;
	}
	public String getCURRENCY_CODE() {
		return CURRENCY_CODE;
	}
	public void setCURRENCY_CODE(String cURRENCY_CODE) {
		CURRENCY_CODE = cURRENCY_CODE;
	}
	public String getCURRENCY_CONVERSION_TYPE() {
		return CURRENCY_CONVERSION_TYPE;
	}
	public void setCURRENCY_CONVERSION_TYPE(String cURRENCY_CONVERSION_TYPE) {
		CURRENCY_CONVERSION_TYPE = cURRENCY_CONVERSION_TYPE;
	}
	public String getCURRENCY_CONVERSION_RATE() {
		return CURRENCY_CONVERSION_RATE;
	}
	public void setCURRENCY_CONVERSION_RATE(String cURRENCY_CONVERSION_RATE) {
		CURRENCY_CONVERSION_RATE = cURRENCY_CONVERSION_RATE;
	}
	public String getCURRENCY_CONVERSION_DATE() {
		return CURRENCY_CONVERSION_DATE;
	}
	public void setCURRENCY_CONVERSION_DATE(String cURRENCY_CONVERSION_DATE) {
		CURRENCY_CONVERSION_DATE = cURRENCY_CONVERSION_DATE;
	}
	public String getTRANSACTION_COST() {
		return TRANSACTION_COST;
	}
	public void setTRANSACTION_COST(String tRANSACTION_COST) {
		TRANSACTION_COST = tRANSACTION_COST;
	}
	public String getTRANSFER_COST() {
		return TRANSFER_COST;
	}
	public void setTRANSFER_COST(String tRANSFER_COST) {
		TRANSFER_COST = tRANSFER_COST;
	}
	public String getNEW_AVERAGE_COST() {
		return NEW_AVERAGE_COST;
	}
	public void setNEW_AVERAGE_COST(String nEW_AVERAGE_COST) {
		NEW_AVERAGE_COST = nEW_AVERAGE_COST;
	}
	public String getVALUE_CHANGE() {
		return VALUE_CHANGE;
	}
	public void setVALUE_CHANGE(String vALUE_CHANGE) {
		VALUE_CHANGE = vALUE_CHANGE;
	}
	public String getPERCENTAGE_CHANGE() {
		return PERCENTAGE_CHANGE;
	}
	public void setPERCENTAGE_CHANGE(String pERCENTAGE_CHANGE) {
		PERCENTAGE_CHANGE = pERCENTAGE_CHANGE;
	}
	public String getDST_SEGMENT1() {
		return DST_SEGMENT1;
	}
	public void setDST_SEGMENT1(String dST_SEGMENT1) {
		DST_SEGMENT1 = dST_SEGMENT1;
	}
	public String getDST_SEGMENT2() {
		return DST_SEGMENT2;
	}
	public void setDST_SEGMENT2(String dST_SEGMENT2) {
		DST_SEGMENT2 = dST_SEGMENT2;
	}
	public String getDST_SEGMENT3() {
		return DST_SEGMENT3;
	}
	public void setDST_SEGMENT3(String dST_SEGMENT3) {
		DST_SEGMENT3 = dST_SEGMENT3;
	}
	public String getDST_SEGMENT4() {
		return DST_SEGMENT4;
	}
	public void setDST_SEGMENT4(String dST_SEGMENT4) {
		DST_SEGMENT4 = dST_SEGMENT4;
	}
	public String getDST_SEGMENT5() {
		return DST_SEGMENT5;
	}
	public void setDST_SEGMENT5(String dST_SEGMENT5) {
		DST_SEGMENT5 = dST_SEGMENT5;
	}
	public String getDST_SEGMENT6() {
		return DST_SEGMENT6;
	}
	public void setDST_SEGMENT6(String dST_SEGMENT6) {
		DST_SEGMENT6 = dST_SEGMENT6;
	}
	public String getDST_SEGMENT7() {
		return DST_SEGMENT7;
	}
	public void setDST_SEGMENT7(String dST_SEGMENT7) {
		DST_SEGMENT7 = dST_SEGMENT7;
	}
	public String getDST_SEGMENT8() {
		return DST_SEGMENT8;
	}
	public void setDST_SEGMENT8(String dST_SEGMENT8) {
		DST_SEGMENT8 = dST_SEGMENT8;
	}
	public String getDST_SEGMENT9() {
		return DST_SEGMENT9;
	}
	public void setDST_SEGMENT9(String dST_SEGMENT9) {
		DST_SEGMENT9 = dST_SEGMENT9;
	}
	public String getDST_SEGMENT10() {
		return DST_SEGMENT10;
	}
	public void setDST_SEGMENT10(String dST_SEGMENT10) {
		DST_SEGMENT10 = dST_SEGMENT10;
	}
	public String getDST_SEGMENT11() {
		return DST_SEGMENT11;
	}
	public void setDST_SEGMENT11(String dST_SEGMENT11) {
		DST_SEGMENT11 = dST_SEGMENT11;
	}
	public String getDST_SEGMENT12() {
		return DST_SEGMENT12;
	}
	public void setDST_SEGMENT12(String dST_SEGMENT12) {
		DST_SEGMENT12 = dST_SEGMENT12;
	}
	public String getDST_SEGMENT13() {
		return DST_SEGMENT13;
	}
	public void setDST_SEGMENT13(String dST_SEGMENT13) {
		DST_SEGMENT13 = dST_SEGMENT13;
	}
	public String getDST_SEGMENT14() {
		return DST_SEGMENT14;
	}
	public void setDST_SEGMENT14(String dST_SEGMENT14) {
		DST_SEGMENT14 = dST_SEGMENT14;
	}
	public String getDST_SEGMENT15() {
		return DST_SEGMENT15;
	}
	public void setDST_SEGMENT15(String dST_SEGMENT15) {
		DST_SEGMENT15 = dST_SEGMENT15;
	}
	public String getDST_SEGMENT16() {
		return DST_SEGMENT16;
	}
	public void setDST_SEGMENT16(String dST_SEGMENT16) {
		DST_SEGMENT16 = dST_SEGMENT16;
	}
	public String getDST_SEGMENT17() {
		return DST_SEGMENT17;
	}
	public void setDST_SEGMENT17(String dST_SEGMENT17) {
		DST_SEGMENT17 = dST_SEGMENT17;
	}
	public String getDST_SEGMENT18() {
		return DST_SEGMENT18;
	}
	public void setDST_SEGMENT18(String dST_SEGMENT18) {
		DST_SEGMENT18 = dST_SEGMENT18;
	}
	public String getDST_SEGMENT19() {
		return DST_SEGMENT19;
	}
	public void setDST_SEGMENT19(String dST_SEGMENT19) {
		DST_SEGMENT19 = dST_SEGMENT19;
	}
	public String getDST_SEGMENT20() {
		return DST_SEGMENT20;
	}
	public void setDST_SEGMENT20(String dST_SEGMENT20) {
		DST_SEGMENT20 = dST_SEGMENT20;
	}
	public String getDST_SEGMENT21() {
		return DST_SEGMENT21;
	}
	public void setDST_SEGMENT21(String dST_SEGMENT21) {
		DST_SEGMENT21 = dST_SEGMENT21;
	}
	public String getDST_SEGMENT22() {
		return DST_SEGMENT22;
	}
	public void setDST_SEGMENT22(String dST_SEGMENT22) {
		DST_SEGMENT22 = dST_SEGMENT22;
	}
	public String getDST_SEGMENT23() {
		return DST_SEGMENT23;
	}
	public void setDST_SEGMENT23(String dST_SEGMENT23) {
		DST_SEGMENT23 = dST_SEGMENT23;
	}
	public String getDST_SEGMENT24() {
		return DST_SEGMENT24;
	}
	public void setDST_SEGMENT24(String dST_SEGMENT24) {
		DST_SEGMENT24 = dST_SEGMENT24;
	}
	public String getDST_SEGMENT25() {
		return DST_SEGMENT25;
	}
	public void setDST_SEGMENT25(String dST_SEGMENT25) {
		DST_SEGMENT25 = dST_SEGMENT25;
	}
	public String getDST_SEGMENT26() {
		return DST_SEGMENT26;
	}
	public void setDST_SEGMENT26(String dST_SEGMENT26) {
		DST_SEGMENT26 = dST_SEGMENT26;
	}
	public String getDST_SEGMENT27() {
		return DST_SEGMENT27;
	}
	public void setDST_SEGMENT27(String dST_SEGMENT27) {
		DST_SEGMENT27 = dST_SEGMENT27;
	}
	public String getDST_SEGMENT28() {
		return DST_SEGMENT28;
	}
	public void setDST_SEGMENT28(String dST_SEGMENT28) {
		DST_SEGMENT28 = dST_SEGMENT28;
	}
	public String getDST_SEGMENT29() {
		return DST_SEGMENT29;
	}
	public void setDST_SEGMENT29(String dST_SEGMENT29) {
		DST_SEGMENT29 = dST_SEGMENT29;
	}
	public String getDST_SEGMENT30() {
		return DST_SEGMENT30;
	}
	public void setDST_SEGMENT30(String dST_SEGMENT30) {
		DST_SEGMENT30 = dST_SEGMENT30;
	}
	public String getLOCATION_TYPE() {
		return LOCATION_TYPE;
	}
	public void setLOCATION_TYPE(String lOCATION_TYPE) {
		LOCATION_TYPE = lOCATION_TYPE;
	}
	public String getEMPLOYEE_CODE() {
		return EMPLOYEE_CODE;
	}
	public void setEMPLOYEE_CODE(String eMPLOYEE_CODE) {
		EMPLOYEE_CODE = eMPLOYEE_CODE;
	}
	public String getRECEIVING_DOCUMENT() {
		return RECEIVING_DOCUMENT;
	}
	public void setRECEIVING_DOCUMENT(String rECEIVING_DOCUMENT) {
		RECEIVING_DOCUMENT = rECEIVING_DOCUMENT;
	}
	public String getLINE_ITEM_NUM() {
		return LINE_ITEM_NUM;
	}
	public void setLINE_ITEM_NUM(String lINE_ITEM_NUM) {
		LINE_ITEM_NUM = lINE_ITEM_NUM;
	}
	public String getSHIPMENT_NUMBER() {
		return SHIPMENT_NUMBER;
	}
	public void setSHIPMENT_NUMBER(String sHIPMENT_NUMBER) {
		SHIPMENT_NUMBER = sHIPMENT_NUMBER;
	}
	public String getTRANSPORTATION_COST() {
		return TRANSPORTATION_COST;
	}
	public void setTRANSPORTATION_COST(String tRANSPORTATION_COST) {
		TRANSPORTATION_COST = tRANSPORTATION_COST;
	}
	public String getCONTAINERS() {
		return CONTAINERS;
	}
	public void setCONTAINERS(String cONTAINERS) {
		CONTAINERS = cONTAINERS;
	}
	public String getWAYBILL_AIRBILL() {
		return WAYBILL_AIRBILL;
	}
	public void setWAYBILL_AIRBILL(String wAYBILL_AIRBILL) {
		WAYBILL_AIRBILL = wAYBILL_AIRBILL;
	}
	public String getEXPECTED_ARRIVAL_DATE() {
		return EXPECTED_ARRIVAL_DATE;
	}
	public void setEXPECTED_ARRIVAL_DATE(String eXPECTED_ARRIVAL_DATE) {
		EXPECTED_ARRIVAL_DATE = eXPECTED_ARRIVAL_DATE;
	}
	public String getREQUIRED_FLAG() {
		return REQUIRED_FLAG;
	}
	public void setREQUIRED_FLAG(String rEQUIRED_FLAG) {
		REQUIRED_FLAG = rEQUIRED_FLAG;
	}
	public String getSHIPPABLE_FLAG() {
		return SHIPPABLE_FLAG;
	}
	public void setSHIPPABLE_FLAG(String sHIPPABLE_FLAG) {
		SHIPPABLE_FLAG = sHIPPABLE_FLAG;
	}
	public String getSHIPPED_QUANTITY() {
		return SHIPPED_QUANTITY;
	}
	public void setSHIPPED_QUANTITY(String sHIPPED_QUANTITY) {
		SHIPPED_QUANTITY = sHIPPED_QUANTITY;
	}
	public String getVALIDATION_REQUIRED() {
		return VALIDATION_REQUIRED;
	}
	public void setVALIDATION_REQUIRED(String vALIDATION_REQUIRED) {
		VALIDATION_REQUIRED = vALIDATION_REQUIRED;
	}
	public String getNEGATIVE_REQ_FLAG() {
		return NEGATIVE_REQ_FLAG;
	}
	public void setNEGATIVE_REQ_FLAG(String nEGATIVE_REQ_FLAG) {
		NEGATIVE_REQ_FLAG = nEGATIVE_REQ_FLAG;
	}
	public String getOWNING_TP_TYPE() {
		return OWNING_TP_TYPE;
	}
	public void setOWNING_TP_TYPE(String oWNING_TP_TYPE) {
		OWNING_TP_TYPE = oWNING_TP_TYPE;
	}
	public String getTRANSFER_OWNING_TP_TYPE() {
		return TRANSFER_OWNING_TP_TYPE;
	}
	public void setTRANSFER_OWNING_TP_TYPE(String tRANSFER_OWNING_TP_TYPE) {
		TRANSFER_OWNING_TP_TYPE = tRANSFER_OWNING_TP_TYPE;
	}
	public String getOWNING_ORGANIZATION_NAME() {
		return OWNING_ORGANIZATION_NAME;
	}
	public void setOWNING_ORGANIZATION_NAME(String oWNING_ORGANIZATION_NAME) {
		OWNING_ORGANIZATION_NAME = oWNING_ORGANIZATION_NAME;
	}
	public String getXFR_OWNING_ORGANIZATION_NAME() {
		return XFR_OWNING_ORGANIZATION_NAME;
	}
	public void setXFR_OWNING_ORGANIZATION_NAME(String xFR_OWNING_ORGANIZATION_NAME) {
		XFR_OWNING_ORGANIZATION_NAME = xFR_OWNING_ORGANIZATION_NAME;
	}
	public String getTRANSFER_PERCENTAGE() {
		return TRANSFER_PERCENTAGE;
	}
	public void setTRANSFER_PERCENTAGE(String tRANSFER_PERCENTAGE) {
		TRANSFER_PERCENTAGE = tRANSFER_PERCENTAGE;
	}
	public String getPLANNING_TP_TYPE() {
		return PLANNING_TP_TYPE;
	}
	public void setPLANNING_TP_TYPE(String pLANNING_TP_TYPE) {
		PLANNING_TP_TYPE = pLANNING_TP_TYPE;
	}
	public String getTRANSFER_PLANNING_TP_TYPE() {
		return TRANSFER_PLANNING_TP_TYPE;
	}
	public void setTRANSFER_PLANNING_TP_TYPE(String tRANSFER_PLANNING_TP_TYPE) {
		TRANSFER_PLANNING_TP_TYPE = tRANSFER_PLANNING_TP_TYPE;
	}
	public String getROUTING_REVISION() {
		return ROUTING_REVISION;
	}
	public void setROUTING_REVISION(String rOUTING_REVISION) {
		ROUTING_REVISION = rOUTING_REVISION;
	}
	public String getROUTING_REVISION_DATE() {
		return ROUTING_REVISION_DATE;
	}
	public void setROUTING_REVISION_DATE(String rOUTING_REVISION_DATE) {
		ROUTING_REVISION_DATE = rOUTING_REVISION_DATE;
	}
	public String getALTERNATE_BOM_DESIGNATOR() {
		return ALTERNATE_BOM_DESIGNATOR;
	}
	public void setALTERNATE_BOM_DESIGNATOR(String aLTERNATE_BOM_DESIGNATOR) {
		ALTERNATE_BOM_DESIGNATOR = aLTERNATE_BOM_DESIGNATOR;
	}
	public String getALTERNATE_ROUTING_DESIGNATOR() {
		return ALTERNATE_ROUTING_DESIGNATOR;
	}
	public void setALTERNATE_ROUTING_DESIGNATOR(String aLTERNATE_ROUTING_DESIGNATOR) {
		ALTERNATE_ROUTING_DESIGNATOR = aLTERNATE_ROUTING_DESIGNATOR;
	}
	public String getORGANIZATION_TYPE() {
		return ORGANIZATION_TYPE;
	}
	public void setORGANIZATION_TYPE(String oRGANIZATION_TYPE) {
		ORGANIZATION_TYPE = oRGANIZATION_TYPE;
	}
	public String getUSSGL_TRANSACTION_CODE() {
		return USSGL_TRANSACTION_CODE;
	}
	public void setUSSGL_TRANSACTION_CODE(String uSSGL_TRANSACTION_CODE) {
		USSGL_TRANSACTION_CODE = uSSGL_TRANSACTION_CODE;
	}
	public String getWIP_ENTITY_TYPE() {
		return WIP_ENTITY_TYPE;
	}
	public void setWIP_ENTITY_TYPE(String wIP_ENTITY_TYPE) {
		WIP_ENTITY_TYPE = wIP_ENTITY_TYPE;
	}
	public String getSCHEDULE_UPDATE_CODE() {
		return SCHEDULE_UPDATE_CODE;
	}
	public void setSCHEDULE_UPDATE_CODE(String sCHEDULE_UPDATE_CODE) {
		SCHEDULE_UPDATE_CODE = sCHEDULE_UPDATE_CODE;
	}
	public String getSETUP_TEARDOWN_CODE() {
		return SETUP_TEARDOWN_CODE;
	}
	public void setSETUP_TEARDOWN_CODE(String sETUP_TEARDOWN_CODE) {
		SETUP_TEARDOWN_CODE = sETUP_TEARDOWN_CODE;
	}
	public String getPRIMARY_SWITCH() {
		return PRIMARY_SWITCH;
	}
	public void setPRIMARY_SWITCH(String pRIMARY_SWITCH) {
		PRIMARY_SWITCH = pRIMARY_SWITCH;
	}
	public String getMRP_CODE() {
		return MRP_CODE;
	}
	public void setMRP_CODE(String mRP_CODE) {
		MRP_CODE = mRP_CODE;
	}
	public String getOPERATION_SEQ_NUM() {
		return OPERATION_SEQ_NUM;
	}
	public void setOPERATION_SEQ_NUM(String oPERATION_SEQ_NUM) {
		OPERATION_SEQ_NUM = oPERATION_SEQ_NUM;
	}
	public String getWIP_SUPPLY_TYPE() {
		return WIP_SUPPLY_TYPE;
	}
	public void setWIP_SUPPLY_TYPE(String wIP_SUPPLY_TYPE) {
		WIP_SUPPLY_TYPE = wIP_SUPPLY_TYPE;
	}
	public String getRELIEVE_RESERVATIONS_FLAG() {
		return RELIEVE_RESERVATIONS_FLAG;
	}
	public void setRELIEVE_RESERVATIONS_FLAG(String rELIEVE_RESERVATIONS_FLAG) {
		RELIEVE_RESERVATIONS_FLAG = rELIEVE_RESERVATIONS_FLAG;
	}
	public String getRELIEVE_HIGH_LEVEL_RSV_FLAG() {
		return RELIEVE_HIGH_LEVEL_RSV_FLAG;
	}
	public void setRELIEVE_HIGH_LEVEL_RSV_FLAG(String rELIEVE_HIGH_LEVEL_RSV_FLAG) {
		RELIEVE_HIGH_LEVEL_RSV_FLAG = rELIEVE_HIGH_LEVEL_RSV_FLAG;
	}
	public String getTRANSFER_PRICE() {
		return TRANSFER_PRICE;
	}
	public void setTRANSFER_PRICE(String tRANSFER_PRICE) {
		TRANSFER_PRICE = tRANSFER_PRICE;
	}
	public String getBUILD_BREAK_TO_UOM() {
		return BUILD_BREAK_TO_UOM;
	}
	public void setBUILD_BREAK_TO_UOM(String bUILD_BREAK_TO_UOM) {
		BUILD_BREAK_TO_UOM = bUILD_BREAK_TO_UOM;
	}
	public String getBUILD_BREAK_TO_UNIT_OF_MEASURE() {
		return BUILD_BREAK_TO_UNIT_OF_MEASURE;
	}
	public void setBUILD_BREAK_TO_UNIT_OF_MEASURE(String bUILD_BREAK_TO_UNIT_OF_MEASURE) {
		BUILD_BREAK_TO_UNIT_OF_MEASURE = bUILD_BREAK_TO_UNIT_OF_MEASURE;
	}
	public String getATTRIBUTE_CATEGORY() {
		return ATTRIBUTE_CATEGORY;
	}
	public void setATTRIBUTE_CATEGORY(String aTTRIBUTE_CATEGORY) {
		ATTRIBUTE_CATEGORY = aTTRIBUTE_CATEGORY;
	}
	public String getATTRIBUTE1() {
		return ATTRIBUTE1;
	}
	public void setATTRIBUTE1(String aTTRIBUTE1) {
		ATTRIBUTE1 = aTTRIBUTE1;
	}
	public String getATTRIBUTE2() {
		return ATTRIBUTE2;
	}
	public void setATTRIBUTE2(String aTTRIBUTE2) {
		ATTRIBUTE2 = aTTRIBUTE2;
	}
	public String getATTRIBUTE3() {
		return ATTRIBUTE3;
	}
	public void setATTRIBUTE3(String aTTRIBUTE3) {
		ATTRIBUTE3 = aTTRIBUTE3;
	}
	public String getATTRIBUTE4() {
		return ATTRIBUTE4;
	}
	public void setATTRIBUTE4(String aTTRIBUTE4) {
		ATTRIBUTE4 = aTTRIBUTE4;
	}
	public String getATTRIBUTE5() {
		return ATTRIBUTE5;
	}
	public void setATTRIBUTE5(String aTTRIBUTE5) {
		ATTRIBUTE5 = aTTRIBUTE5;
	}
	public String getATTRIBUTE6() {
		return ATTRIBUTE6;
	}
	public void setATTRIBUTE6(String aTTRIBUTE6) {
		ATTRIBUTE6 = aTTRIBUTE6;
	}
	public String getATTRIBUTE7() {
		return ATTRIBUTE7;
	}
	public void setATTRIBUTE7(String aTTRIBUTE7) {
		ATTRIBUTE7 = aTTRIBUTE7;
	}
	public String getATTRIBUTE8() {
		return ATTRIBUTE8;
	}
	public void setATTRIBUTE8(String aTTRIBUTE8) {
		ATTRIBUTE8 = aTTRIBUTE8;
	}
	public String getATTRIBUTE9() {
		return ATTRIBUTE9;
	}
	public void setATTRIBUTE9(String aTTRIBUTE9) {
		ATTRIBUTE9 = aTTRIBUTE9;
	}
	public String getATTRIBUTE10() {
		return ATTRIBUTE10;
	}
	public void setATTRIBUTE10(String aTTRIBUTE10) {
		ATTRIBUTE10 = aTTRIBUTE10;
	}
	public String getATTRIBUTE11() {
		return ATTRIBUTE11;
	}
	public void setATTRIBUTE11(String aTTRIBUTE11) {
		ATTRIBUTE11 = aTTRIBUTE11;
	}
	public String getATTRIBUTE12() {
		return ATTRIBUTE12;
	}
	public void setATTRIBUTE12(String aTTRIBUTE12) {
		ATTRIBUTE12 = aTTRIBUTE12;
	}
	public String getATTRIBUTE13() {
		return ATTRIBUTE13;
	}
	public void setATTRIBUTE13(String aTTRIBUTE13) {
		ATTRIBUTE13 = aTTRIBUTE13;
	}
	public String getATTRIBUTE14() {
		return ATTRIBUTE14;
	}
	public void setATTRIBUTE14(String aTTRIBUTE14) {
		ATTRIBUTE14 = aTTRIBUTE14;
	}
	public String getATTRIBUTE15() {
		return ATTRIBUTE15;
	}
	public void setATTRIBUTE15(String aTTRIBUTE15) {
		ATTRIBUTE15 = aTTRIBUTE15;
	}
	public String getATTRIBUTE16() {
		return ATTRIBUTE16;
	}
	public void setATTRIBUTE16(String aTTRIBUTE16) {
		ATTRIBUTE16 = aTTRIBUTE16;
	}
	public String getATTRIBUTE17() {
		return ATTRIBUTE17;
	}
	public void setATTRIBUTE17(String aTTRIBUTE17) {
		ATTRIBUTE17 = aTTRIBUTE17;
	}
	public String getATTRIBUTE18() {
		return ATTRIBUTE18;
	}
	public void setATTRIBUTE18(String aTTRIBUTE18) {
		ATTRIBUTE18 = aTTRIBUTE18;
	}
	public String getATTRIBUTE19() {
		return ATTRIBUTE19;
	}
	public void setATTRIBUTE19(String aTTRIBUTE19) {
		ATTRIBUTE19 = aTTRIBUTE19;
	}
	public String getATTRIBUTE20() {
		return ATTRIBUTE20;
	}
	public void setATTRIBUTE20(String aTTRIBUTE20) {
		ATTRIBUTE20 = aTTRIBUTE20;
	}
	public String getATTRIBUTE_NUMBER1() {
		return ATTRIBUTE_NUMBER1;
	}
	public void setATTRIBUTE_NUMBER1(String aTTRIBUTE_NUMBER1) {
		ATTRIBUTE_NUMBER1 = aTTRIBUTE_NUMBER1;
	}
	public String getATTRIBUTE_NUMBER2() {
		return ATTRIBUTE_NUMBER2;
	}
	public void setATTRIBUTE_NUMBER2(String aTTRIBUTE_NUMBER2) {
		ATTRIBUTE_NUMBER2 = aTTRIBUTE_NUMBER2;
	}
	public String getATTRIBUTE_NUMBER3() {
		return ATTRIBUTE_NUMBER3;
	}
	public void setATTRIBUTE_NUMBER3(String aTTRIBUTE_NUMBER3) {
		ATTRIBUTE_NUMBER3 = aTTRIBUTE_NUMBER3;
	}
	public String getATTRIBUTE_NUMBER4() {
		return ATTRIBUTE_NUMBER4;
	}
	public void setATTRIBUTE_NUMBER4(String aTTRIBUTE_NUMBER4) {
		ATTRIBUTE_NUMBER4 = aTTRIBUTE_NUMBER4;
	}
	public String getATTRIBUTE_NUMBER5() {
		return ATTRIBUTE_NUMBER5;
	}
	public void setATTRIBUTE_NUMBER5(String aTTRIBUTE_NUMBER5) {
		ATTRIBUTE_NUMBER5 = aTTRIBUTE_NUMBER5;
	}
	public String getATTRIBUTE_NUMBER6() {
		return ATTRIBUTE_NUMBER6;
	}
	public void setATTRIBUTE_NUMBER6(String aTTRIBUTE_NUMBER6) {
		ATTRIBUTE_NUMBER6 = aTTRIBUTE_NUMBER6;
	}
	public String getATTRIBUTE_NUMBER7() {
		return ATTRIBUTE_NUMBER7;
	}
	public void setATTRIBUTE_NUMBER7(String aTTRIBUTE_NUMBER7) {
		ATTRIBUTE_NUMBER7 = aTTRIBUTE_NUMBER7;
	}
	public String getATTRIBUTE_NUMBER8() {
		return ATTRIBUTE_NUMBER8;
	}
	public void setATTRIBUTE_NUMBER8(String aTTRIBUTE_NUMBER8) {
		ATTRIBUTE_NUMBER8 = aTTRIBUTE_NUMBER8;
	}
	public String getATTRIBUTE_NUMBER9() {
		return ATTRIBUTE_NUMBER9;
	}
	public void setATTRIBUTE_NUMBER9(String aTTRIBUTE_NUMBER9) {
		ATTRIBUTE_NUMBER9 = aTTRIBUTE_NUMBER9;
	}
	public String getATTRIBUTE_NUMBER10() {
		return ATTRIBUTE_NUMBER10;
	}
	public void setATTRIBUTE_NUMBER10(String aTTRIBUTE_NUMBER10) {
		ATTRIBUTE_NUMBER10 = aTTRIBUTE_NUMBER10;
	}
	public String getATTRIBUTE_DATE1() {
		return ATTRIBUTE_DATE1;
	}
	public void setATTRIBUTE_DATE1(String aTTRIBUTE_DATE1) {
		ATTRIBUTE_DATE1 = aTTRIBUTE_DATE1;
	}
	public String getATTRIBUTE_DATE2() {
		return ATTRIBUTE_DATE2;
	}
	public void setATTRIBUTE_DATE2(String aTTRIBUTE_DATE2) {
		ATTRIBUTE_DATE2 = aTTRIBUTE_DATE2;
	}
	public String getATTRIBUTE_DATE3() {
		return ATTRIBUTE_DATE3;
	}
	public void setATTRIBUTE_DATE3(String aTTRIBUTE_DATE3) {
		ATTRIBUTE_DATE3 = aTTRIBUTE_DATE3;
	}
	public String getATTRIBUTE_DATE4() {
		return ATTRIBUTE_DATE4;
	}
	public void setATTRIBUTE_DATE4(String aTTRIBUTE_DATE4) {
		ATTRIBUTE_DATE4 = aTTRIBUTE_DATE4;
	}
	public String getATTRIBUTE_DATE5() {
		return ATTRIBUTE_DATE5;
	}
	public void setATTRIBUTE_DATE5(String aTTRIBUTE_DATE5) {
		ATTRIBUTE_DATE5 = aTTRIBUTE_DATE5;
	}
	public String getATTRIBUTE_TIMESTAMP1() {
		return ATTRIBUTE_TIMESTAMP1;
	}
	public void setATTRIBUTE_TIMESTAMP1(String aTTRIBUTE_TIMESTAMP1) {
		ATTRIBUTE_TIMESTAMP1 = aTTRIBUTE_TIMESTAMP1;
	}
	public String getATTRIBUTE_TIMESTAMP2() {
		return ATTRIBUTE_TIMESTAMP2;
	}
	public void setATTRIBUTE_TIMESTAMP2(String aTTRIBUTE_TIMESTAMP2) {
		ATTRIBUTE_TIMESTAMP2 = aTTRIBUTE_TIMESTAMP2;
	}
	public String getATTRIBUTE_TIMESTAMP3() {
		return ATTRIBUTE_TIMESTAMP3;
	}
	public void setATTRIBUTE_TIMESTAMP3(String aTTRIBUTE_TIMESTAMP3) {
		ATTRIBUTE_TIMESTAMP3 = aTTRIBUTE_TIMESTAMP3;
	}
	public String getATTRIBUTE_TIMESTAMP4() {
		return ATTRIBUTE_TIMESTAMP4;
	}
	public void setATTRIBUTE_TIMESTAMP4(String aTTRIBUTE_TIMESTAMP4) {
		ATTRIBUTE_TIMESTAMP4 = aTTRIBUTE_TIMESTAMP4;
	}
	public String getATTRIBUTE_TIMESTAMP5() {
		return ATTRIBUTE_TIMESTAMP5;
	}
	public void setATTRIBUTE_TIMESTAMP5(String aTTRIBUTE_TIMESTAMP5) {
		ATTRIBUTE_TIMESTAMP5 = aTTRIBUTE_TIMESTAMP5;
	}
	public String getTRANSACTION_COST_IDENTIFIER() {
		return TRANSACTION_COST_IDENTIFIER;
	}
	public void setTRANSACTION_COST_IDENTIFIER(String tRANSACTION_COST_IDENTIFIER) {
		TRANSACTION_COST_IDENTIFIER = tRANSACTION_COST_IDENTIFIER;
	}
	public String getDEFAULT_TAXATION_COUNTRY() {
		return DEFAULT_TAXATION_COUNTRY;
	}
	public void setDEFAULT_TAXATION_COUNTRY(String dEFAULT_TAXATION_COUNTRY) {
		DEFAULT_TAXATION_COUNTRY = dEFAULT_TAXATION_COUNTRY;
	}
	public String getDOCUMENT_SUB_TYPE() {
		return DOCUMENT_SUB_TYPE;
	}
	public void setDOCUMENT_SUB_TYPE(String dOCUMENT_SUB_TYPE) {
		DOCUMENT_SUB_TYPE = dOCUMENT_SUB_TYPE;
	}
	public String getTRX_BUSINESS_CATEGORY() {
		return TRX_BUSINESS_CATEGORY;
	}
	public void setTRX_BUSINESS_CATEGORY(String tRX_BUSINESS_CATEGORY) {
		TRX_BUSINESS_CATEGORY = tRX_BUSINESS_CATEGORY;
	}
	public String getUSER_DEFINED_FISC_CLASS() {
		return USER_DEFINED_FISC_CLASS;
	}
	public void setUSER_DEFINED_FISC_CLASS(String uSER_DEFINED_FISC_CLASS) {
		USER_DEFINED_FISC_CLASS = uSER_DEFINED_FISC_CLASS;
	}
	public String getTAX_INVOICE_NUMBER() {
		return TAX_INVOICE_NUMBER;
	}
	public void setTAX_INVOICE_NUMBER(String tAX_INVOICE_NUMBER) {
		TAX_INVOICE_NUMBER = tAX_INVOICE_NUMBER;
	}
	public String getTAX_INVOICE_DATE() {
		return TAX_INVOICE_DATE;
	}
	public void setTAX_INVOICE_DATE(String tAX_INVOICE_DATE) {
		TAX_INVOICE_DATE = tAX_INVOICE_DATE;
	}
	public String getPRODUCT_CATEGORY() {
		return PRODUCT_CATEGORY;
	}
	public void setPRODUCT_CATEGORY(String pRODUCT_CATEGORY) {
		PRODUCT_CATEGORY = pRODUCT_CATEGORY;
	}
	public String getPRODUCT_TYPE() {
		return PRODUCT_TYPE;
	}
	public void setPRODUCT_TYPE(String pRODUCT_TYPE) {
		PRODUCT_TYPE = pRODUCT_TYPE;
	}
	public String getASSESSABLE_VALUE() {
		return ASSESSABLE_VALUE;
	}
	public void setASSESSABLE_VALUE(String aSSESSABLE_VALUE) {
		ASSESSABLE_VALUE = aSSESSABLE_VALUE;
	}
	public String getTAX_CLASSIFICATION_CODE() {
		return TAX_CLASSIFICATION_CODE;
	}
	public void setTAX_CLASSIFICATION_CODE(String tAX_CLASSIFICATION_CODE) {
		TAX_CLASSIFICATION_CODE = tAX_CLASSIFICATION_CODE;
	}
	public String getEXEMPT_CERTIFICATE_NUMBER() {
		return EXEMPT_CERTIFICATE_NUMBER;
	}
	public void setEXEMPT_CERTIFICATE_NUMBER(String eXEMPT_CERTIFICATE_NUMBER) {
		EXEMPT_CERTIFICATE_NUMBER = eXEMPT_CERTIFICATE_NUMBER;
	}
	public String getEXEMPT_REASON_CODE() {
		return EXEMPT_REASON_CODE;
	}
	public void setEXEMPT_REASON_CODE(String eXEMPT_REASON_CODE) {
		EXEMPT_REASON_CODE = eXEMPT_REASON_CODE;
	}
	public String getINTENDED_USE() {
		return INTENDED_USE;
	}
	public void setINTENDED_USE(String iNTENDED_USE) {
		INTENDED_USE = iNTENDED_USE;
	}
	public String getFIRST_PTY_NUMBER() {
		return FIRST_PTY_NUMBER;
	}
	public void setFIRST_PTY_NUMBER(String fIRST_PTY_NUMBER) {
		FIRST_PTY_NUMBER = fIRST_PTY_NUMBER;
	}
	public String getTHIRD_PTY_NUMBER() {
		return THIRD_PTY_NUMBER;
	}
	public void setTHIRD_PTY_NUMBER(String tHIRD_PTY_NUMBER) {
		THIRD_PTY_NUMBER = tHIRD_PTY_NUMBER;
	}
	public String getFINAL_DISCHARGE_LOC_CODE() {
		return FINAL_DISCHARGE_LOC_CODE;
	}
	public void setFINAL_DISCHARGE_LOC_CODE(String fINAL_DISCHARGE_LOC_CODE) {
		FINAL_DISCHARGE_LOC_CODE = fINAL_DISCHARGE_LOC_CODE;
	}
	public String getCATEGORY_NAME() {
		return CATEGORY_NAME;
	}
	public void setCATEGORY_NAME(String cATEGORY_NAME) {
		CATEGORY_NAME = cATEGORY_NAME;
	}
	public String getOWNING_ORGANIZATION_ID() {
		return OWNING_ORGANIZATION_ID;
	}
	public void setOWNING_ORGANIZATION_ID(String oWNING_ORGANIZATION_ID) {
		OWNING_ORGANIZATION_ID = oWNING_ORGANIZATION_ID;
	}
	public String getXFR_OWNING_ORGANIZATION_ID() {
		return XFR_OWNING_ORGANIZATION_ID;
	}
	public void setXFR_OWNING_ORGANIZATION_ID(String xFR_OWNING_ORGANIZATION_ID) {
		XFR_OWNING_ORGANIZATION_ID = xFR_OWNING_ORGANIZATION_ID;
	}
	public String getPRC_BU_NAME() {
		return PRC_BU_NAME;
	}
	public void setPRC_BU_NAME(String pRC_BU_NAME) {
		PRC_BU_NAME = pRC_BU_NAME;
	}
	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}
	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}
	public String getVENDOR_NUMBER() {
		return VENDOR_NUMBER;
	}
	public void setVENDOR_NUMBER(String vENDOR_NUMBER) {
		VENDOR_NUMBER = vENDOR_NUMBER;
	}
	public String getCONSIGNMENT_AGREEMENT_NUM() {
		return CONSIGNMENT_AGREEMENT_NUM;
	}
	public void setCONSIGNMENT_AGREEMENT_NUM(String cONSIGNMENT_AGREEMENT_NUM) {
		CONSIGNMENT_AGREEMENT_NUM = cONSIGNMENT_AGREEMENT_NUM;
	}
	public String getUSE_CURRENT_COST() {
		return USE_CURRENT_COST;
	}
	public void setUSE_CURRENT_COST(String uSE_CURRENT_COST) {
		USE_CURRENT_COST = uSE_CURRENT_COST;
	}
	public String getEXTERNAL_SYSTEM_PACKING_UNIT() {
		return EXTERNAL_SYSTEM_PACKING_UNIT;
	}
	public void setEXTERNAL_SYSTEM_PACKING_UNIT(String eXTERNAL_SYSTEM_PACKING_UNIT) {
		EXTERNAL_SYSTEM_PACKING_UNIT = eXTERNAL_SYSTEM_PACKING_UNIT;
	}
	public String getTRANSFER_LOCATOR_NAME() {
		return TRANSFER_LOCATOR_NAME;
	}
	public void setTRANSFER_LOCATOR_NAME(String tRANSFER_LOCATOR_NAME) {
		TRANSFER_LOCATOR_NAME = tRANSFER_LOCATOR_NAME;
	}
}

package com.tdo.integration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class InventoryTransactionV1 {
	
	@JsonProperty("inventoryTransactionHeader")
	List<InventoryTransactionHeader> inventoryTransactionHeader;
	@JsonProperty("inventoryTransactionLots")
	List<InventoryTransactionLots> inventoryTransactionLots;
	@JsonProperty("inventoryTransactionCost")
	List<InventoryTransactionCost> inventoryTransactionCost;

	public List<InventoryTransactionHeader> getInventoryTransactionHeader() {
		return inventoryTransactionHeader;
	}
	public void setInventoryTransactionHeader(List<InventoryTransactionHeader> inventoryTransactionHeader) {
		this.inventoryTransactionHeader = inventoryTransactionHeader;
	}
	public List<InventoryTransactionLots> getInventoryTransactionLots() {
		return inventoryTransactionLots;
	}
	public void setInventoryTransactionLots(List<InventoryTransactionLots> inventoryTransactionLots) {
		this.inventoryTransactionLots = inventoryTransactionLots;
	}
	public List<InventoryTransactionCost> getInventoryTransactionCost() {
		return inventoryTransactionCost;
	}
	public void setInventoryTransactionCost(List<InventoryTransactionCost> inventoryTransactionCost) {
		this.inventoryTransactionCost = inventoryTransactionCost;
	}

}

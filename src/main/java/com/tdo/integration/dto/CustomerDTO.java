package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerDTO {
	
	@JsonProperty("address1")
	private String address1;
	
	@JsonProperty("country")
	private String country;
	
	@JsonProperty("city")
	private String city;
	
	@JsonProperty("postalCode")
	private String postalCode;
	
	@JsonProperty("state")
	private String state;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("rfc")
	private String rfc;
	
	@JsonProperty("site")
	private String site;
	
	@JsonProperty("account")
	private Integer account;
	
	@JsonProperty("msg")
	private String msg;
	
	@JsonProperty("partyNumber")
	private String partyNumber;
	
	@JsonProperty("siteNumber")
	private String siteNumber;
	
	@JsonProperty("accountNumber")
	private String accountNumber;
	
	@JsonProperty("profileClassName")
	private String profileClassName;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getPartyNumber() {
		return partyNumber;
	}

	public void setPartyNumber(String partyNumber) {
		this.partyNumber = partyNumber;
	}

	public String getSiteNumber() {
		return siteNumber;
	}

	public void setSiteNumber(String siteNumber) {
		this.siteNumber = siteNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getProfileClassName() {
		return profileClassName;
	}

	public void setProfileClassName(String profileClassName) {
		this.profileClassName = profileClassName;
	}
	
	
	
}

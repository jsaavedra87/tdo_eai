package com.tdo.integration.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InventoryTransactionDTO {

	@JsonProperty("itemId")
	private int itemId;
	
	@JsonProperty("itemName")
	private String itemName;
	
	@JsonProperty("itemDescription")
	private String itemDescription;
	
	@JsonProperty("uom")
	private String uom;
	
	@JsonProperty("inventoryOrganizationCode")
	private String inventoryOrganizationCode;
	
	@JsonProperty("inventoryOrganizationName")
	private String inventoryOrganizationName;
	
	@JsonProperty("transactionDate")
	private XMLGregorianCalendar transactionDate;
	
	@JsonProperty("inventoryLocation")
	private String inventoryLocation;
	
	@JsonProperty("lotNumber")
	private String lotNumber;
	
	@JsonProperty("creationDate")
	private XMLGregorianCalendar creationDate;
	
	@JsonProperty("expirationDate")
	private XMLGregorianCalendar expirationDate;
	
	@JsonProperty("subInventory")
	private String subInventory;
	
	@JsonProperty("transactionType")
	private String transactionType;
	
	@JsonProperty("quantity")
	private double quantity;
	
	@JsonProperty("averageCost")
	private double averageCost;
	
	@JsonProperty("itemType")
	private String itemType;
	
	@JsonProperty("transactionId")
	private String transactionId;
	
	@JsonProperty("transactionRealDate")
	private XMLGregorianCalendar transactionRealDate;	

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getInventoryOrganizationCode() {
		return inventoryOrganizationCode;
	}

	public void setInventoryOrganizationCode(String inventoryOrganizationCode) {
		this.inventoryOrganizationCode = inventoryOrganizationCode;
	}

	public String getInventoryOrganizationName() {
		return inventoryOrganizationName;
	}

	public void setInventoryOrganizationName(String inventoryOrganizationName) {
		this.inventoryOrganizationName = inventoryOrganizationName;
	}

	public XMLGregorianCalendar getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(XMLGregorianCalendar transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getInventoryLocation() {
		return inventoryLocation;
	}

	public void setInventoryLocation(String inventoryLocation) {
		this.inventoryLocation = inventoryLocation;
	}

	public String getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	public XMLGregorianCalendar getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(XMLGregorianCalendar creationDate) {
		this.creationDate = creationDate;
	}

	public XMLGregorianCalendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(XMLGregorianCalendar expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getSubInventory() {
		return subInventory;
	}

	public void setSubInventory(String subInventory) {
		this.subInventory = subInventory;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getAverageCost() {
		return averageCost;
	}

	public void setAverageCost(double averageCost) {
		this.averageCost = averageCost;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public XMLGregorianCalendar getTransactionRealDate() {
		return transactionRealDate;
	}

	public void setTransactionRealDate(XMLGregorianCalendar transactionRealDate) {
		this.transactionRealDate = transactionRealDate;
	}

	
}

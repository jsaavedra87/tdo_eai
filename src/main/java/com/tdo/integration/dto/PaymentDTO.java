package com.tdo.integration.dto;

import java.util.List;

import com.tdo.integration.data.model.tdo.Abono;

public class PaymentDTO {

	private List<Abono> lstPayment;

	public List<Abono> getLstPayment() {
		return lstPayment;
	}

	public void setLstPayment(List<Abono> lstPayment) {
		this.lstPayment = lstPayment;
	}

}

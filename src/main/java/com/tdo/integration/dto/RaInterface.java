package com.tdo.integration.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.tdo.integration.data.model.tdo.FileTransferControl;

@JsonAutoDetect
public class RaInterface {
	private String businessUnitId; 
	
	List<RaInterfaceLinesAll> raInterfaceLinesAll;
	List<RaInterfaceDistributionsAll> raInterfaceDistributionsAll;
	private FileTransferControl fileTransferControl;
	
	public String getBusinessUnitId() {
		return businessUnitId;
	}
	public void setBusinessUnitId(String businessUnitId) {
		this.businessUnitId = businessUnitId;
	}
	
	public List<RaInterfaceLinesAll> getRaInterfaceLinesAll() {
		return raInterfaceLinesAll;
	}
	public void setRaInterfaceLinesAll(List<RaInterfaceLinesAll> raInterfaceLinesAll) {
		this.raInterfaceLinesAll = raInterfaceLinesAll;
	}
	public List<RaInterfaceDistributionsAll> getRaInterfaceDistributionsAll() {
		return raInterfaceDistributionsAll;
	}
	public void setRaInterfaceDistributionsAll(List<RaInterfaceDistributionsAll> raInterfaceDistributionsAll) {
		this.raInterfaceDistributionsAll = raInterfaceDistributionsAll;
	}
	public FileTransferControl getFileTransferControl() {
		return fileTransferControl;
	}
	public void setFileTransferControl(FileTransferControl fileTransferControl) {
		this.fileTransferControl = fileTransferControl;
	}

}

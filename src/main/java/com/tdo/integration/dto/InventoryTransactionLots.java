package com.tdo.integration.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class InventoryTransactionLots {
	
	@JsonProperty("LOT_INTERFACE_NBR")
	private String LOT_INTERFACE_NBR;
	@JsonProperty("INV_SERIAL_NBR")
	private String INV_SERIAL_NBR;
	@JsonProperty("SOURCE_CODE")
	private String SOURCE_CODE;
	@JsonProperty("SOURCE_LINE_ID")
	private String SOURCE_LINE_ID;
	@JsonProperty("LOT_NUMBER")
	private String LOT_NUMBER;
	@JsonProperty("DESCRIPTION")
	private String DESCRIPTION;
	@JsonProperty("LOT_EXPIRATION_DATE")
	private String LOT_EXPIRATION_DATE;
	@JsonProperty("TRANSACTION_QUANTITY")
	private String TRANSACTION_QUANTITY;
	@JsonProperty("PRIMARY_QUANTITY")
	private String PRIMARY_QUANTITY;
	@JsonProperty("ORIGINATION_TYPE")
	private String ORIGINATION_TYPE;
	@JsonProperty("ORIGINATION_DATE")
	private String ORIGINATION_DATE;
	@JsonProperty("STATUS_CODE")
	private String STATUS_CODE;
	@JsonProperty("RETEST_DATE")
	private String RETEST_DATE;
	@JsonProperty("EXPIRATION_ACTION_NAME")
	private String EXPIRATION_ACTION_NAME;
	@JsonProperty("EXPIRATION_ACTION_CODE")
	private String EXPIRATION_ACTION_CODE;
	@JsonProperty("EXPIRATION_ACTION_DATE")
	private String EXPIRATION_ACTION_DATE;
	@JsonProperty("HOLD_DATE")
	private String HOLD_DATE;
	@JsonProperty("MATURITY_DATE")
	private String MATURITY_DATE;
	@JsonProperty("DATE_CODE")
	private String DATE_CODE;
	@JsonProperty("GRADE_CODE")
	private String GRADE_CODE;
	@JsonProperty("CHANGE_DATE")
	private String CHANGE_DATE;
	@JsonProperty("AGE")
	private String AGE;
	@JsonProperty("REASON_CODE")
	private String REASON_CODE;
	@JsonProperty("REASON_NAME")
	private String REASON_NAME;
	@JsonProperty("PROCESS_FLAG")
	private String PROCESS_FLAG;
	@JsonProperty("SUPPLIER_LOT_NUMBER")
	private String SUPPLIER_LOT_NUMBER;
	@JsonProperty("TERRITORY_CODE")
	private String TERRITORY_CODE;
	@JsonProperty("TERRITORY_SHORT_NAME")
	private String TERRITORY_SHORT_NAME;
	@JsonProperty("ITEM_SIZE")
	private String ITEM_SIZE;
	@JsonProperty("COLOR")
	private String COLOR;
	@JsonProperty("LOT_VOLUME")
	private String LOT_VOLUME;
	@JsonProperty("VOLUME_UOM_NAME")
	private String VOLUME_UOM_NAME;
	@JsonProperty("VOLUME_UOM")
	private String VOLUME_UOM;
	@JsonProperty("PLACE_OF_ORIGIN")
	private String PLACE_OF_ORIGIN;
	@JsonProperty("BEST_BY_DATE")
	private String BEST_BY_DATE;
	@JsonProperty("LOT_LENGTH")
	private String LOT_LENGTH;
	@JsonProperty("LENGTH_UOM")
	private String LENGTH_UOM;
	@JsonProperty("LENGTH_UOM_NAME")
	private String LENGTH_UOM_NAME;
	@JsonProperty("RECYCLED_CONTENT")
	private String RECYCLED_CONTENT;
	@JsonProperty("LOT_THICKNESS")
	private String LOT_THICKNESS;
	@JsonProperty("THICKNESS_UOM")
	private String THICKNESS_UOM;
	@JsonProperty("LOT_WIDTH")
	private String LOT_WIDTH;
	@JsonProperty("WIDTH_UOM")
	private String WIDTH_UOM;
	@JsonProperty("WIDTH_UOM_NAME")
	private String WIDTH_UOM_NAME;
	@JsonProperty("CURL_WRINKLE_FOLD")
	private String CURL_WRINKLE_FOLD;
	@JsonProperty("VENDOR_NAME")
	private String VENDOR_NAME;
	@JsonProperty("PRODUCT_CODE")
	private String PRODUCT_CODE;
	@JsonProperty("PRODUCT_TRANSACTION_ID")
	private String PRODUCT_TRANSACTION_ID;
	@JsonProperty("SECONDARY_TRANSACTION_QUANTITY")
	private String SECONDARY_TRANSACTION_QUANTITY;
	@JsonProperty("SUBLOT_NUM")
	private String SUBLOT_NUM;
	@JsonProperty("PARENT_LOT_NUMBER")
	private String PARENT_LOT_NUMBER;
	@JsonProperty("PARENT_OBJECT_TYPE")
	private String PARENT_OBJECT_TYPE;
	@JsonProperty("PARENT_OBJECT_NUMBER")
	private String PARENT_OBJECT_NUMBER;
	@JsonProperty("PARENT_OBJECT_TYPE2")
	private String PARENT_OBJECT_TYPE2;
	@JsonProperty("PARENT_OBJECT_NUMBER2")
	private String PARENT_OBJECT_NUMBER2;
	@JsonProperty("LOT_ATTRIBUTE_CATEGORY")
	private String LOT_ATTRIBUTE_CATEGORY;
	@JsonProperty("C_ATTRIBUTE1")
	private String C_ATTRIBUTE1;
	@JsonProperty("C_ATTRIBUTE2")
	private String C_ATTRIBUTE2;
	@JsonProperty("C_ATTRIBUTE3")
	private String C_ATTRIBUTE3;
	@JsonProperty("C_ATTRIBUTE4")
	private String C_ATTRIBUTE4;
	@JsonProperty("C_ATTRIBUTE5")
	private String C_ATTRIBUTE5;
	@JsonProperty("C_ATTRIBUTE6")
	private String C_ATTRIBUTE6;
	@JsonProperty("C_ATTRIBUTE7")
	private String C_ATTRIBUTE7;
	@JsonProperty("C_ATTRIBUTE8")
	private String C_ATTRIBUTE8;
	@JsonProperty("C_ATTRIBUTE9")
	private String C_ATTRIBUTE9;
	@JsonProperty("C_ATTRIBUTE10")
	private String C_ATTRIBUTE10;
	@JsonProperty("C_ATTRIBUTE11")
	private String C_ATTRIBUTE11;
	@JsonProperty("C_ATTRIBUTE12")
	private String C_ATTRIBUTE12;
	@JsonProperty("C_ATTRIBUTE13")
	private String C_ATTRIBUTE13;
	@JsonProperty("C_ATTRIBUTE14")
	private String C_ATTRIBUTE14;
	@JsonProperty("C_ATTRIBUTE15")
	private String C_ATTRIBUTE15;
	@JsonProperty("C_ATTRIBUTE16")
	private String C_ATTRIBUTE16;
	@JsonProperty("C_ATTRIBUTE17")
	private String C_ATTRIBUTE17;
	@JsonProperty("C_ATTRIBUTE18")
	private String C_ATTRIBUTE18;
	@JsonProperty("C_ATTRIBUTE19")
	private String C_ATTRIBUTE19;
	@JsonProperty("C_ATTRIBUTE20")
	private String C_ATTRIBUTE20;
	@JsonProperty("D_ATTRIBUTE1")
	private String D_ATTRIBUTE1;
	@JsonProperty("D_ATTRIBUTE2")
	private String D_ATTRIBUTE2;
	@JsonProperty("D_ATTRIBUTE3")
	private String D_ATTRIBUTE3;
	@JsonProperty("D_ATTRIBUTE4")
	private String D_ATTRIBUTE4;
	@JsonProperty("D_ATTRIBUTE5")
	private String D_ATTRIBUTE5;
	@JsonProperty("D_ATTRIBUTE6")
	private String D_ATTRIBUTE6;
	@JsonProperty("D_ATTRIBUTE7")
	private String D_ATTRIBUTE7;
	@JsonProperty("D_ATTRIBUTE8")
	private String D_ATTRIBUTE8;
	@JsonProperty("D_ATTRIBUTE9")
	private String D_ATTRIBUTE9;
	@JsonProperty("D_ATTRIBUTE10")
	private String D_ATTRIBUTE10;
	@JsonProperty("N_ATTRIBUTE1")
	private String N_ATTRIBUTE1;
	@JsonProperty("N_ATTRIBUTE2")
	private String N_ATTRIBUTE2;
	@JsonProperty("N_ATTRIBUTE3")
	private String N_ATTRIBUTE3;
	@JsonProperty("N_ATTRIBUTE4")
	private String N_ATTRIBUTE4;
	@JsonProperty("N_ATTRIBUTE5")
	private String N_ATTRIBUTE5;
	@JsonProperty("N_ATTRIBUTE6")
	private String N_ATTRIBUTE6;
	@JsonProperty("N_ATTRIBUTE7")
	private String N_ATTRIBUTE7;
	@JsonProperty("N_ATTRIBUTE8")
	private String N_ATTRIBUTE8;
	@JsonProperty("N_ATTRIBUTE9")
	private String N_ATTRIBUTE9;
	@JsonProperty("N_ATTRIBUTE10")
	private String N_ATTRIBUTE10;
	@JsonProperty("T_ATTRIBUTE1")
	private String T_ATTRIBUTE1;
	@JsonProperty("T_ATTRIBUTE2")
	private String T_ATTRIBUTE2;
	@JsonProperty("T_ATTRIBUTE3")
	private String T_ATTRIBUTE3;
	@JsonProperty("T_ATTRIBUTE4")
	private String T_ATTRIBUTE4;
	@JsonProperty("T_ATTRIBUTE5")
	private String T_ATTRIBUTE5;
	@JsonProperty("ATTRIBUTE_CATEGORY")
	private String ATTRIBUTE_CATEGORY;
	@JsonProperty("ATTRIBUTE1")
	private String ATTRIBUTE1;
	@JsonProperty("ATTRIBUTE2")
	private String ATTRIBUTE2;
	@JsonProperty("ATTRIBUTE3")
	private String ATTRIBUTE3;
	@JsonProperty("ATTRIBUTE4")
	private String ATTRIBUTE4;
	@JsonProperty("ATTRIBUTE5")
	private String ATTRIBUTE5;
	@JsonProperty("ATTRIBUTE6")
	private String ATTRIBUTE6;
	@JsonProperty("ATTRIBUTE7")
	private String ATTRIBUTE7;
	@JsonProperty("ATTRIBUTE8")
	private String ATTRIBUTE8;
	@JsonProperty("ATTRIBUTE9")
	private String ATTRIBUTE9;
	@JsonProperty("ATTRIBUTE10")
	private String ATTRIBUTE10;
	@JsonProperty("ATTRIBUTE11")
	private String ATTRIBUTE11;
	@JsonProperty("ATTRIBUTE12")
	private String ATTRIBUTE12;
	@JsonProperty("ATTRIBUTE13")
	private String ATTRIBUTE13;
	@JsonProperty("ATTRIBUTE14")
	private String ATTRIBUTE14;
	@JsonProperty("ATTRIBUTE15")
	private String ATTRIBUTE15;
	@JsonProperty("ATTRIBUTE16")
	private String ATTRIBUTE16;
	@JsonProperty("ATTRIBUTE17")
	private String ATTRIBUTE17;
	@JsonProperty("ATTRIBUTE18")
	private String ATTRIBUTE18;
	@JsonProperty("ATTRIBUTE19")
	private String ATTRIBUTE19;
	@JsonProperty("ATTRIBUTE20")
	private String ATTRIBUTE20;
	@JsonProperty("ATTRIBUTE_NUMBER1")
	private String ATTRIBUTE_NUMBER1;
	@JsonProperty("ATTRIBUTE_NUMBER2")
	private String ATTRIBUTE_NUMBER2;
	@JsonProperty("ATTRIBUTE_NUMBER3")
	private String ATTRIBUTE_NUMBER3;
	@JsonProperty("ATTRIBUTE_NUMBER4")
	private String ATTRIBUTE_NUMBER4;
	@JsonProperty("ATTRIBUTE_NUMBER5")
	private String ATTRIBUTE_NUMBER5;
	@JsonProperty("ATTRIBUTE_NUMBER6")
	private String ATTRIBUTE_NUMBER6;
	@JsonProperty("ATTRIBUTE_NUMBER7")
	private String ATTRIBUTE_NUMBER7;
	@JsonProperty("ATTRIBUTE_NUMBER8")
	private String ATTRIBUTE_NUMBER8;
	@JsonProperty("ATTRIBUTE_NUMBER9")
	private String ATTRIBUTE_NUMBER9;
	@JsonProperty("ATTRIBUTE_NUMBER10")
	private String ATTRIBUTE_NUMBER10;
	@JsonProperty("ATTRIBUTE_DATE1")
	private String ATTRIBUTE_DATE1;
	@JsonProperty("ATTRIBUTE_DATE2")
	private String ATTRIBUTE_DATE2;
	@JsonProperty("ATTRIBUTE_DATE3")
	private String ATTRIBUTE_DATE3;
	@JsonProperty("ATTRIBUTE_DATE4")
	private String ATTRIBUTE_DATE4;
	@JsonProperty("ATTRIBUTE_DATE5")
	private String ATTRIBUTE_DATE5;
	@JsonProperty("ATTRIBUTE_TIMESTAMP1")
	private String ATTRIBUTE_TIMESTAMP1;
	@JsonProperty("ATTRIBUTE_TIMESTAMP2")
	private String ATTRIBUTE_TIMESTAMP2;
	@JsonProperty("ATTRIBUTE_TIMESTAMP3")
	private String ATTRIBUTE_TIMESTAMP3;
	@JsonProperty("ATTRIBUTE_TIMESTAMP4")
	private String ATTRIBUTE_TIMESTAMP4;
	@JsonProperty("ATTRIBUTE_TIMESTAMP5")
	private String ATTRIBUTE_TIMESTAMP5;
	
	public String getLOT_INTERFACE_NBR() {
		return LOT_INTERFACE_NBR;
	}
	public void setLOT_INTERFACE_NBR(String lOT_INTERFACE_NBR) {
		LOT_INTERFACE_NBR = lOT_INTERFACE_NBR;
	}
	public String getINV_SERIAL_NBR() {
		return INV_SERIAL_NBR;
	}
	public void setINV_SERIAL_NBR(String iNV_SERIAL_NBR) {
		INV_SERIAL_NBR = iNV_SERIAL_NBR;
	}
	public String getSOURCE_CODE() {
		return SOURCE_CODE;
	}
	public void setSOURCE_CODE(String sOURCE_CODE) {
		SOURCE_CODE = sOURCE_CODE;
	}
	public String getSOURCE_LINE_ID() {
		return SOURCE_LINE_ID;
	}
	public void setSOURCE_LINE_ID(String sOURCE_LINE_ID) {
		SOURCE_LINE_ID = sOURCE_LINE_ID;
	}
	public String getLOT_NUMBER() {
		return LOT_NUMBER;
	}
	public void setLOT_NUMBER(String lOT_NUMBER) {
		LOT_NUMBER = lOT_NUMBER;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	public String getLOT_EXPIRATION_DATE() {
		return LOT_EXPIRATION_DATE;
	}
	public void setLOT_EXPIRATION_DATE(String lOT_EXPIRATION_DATE) {
		LOT_EXPIRATION_DATE = lOT_EXPIRATION_DATE;
	}
	public String getTRANSACTION_QUANTITY() {
		return TRANSACTION_QUANTITY;
	}
	public void setTRANSACTION_QUANTITY(String tRANSACTION_QUANTITY) {
		TRANSACTION_QUANTITY = tRANSACTION_QUANTITY;
	}
	public String getPRIMARY_QUANTITY() {
		return PRIMARY_QUANTITY;
	}
	public void setPRIMARY_QUANTITY(String pRIMARY_QUANTITY) {
		PRIMARY_QUANTITY = pRIMARY_QUANTITY;
	}
	public String getORIGINATION_TYPE() {
		return ORIGINATION_TYPE;
	}
	public void setORIGINATION_TYPE(String oRIGINATION_TYPE) {
		ORIGINATION_TYPE = oRIGINATION_TYPE;
	}
	public String getORIGINATION_DATE() {
		return ORIGINATION_DATE;
	}
	public void setORIGINATION_DATE(String oRIGINATION_DATE) {
		ORIGINATION_DATE = oRIGINATION_DATE;
	}
	public String getSTATUS_CODE() {
		return STATUS_CODE;
	}
	public void setSTATUS_CODE(String sTATUS_CODE) {
		STATUS_CODE = sTATUS_CODE;
	}
	public String getRETEST_DATE() {
		return RETEST_DATE;
	}
	public void setRETEST_DATE(String rETEST_DATE) {
		RETEST_DATE = rETEST_DATE;
	}
	public String getEXPIRATION_ACTION_NAME() {
		return EXPIRATION_ACTION_NAME;
	}
	public void setEXPIRATION_ACTION_NAME(String eXPIRATION_ACTION_NAME) {
		EXPIRATION_ACTION_NAME = eXPIRATION_ACTION_NAME;
	}
	public String getEXPIRATION_ACTION_CODE() {
		return EXPIRATION_ACTION_CODE;
	}
	public void setEXPIRATION_ACTION_CODE(String eXPIRATION_ACTION_CODE) {
		EXPIRATION_ACTION_CODE = eXPIRATION_ACTION_CODE;
	}
	public String getEXPIRATION_ACTION_DATE() {
		return EXPIRATION_ACTION_DATE;
	}
	public void setEXPIRATION_ACTION_DATE(String eXPIRATION_ACTION_DATE) {
		EXPIRATION_ACTION_DATE = eXPIRATION_ACTION_DATE;
	}
	public String getHOLD_DATE() {
		return HOLD_DATE;
	}
	public void setHOLD_DATE(String hOLD_DATE) {
		HOLD_DATE = hOLD_DATE;
	}
	public String getMATURITY_DATE() {
		return MATURITY_DATE;
	}
	public void setMATURITY_DATE(String mATURITY_DATE) {
		MATURITY_DATE = mATURITY_DATE;
	}
	public String getDATE_CODE() {
		return DATE_CODE;
	}
	public void setDATE_CODE(String dATE_CODE) {
		DATE_CODE = dATE_CODE;
	}
	public String getGRADE_CODE() {
		return GRADE_CODE;
	}
	public void setGRADE_CODE(String gRADE_CODE) {
		GRADE_CODE = gRADE_CODE;
	}
	public String getCHANGE_DATE() {
		return CHANGE_DATE;
	}
	public void setCHANGE_DATE(String cHANGE_DATE) {
		CHANGE_DATE = cHANGE_DATE;
	}
	public String getAGE() {
		return AGE;
	}
	public void setAGE(String aGE) {
		AGE = aGE;
	}
	public String getREASON_CODE() {
		return REASON_CODE;
	}
	public void setREASON_CODE(String rEASON_CODE) {
		REASON_CODE = rEASON_CODE;
	}
	public String getREASON_NAME() {
		return REASON_NAME;
	}
	public void setREASON_NAME(String rEASON_NAME) {
		REASON_NAME = rEASON_NAME;
	}
	public String getPROCESS_FLAG() {
		return PROCESS_FLAG;
	}
	public void setPROCESS_FLAG(String pROCESS_FLAG) {
		PROCESS_FLAG = pROCESS_FLAG;
	}
	public String getSUPPLIER_LOT_NUMBER() {
		return SUPPLIER_LOT_NUMBER;
	}
	public void setSUPPLIER_LOT_NUMBER(String sUPPLIER_LOT_NUMBER) {
		SUPPLIER_LOT_NUMBER = sUPPLIER_LOT_NUMBER;
	}
	public String getTERRITORY_CODE() {
		return TERRITORY_CODE;
	}
	public void setTERRITORY_CODE(String tERRITORY_CODE) {
		TERRITORY_CODE = tERRITORY_CODE;
	}
	public String getTERRITORY_SHORT_NAME() {
		return TERRITORY_SHORT_NAME;
	}
	public void setTERRITORY_SHORT_NAME(String tERRITORY_SHORT_NAME) {
		TERRITORY_SHORT_NAME = tERRITORY_SHORT_NAME;
	}
	public String getITEM_SIZE() {
		return ITEM_SIZE;
	}
	public void setITEM_SIZE(String iTEM_SIZE) {
		ITEM_SIZE = iTEM_SIZE;
	}
	public String getCOLOR() {
		return COLOR;
	}
	public void setCOLOR(String cOLOR) {
		COLOR = cOLOR;
	}
	public String getLOT_VOLUME() {
		return LOT_VOLUME;
	}
	public void setLOT_VOLUME(String lOT_VOLUME) {
		LOT_VOLUME = lOT_VOLUME;
	}
	public String getVOLUME_UOM_NAME() {
		return VOLUME_UOM_NAME;
	}
	public void setVOLUME_UOM_NAME(String vOLUME_UOM_NAME) {
		VOLUME_UOM_NAME = vOLUME_UOM_NAME;
	}
	public String getVOLUME_UOM() {
		return VOLUME_UOM;
	}
	public void setVOLUME_UOM(String vOLUME_UOM) {
		VOLUME_UOM = vOLUME_UOM;
	}
	public String getPLACE_OF_ORIGIN() {
		return PLACE_OF_ORIGIN;
	}
	public void setPLACE_OF_ORIGIN(String pLACE_OF_ORIGIN) {
		PLACE_OF_ORIGIN = pLACE_OF_ORIGIN;
	}
	public String getBEST_BY_DATE() {
		return BEST_BY_DATE;
	}
	public void setBEST_BY_DATE(String bEST_BY_DATE) {
		BEST_BY_DATE = bEST_BY_DATE;
	}
	public String getLOT_LENGTH() {
		return LOT_LENGTH;
	}
	public void setLOT_LENGTH(String lOT_LENGTH) {
		LOT_LENGTH = lOT_LENGTH;
	}
	public String getLENGTH_UOM() {
		return LENGTH_UOM;
	}
	public void setLENGTH_UOM(String lENGTH_UOM) {
		LENGTH_UOM = lENGTH_UOM;
	}
	public String getLENGTH_UOM_NAME() {
		return LENGTH_UOM_NAME;
	}
	public void setLENGTH_UOM_NAME(String lENGTH_UOM_NAME) {
		LENGTH_UOM_NAME = lENGTH_UOM_NAME;
	}
	public String getRECYCLED_CONTENT() {
		return RECYCLED_CONTENT;
	}
	public void setRECYCLED_CONTENT(String rECYCLED_CONTENT) {
		RECYCLED_CONTENT = rECYCLED_CONTENT;
	}
	public String getLOT_THICKNESS() {
		return LOT_THICKNESS;
	}
	public void setLOT_THICKNESS(String lOT_THICKNESS) {
		LOT_THICKNESS = lOT_THICKNESS;
	}
	public String getTHICKNESS_UOM() {
		return THICKNESS_UOM;
	}
	public void setTHICKNESS_UOM(String tHICKNESS_UOM) {
		THICKNESS_UOM = tHICKNESS_UOM;
	}
	public String getLOT_WIDTH() {
		return LOT_WIDTH;
	}
	public void setLOT_WIDTH(String lOT_WIDTH) {
		LOT_WIDTH = lOT_WIDTH;
	}
	public String getWIDTH_UOM() {
		return WIDTH_UOM;
	}
	public void setWIDTH_UOM(String wIDTH_UOM) {
		WIDTH_UOM = wIDTH_UOM;
	}
	public String getWIDTH_UOM_NAME() {
		return WIDTH_UOM_NAME;
	}
	public void setWIDTH_UOM_NAME(String wIDTH_UOM_NAME) {
		WIDTH_UOM_NAME = wIDTH_UOM_NAME;
	}
	public String getCURL_WRINKLE_FOLD() {
		return CURL_WRINKLE_FOLD;
	}
	public void setCURL_WRINKLE_FOLD(String cURL_WRINKLE_FOLD) {
		CURL_WRINKLE_FOLD = cURL_WRINKLE_FOLD;
	}
	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}
	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}
	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}
	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}
	public String getPRODUCT_TRANSACTION_ID() {
		return PRODUCT_TRANSACTION_ID;
	}
	public void setPRODUCT_TRANSACTION_ID(String pRODUCT_TRANSACTION_ID) {
		PRODUCT_TRANSACTION_ID = pRODUCT_TRANSACTION_ID;
	}
	public String getSECONDARY_TRANSACTION_QUANTITY() {
		return SECONDARY_TRANSACTION_QUANTITY;
	}
	public void setSECONDARY_TRANSACTION_QUANTITY(String sECONDARY_TRANSACTION_QUANTITY) {
		SECONDARY_TRANSACTION_QUANTITY = sECONDARY_TRANSACTION_QUANTITY;
	}
	public String getSUBLOT_NUM() {
		return SUBLOT_NUM;
	}
	public void setSUBLOT_NUM(String sUBLOT_NUM) {
		SUBLOT_NUM = sUBLOT_NUM;
	}
	public String getPARENT_LOT_NUMBER() {
		return PARENT_LOT_NUMBER;
	}
	public void setPARENT_LOT_NUMBER(String pARENT_LOT_NUMBER) {
		PARENT_LOT_NUMBER = pARENT_LOT_NUMBER;
	}
	public String getPARENT_OBJECT_TYPE() {
		return PARENT_OBJECT_TYPE;
	}
	public void setPARENT_OBJECT_TYPE(String pARENT_OBJECT_TYPE) {
		PARENT_OBJECT_TYPE = pARENT_OBJECT_TYPE;
	}
	public String getPARENT_OBJECT_NUMBER() {
		return PARENT_OBJECT_NUMBER;
	}
	public void setPARENT_OBJECT_NUMBER(String pARENT_OBJECT_NUMBER) {
		PARENT_OBJECT_NUMBER = pARENT_OBJECT_NUMBER;
	}
	public String getPARENT_OBJECT_TYPE2() {
		return PARENT_OBJECT_TYPE2;
	}
	public void setPARENT_OBJECT_TYPE2(String pARENT_OBJECT_TYPE2) {
		PARENT_OBJECT_TYPE2 = pARENT_OBJECT_TYPE2;
	}
	public String getPARENT_OBJECT_NUMBER2() {
		return PARENT_OBJECT_NUMBER2;
	}
	public void setPARENT_OBJECT_NUMBER2(String pARENT_OBJECT_NUMBER2) {
		PARENT_OBJECT_NUMBER2 = pARENT_OBJECT_NUMBER2;
	}
	public String getLOT_ATTRIBUTE_CATEGORY() {
		return LOT_ATTRIBUTE_CATEGORY;
	}
	public void setLOT_ATTRIBUTE_CATEGORY(String lOT_ATTRIBUTE_CATEGORY) {
		LOT_ATTRIBUTE_CATEGORY = lOT_ATTRIBUTE_CATEGORY;
	}
	public String getC_ATTRIBUTE1() {
		return C_ATTRIBUTE1;
	}
	public void setC_ATTRIBUTE1(String c_ATTRIBUTE1) {
		C_ATTRIBUTE1 = c_ATTRIBUTE1;
	}
	public String getC_ATTRIBUTE2() {
		return C_ATTRIBUTE2;
	}
	public void setC_ATTRIBUTE2(String c_ATTRIBUTE2) {
		C_ATTRIBUTE2 = c_ATTRIBUTE2;
	}
	public String getC_ATTRIBUTE3() {
		return C_ATTRIBUTE3;
	}
	public void setC_ATTRIBUTE3(String c_ATTRIBUTE3) {
		C_ATTRIBUTE3 = c_ATTRIBUTE3;
	}
	public String getC_ATTRIBUTE4() {
		return C_ATTRIBUTE4;
	}
	public void setC_ATTRIBUTE4(String c_ATTRIBUTE4) {
		C_ATTRIBUTE4 = c_ATTRIBUTE4;
	}
	public String getC_ATTRIBUTE5() {
		return C_ATTRIBUTE5;
	}
	public void setC_ATTRIBUTE5(String c_ATTRIBUTE5) {
		C_ATTRIBUTE5 = c_ATTRIBUTE5;
	}
	public String getC_ATTRIBUTE6() {
		return C_ATTRIBUTE6;
	}
	public void setC_ATTRIBUTE6(String c_ATTRIBUTE6) {
		C_ATTRIBUTE6 = c_ATTRIBUTE6;
	}
	public String getC_ATTRIBUTE7() {
		return C_ATTRIBUTE7;
	}
	public void setC_ATTRIBUTE7(String c_ATTRIBUTE7) {
		C_ATTRIBUTE7 = c_ATTRIBUTE7;
	}
	public String getC_ATTRIBUTE8() {
		return C_ATTRIBUTE8;
	}
	public void setC_ATTRIBUTE8(String c_ATTRIBUTE8) {
		C_ATTRIBUTE8 = c_ATTRIBUTE8;
	}
	public String getC_ATTRIBUTE9() {
		return C_ATTRIBUTE9;
	}
	public void setC_ATTRIBUTE9(String c_ATTRIBUTE9) {
		C_ATTRIBUTE9 = c_ATTRIBUTE9;
	}
	public String getC_ATTRIBUTE10() {
		return C_ATTRIBUTE10;
	}
	public void setC_ATTRIBUTE10(String c_ATTRIBUTE10) {
		C_ATTRIBUTE10 = c_ATTRIBUTE10;
	}
	public String getC_ATTRIBUTE11() {
		return C_ATTRIBUTE11;
	}
	public void setC_ATTRIBUTE11(String c_ATTRIBUTE11) {
		C_ATTRIBUTE11 = c_ATTRIBUTE11;
	}
	public String getC_ATTRIBUTE12() {
		return C_ATTRIBUTE12;
	}
	public void setC_ATTRIBUTE12(String c_ATTRIBUTE12) {
		C_ATTRIBUTE12 = c_ATTRIBUTE12;
	}
	public String getC_ATTRIBUTE13() {
		return C_ATTRIBUTE13;
	}
	public void setC_ATTRIBUTE13(String c_ATTRIBUTE13) {
		C_ATTRIBUTE13 = c_ATTRIBUTE13;
	}
	public String getC_ATTRIBUTE14() {
		return C_ATTRIBUTE14;
	}
	public void setC_ATTRIBUTE14(String c_ATTRIBUTE14) {
		C_ATTRIBUTE14 = c_ATTRIBUTE14;
	}
	public String getC_ATTRIBUTE15() {
		return C_ATTRIBUTE15;
	}
	public void setC_ATTRIBUTE15(String c_ATTRIBUTE15) {
		C_ATTRIBUTE15 = c_ATTRIBUTE15;
	}
	public String getC_ATTRIBUTE16() {
		return C_ATTRIBUTE16;
	}
	public void setC_ATTRIBUTE16(String c_ATTRIBUTE16) {
		C_ATTRIBUTE16 = c_ATTRIBUTE16;
	}
	public String getC_ATTRIBUTE17() {
		return C_ATTRIBUTE17;
	}
	public void setC_ATTRIBUTE17(String c_ATTRIBUTE17) {
		C_ATTRIBUTE17 = c_ATTRIBUTE17;
	}
	public String getC_ATTRIBUTE18() {
		return C_ATTRIBUTE18;
	}
	public void setC_ATTRIBUTE18(String c_ATTRIBUTE18) {
		C_ATTRIBUTE18 = c_ATTRIBUTE18;
	}
	public String getC_ATTRIBUTE19() {
		return C_ATTRIBUTE19;
	}
	public void setC_ATTRIBUTE19(String c_ATTRIBUTE19) {
		C_ATTRIBUTE19 = c_ATTRIBUTE19;
	}
	public String getC_ATTRIBUTE20() {
		return C_ATTRIBUTE20;
	}
	public void setC_ATTRIBUTE20(String c_ATTRIBUTE20) {
		C_ATTRIBUTE20 = c_ATTRIBUTE20;
	}
	public String getD_ATTRIBUTE1() {
		return D_ATTRIBUTE1;
	}
	public void setD_ATTRIBUTE1(String d_ATTRIBUTE1) {
		D_ATTRIBUTE1 = d_ATTRIBUTE1;
	}
	public String getD_ATTRIBUTE2() {
		return D_ATTRIBUTE2;
	}
	public void setD_ATTRIBUTE2(String d_ATTRIBUTE2) {
		D_ATTRIBUTE2 = d_ATTRIBUTE2;
	}
	public String getD_ATTRIBUTE3() {
		return D_ATTRIBUTE3;
	}
	public void setD_ATTRIBUTE3(String d_ATTRIBUTE3) {
		D_ATTRIBUTE3 = d_ATTRIBUTE3;
	}
	public String getD_ATTRIBUTE4() {
		return D_ATTRIBUTE4;
	}
	public void setD_ATTRIBUTE4(String d_ATTRIBUTE4) {
		D_ATTRIBUTE4 = d_ATTRIBUTE4;
	}
	public String getD_ATTRIBUTE5() {
		return D_ATTRIBUTE5;
	}
	public void setD_ATTRIBUTE5(String d_ATTRIBUTE5) {
		D_ATTRIBUTE5 = d_ATTRIBUTE5;
	}
	public String getD_ATTRIBUTE6() {
		return D_ATTRIBUTE6;
	}
	public void setD_ATTRIBUTE6(String d_ATTRIBUTE6) {
		D_ATTRIBUTE6 = d_ATTRIBUTE6;
	}
	public String getD_ATTRIBUTE7() {
		return D_ATTRIBUTE7;
	}
	public void setD_ATTRIBUTE7(String d_ATTRIBUTE7) {
		D_ATTRIBUTE7 = d_ATTRIBUTE7;
	}
	public String getD_ATTRIBUTE8() {
		return D_ATTRIBUTE8;
	}
	public void setD_ATTRIBUTE8(String d_ATTRIBUTE8) {
		D_ATTRIBUTE8 = d_ATTRIBUTE8;
	}
	public String getD_ATTRIBUTE9() {
		return D_ATTRIBUTE9;
	}
	public void setD_ATTRIBUTE9(String d_ATTRIBUTE9) {
		D_ATTRIBUTE9 = d_ATTRIBUTE9;
	}
	public String getD_ATTRIBUTE10() {
		return D_ATTRIBUTE10;
	}
	public void setD_ATTRIBUTE10(String d_ATTRIBUTE10) {
		D_ATTRIBUTE10 = d_ATTRIBUTE10;
	}
	public String getN_ATTRIBUTE1() {
		return N_ATTRIBUTE1;
	}
	public void setN_ATTRIBUTE1(String n_ATTRIBUTE1) {
		N_ATTRIBUTE1 = n_ATTRIBUTE1;
	}
	public String getN_ATTRIBUTE2() {
		return N_ATTRIBUTE2;
	}
	public void setN_ATTRIBUTE2(String n_ATTRIBUTE2) {
		N_ATTRIBUTE2 = n_ATTRIBUTE2;
	}
	public String getN_ATTRIBUTE3() {
		return N_ATTRIBUTE3;
	}
	public void setN_ATTRIBUTE3(String n_ATTRIBUTE3) {
		N_ATTRIBUTE3 = n_ATTRIBUTE3;
	}
	public String getN_ATTRIBUTE4() {
		return N_ATTRIBUTE4;
	}
	public void setN_ATTRIBUTE4(String n_ATTRIBUTE4) {
		N_ATTRIBUTE4 = n_ATTRIBUTE4;
	}
	public String getN_ATTRIBUTE5() {
		return N_ATTRIBUTE5;
	}
	public void setN_ATTRIBUTE5(String n_ATTRIBUTE5) {
		N_ATTRIBUTE5 = n_ATTRIBUTE5;
	}
	public String getN_ATTRIBUTE6() {
		return N_ATTRIBUTE6;
	}
	public void setN_ATTRIBUTE6(String n_ATTRIBUTE6) {
		N_ATTRIBUTE6 = n_ATTRIBUTE6;
	}
	public String getN_ATTRIBUTE7() {
		return N_ATTRIBUTE7;
	}
	public void setN_ATTRIBUTE7(String n_ATTRIBUTE7) {
		N_ATTRIBUTE7 = n_ATTRIBUTE7;
	}
	public String getN_ATTRIBUTE8() {
		return N_ATTRIBUTE8;
	}
	public void setN_ATTRIBUTE8(String n_ATTRIBUTE8) {
		N_ATTRIBUTE8 = n_ATTRIBUTE8;
	}
	public String getN_ATTRIBUTE9() {
		return N_ATTRIBUTE9;
	}
	public void setN_ATTRIBUTE9(String n_ATTRIBUTE9) {
		N_ATTRIBUTE9 = n_ATTRIBUTE9;
	}
	public String getN_ATTRIBUTE10() {
		return N_ATTRIBUTE10;
	}
	public void setN_ATTRIBUTE10(String n_ATTRIBUTE10) {
		N_ATTRIBUTE10 = n_ATTRIBUTE10;
	}
	public String getT_ATTRIBUTE1() {
		return T_ATTRIBUTE1;
	}
	public void setT_ATTRIBUTE1(String t_ATTRIBUTE1) {
		T_ATTRIBUTE1 = t_ATTRIBUTE1;
	}
	public String getT_ATTRIBUTE2() {
		return T_ATTRIBUTE2;
	}
	public void setT_ATTRIBUTE2(String t_ATTRIBUTE2) {
		T_ATTRIBUTE2 = t_ATTRIBUTE2;
	}
	public String getT_ATTRIBUTE3() {
		return T_ATTRIBUTE3;
	}
	public void setT_ATTRIBUTE3(String t_ATTRIBUTE3) {
		T_ATTRIBUTE3 = t_ATTRIBUTE3;
	}
	public String getT_ATTRIBUTE4() {
		return T_ATTRIBUTE4;
	}
	public void setT_ATTRIBUTE4(String t_ATTRIBUTE4) {
		T_ATTRIBUTE4 = t_ATTRIBUTE4;
	}
	public String getT_ATTRIBUTE5() {
		return T_ATTRIBUTE5;
	}
	public void setT_ATTRIBUTE5(String t_ATTRIBUTE5) {
		T_ATTRIBUTE5 = t_ATTRIBUTE5;
	}
	public String getATTRIBUTE_CATEGORY() {
		return ATTRIBUTE_CATEGORY;
	}
	public void setATTRIBUTE_CATEGORY(String aTTRIBUTE_CATEGORY) {
		ATTRIBUTE_CATEGORY = aTTRIBUTE_CATEGORY;
	}
	public String getATTRIBUTE1() {
		return ATTRIBUTE1;
	}
	public void setATTRIBUTE1(String aTTRIBUTE1) {
		ATTRIBUTE1 = aTTRIBUTE1;
	}
	public String getATTRIBUTE2() {
		return ATTRIBUTE2;
	}
	public void setATTRIBUTE2(String aTTRIBUTE2) {
		ATTRIBUTE2 = aTTRIBUTE2;
	}
	public String getATTRIBUTE3() {
		return ATTRIBUTE3;
	}
	public void setATTRIBUTE3(String aTTRIBUTE3) {
		ATTRIBUTE3 = aTTRIBUTE3;
	}
	public String getATTRIBUTE4() {
		return ATTRIBUTE4;
	}
	public void setATTRIBUTE4(String aTTRIBUTE4) {
		ATTRIBUTE4 = aTTRIBUTE4;
	}
	public String getATTRIBUTE5() {
		return ATTRIBUTE5;
	}
	public void setATTRIBUTE5(String aTTRIBUTE5) {
		ATTRIBUTE5 = aTTRIBUTE5;
	}
	public String getATTRIBUTE6() {
		return ATTRIBUTE6;
	}
	public void setATTRIBUTE6(String aTTRIBUTE6) {
		ATTRIBUTE6 = aTTRIBUTE6;
	}
	public String getATTRIBUTE7() {
		return ATTRIBUTE7;
	}
	public void setATTRIBUTE7(String aTTRIBUTE7) {
		ATTRIBUTE7 = aTTRIBUTE7;
	}
	public String getATTRIBUTE8() {
		return ATTRIBUTE8;
	}
	public void setATTRIBUTE8(String aTTRIBUTE8) {
		ATTRIBUTE8 = aTTRIBUTE8;
	}
	public String getATTRIBUTE9() {
		return ATTRIBUTE9;
	}
	public void setATTRIBUTE9(String aTTRIBUTE9) {
		ATTRIBUTE9 = aTTRIBUTE9;
	}
	public String getATTRIBUTE10() {
		return ATTRIBUTE10;
	}
	public void setATTRIBUTE10(String aTTRIBUTE10) {
		ATTRIBUTE10 = aTTRIBUTE10;
	}
	public String getATTRIBUTE11() {
		return ATTRIBUTE11;
	}
	public void setATTRIBUTE11(String aTTRIBUTE11) {
		ATTRIBUTE11 = aTTRIBUTE11;
	}
	public String getATTRIBUTE12() {
		return ATTRIBUTE12;
	}
	public void setATTRIBUTE12(String aTTRIBUTE12) {
		ATTRIBUTE12 = aTTRIBUTE12;
	}
	public String getATTRIBUTE13() {
		return ATTRIBUTE13;
	}
	public void setATTRIBUTE13(String aTTRIBUTE13) {
		ATTRIBUTE13 = aTTRIBUTE13;
	}
	public String getATTRIBUTE14() {
		return ATTRIBUTE14;
	}
	public void setATTRIBUTE14(String aTTRIBUTE14) {
		ATTRIBUTE14 = aTTRIBUTE14;
	}
	public String getATTRIBUTE15() {
		return ATTRIBUTE15;
	}
	public void setATTRIBUTE15(String aTTRIBUTE15) {
		ATTRIBUTE15 = aTTRIBUTE15;
	}
	public String getATTRIBUTE16() {
		return ATTRIBUTE16;
	}
	public void setATTRIBUTE16(String aTTRIBUTE16) {
		ATTRIBUTE16 = aTTRIBUTE16;
	}
	public String getATTRIBUTE17() {
		return ATTRIBUTE17;
	}
	public void setATTRIBUTE17(String aTTRIBUTE17) {
		ATTRIBUTE17 = aTTRIBUTE17;
	}
	public String getATTRIBUTE18() {
		return ATTRIBUTE18;
	}
	public void setATTRIBUTE18(String aTTRIBUTE18) {
		ATTRIBUTE18 = aTTRIBUTE18;
	}
	public String getATTRIBUTE19() {
		return ATTRIBUTE19;
	}
	public void setATTRIBUTE19(String aTTRIBUTE19) {
		ATTRIBUTE19 = aTTRIBUTE19;
	}
	public String getATTRIBUTE20() {
		return ATTRIBUTE20;
	}
	public void setATTRIBUTE20(String aTTRIBUTE20) {
		ATTRIBUTE20 = aTTRIBUTE20;
	}
	public String getATTRIBUTE_NUMBER1() {
		return ATTRIBUTE_NUMBER1;
	}
	public void setATTRIBUTE_NUMBER1(String aTTRIBUTE_NUMBER1) {
		ATTRIBUTE_NUMBER1 = aTTRIBUTE_NUMBER1;
	}
	public String getATTRIBUTE_NUMBER2() {
		return ATTRIBUTE_NUMBER2;
	}
	public void setATTRIBUTE_NUMBER2(String aTTRIBUTE_NUMBER2) {
		ATTRIBUTE_NUMBER2 = aTTRIBUTE_NUMBER2;
	}
	public String getATTRIBUTE_NUMBER3() {
		return ATTRIBUTE_NUMBER3;
	}
	public void setATTRIBUTE_NUMBER3(String aTTRIBUTE_NUMBER3) {
		ATTRIBUTE_NUMBER3 = aTTRIBUTE_NUMBER3;
	}
	public String getATTRIBUTE_NUMBER4() {
		return ATTRIBUTE_NUMBER4;
	}
	public void setATTRIBUTE_NUMBER4(String aTTRIBUTE_NUMBER4) {
		ATTRIBUTE_NUMBER4 = aTTRIBUTE_NUMBER4;
	}
	public String getATTRIBUTE_NUMBER5() {
		return ATTRIBUTE_NUMBER5;
	}
	public void setATTRIBUTE_NUMBER5(String aTTRIBUTE_NUMBER5) {
		ATTRIBUTE_NUMBER5 = aTTRIBUTE_NUMBER5;
	}
	public String getATTRIBUTE_NUMBER6() {
		return ATTRIBUTE_NUMBER6;
	}
	public void setATTRIBUTE_NUMBER6(String aTTRIBUTE_NUMBER6) {
		ATTRIBUTE_NUMBER6 = aTTRIBUTE_NUMBER6;
	}
	public String getATTRIBUTE_NUMBER7() {
		return ATTRIBUTE_NUMBER7;
	}
	public void setATTRIBUTE_NUMBER7(String aTTRIBUTE_NUMBER7) {
		ATTRIBUTE_NUMBER7 = aTTRIBUTE_NUMBER7;
	}
	public String getATTRIBUTE_NUMBER8() {
		return ATTRIBUTE_NUMBER8;
	}
	public void setATTRIBUTE_NUMBER8(String aTTRIBUTE_NUMBER8) {
		ATTRIBUTE_NUMBER8 = aTTRIBUTE_NUMBER8;
	}
	public String getATTRIBUTE_NUMBER9() {
		return ATTRIBUTE_NUMBER9;
	}
	public void setATTRIBUTE_NUMBER9(String aTTRIBUTE_NUMBER9) {
		ATTRIBUTE_NUMBER9 = aTTRIBUTE_NUMBER9;
	}
	public String getATTRIBUTE_NUMBER10() {
		return ATTRIBUTE_NUMBER10;
	}
	public void setATTRIBUTE_NUMBER10(String aTTRIBUTE_NUMBER10) {
		ATTRIBUTE_NUMBER10 = aTTRIBUTE_NUMBER10;
	}
	public String getATTRIBUTE_DATE1() {
		return ATTRIBUTE_DATE1;
	}
	public void setATTRIBUTE_DATE1(String aTTRIBUTE_DATE1) {
		ATTRIBUTE_DATE1 = aTTRIBUTE_DATE1;
	}
	public String getATTRIBUTE_DATE2() {
		return ATTRIBUTE_DATE2;
	}
	public void setATTRIBUTE_DATE2(String aTTRIBUTE_DATE2) {
		ATTRIBUTE_DATE2 = aTTRIBUTE_DATE2;
	}
	public String getATTRIBUTE_DATE3() {
		return ATTRIBUTE_DATE3;
	}
	public void setATTRIBUTE_DATE3(String aTTRIBUTE_DATE3) {
		ATTRIBUTE_DATE3 = aTTRIBUTE_DATE3;
	}
	public String getATTRIBUTE_DATE4() {
		return ATTRIBUTE_DATE4;
	}
	public void setATTRIBUTE_DATE4(String aTTRIBUTE_DATE4) {
		ATTRIBUTE_DATE4 = aTTRIBUTE_DATE4;
	}
	public String getATTRIBUTE_DATE5() {
		return ATTRIBUTE_DATE5;
	}
	public void setATTRIBUTE_DATE5(String aTTRIBUTE_DATE5) {
		ATTRIBUTE_DATE5 = aTTRIBUTE_DATE5;
	}
	public String getATTRIBUTE_TIMESTAMP1() {
		return ATTRIBUTE_TIMESTAMP1;
	}
	public void setATTRIBUTE_TIMESTAMP1(String aTTRIBUTE_TIMESTAMP1) {
		ATTRIBUTE_TIMESTAMP1 = aTTRIBUTE_TIMESTAMP1;
	}
	public String getATTRIBUTE_TIMESTAMP2() {
		return ATTRIBUTE_TIMESTAMP2;
	}
	public void setATTRIBUTE_TIMESTAMP2(String aTTRIBUTE_TIMESTAMP2) {
		ATTRIBUTE_TIMESTAMP2 = aTTRIBUTE_TIMESTAMP2;
	}
	public String getATTRIBUTE_TIMESTAMP3() {
		return ATTRIBUTE_TIMESTAMP3;
	}
	public void setATTRIBUTE_TIMESTAMP3(String aTTRIBUTE_TIMESTAMP3) {
		ATTRIBUTE_TIMESTAMP3 = aTTRIBUTE_TIMESTAMP3;
	}
	public String getATTRIBUTE_TIMESTAMP4() {
		return ATTRIBUTE_TIMESTAMP4;
	}
	public void setATTRIBUTE_TIMESTAMP4(String aTTRIBUTE_TIMESTAMP4) {
		ATTRIBUTE_TIMESTAMP4 = aTTRIBUTE_TIMESTAMP4;
	}
	public String getATTRIBUTE_TIMESTAMP5() {
		return ATTRIBUTE_TIMESTAMP5;
	}
	public void setATTRIBUTE_TIMESTAMP5(String aTTRIBUTE_TIMESTAMP5) {
		ATTRIBUTE_TIMESTAMP5 = aTTRIBUTE_TIMESTAMP5;
	}

}

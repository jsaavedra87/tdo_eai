package com.tdo.integration.dto;

import java.util.Map;

public class CreditNoteResponseDTO {
	
	private String sucursal;
	private String folio; 
	private Map responseMap;
	
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public Map getResponseMap() {
		return responseMap;
	}
	public void setResponseMap(Map responseMap) {
		this.responseMap = responseMap;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	
}

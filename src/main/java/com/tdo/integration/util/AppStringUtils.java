package com.tdo.integration.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class AppStringUtils {

	public static String getEnvResponse(String str) {
		try {
				String wordToFind = "env:Envelope";
				Pattern word = Pattern.compile(wordToFind);
				Matcher match = word.matcher(str);
		
				int start = 0;
				int end = 0;
				while (match.find()) {
				     if(start == 0) start = match.start() - 1;
				     end = match.end() + 1;
				}
				 String result = str.substring(start, end);
				 return result;
	
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getContentFromEnv(String str) {
		try {
			JSONObject jsonObject = new JSONObject(str);
			return (String) jsonObject.get("content");
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}

}

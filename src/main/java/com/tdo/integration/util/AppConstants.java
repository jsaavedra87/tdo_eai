package com.tdo.integration.util;

import java.util.ArrayList;
import java.util.List;

public final class AppConstants {
	
	public static final String GLSuccess_Msg = "El archivo de ventas ha sido generado de forma exitosa: ";
	public static final String SOAP_SALESPARTY_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/SalesPartyService";
	public static final String SOAP_CUSTOMERPROFILE_URL = "https://eiyp.fa.us6.oraclecloud.com:443/fscmService/ReceivablesCustomerProfileService";
	public static final String SOAP_CUSTOMERACCOUNT_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/CustomerAccountService";
	
	public static final String SOAP_ITEMMASTER_URL = "https://eiyp.fa.us6.oraclecloud.com:443/fscmService/ItemServiceV2";
	public static final String SOAP_ITEMCOST_URL = "https://eiyp.fa.us6.oraclecloud.com:443/fscmService/ItemCostServiceV2";
	
	
	public static final String SOAP_CREATELOCATION_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/FoundationPartiesLocationService";
	public static final String SOAP_CREATEORGANIZATION_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/FoundationPartiesOrganizationService";	
	public static final String SOAP_CREATEACCOUNT_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/CustomerAccountService";
	public static final String SOAP_CREATEPROFILE_URL = "https://eiyp.fa.us6.oraclecloud.com:443/fscmService/ReceivablesCustomerProfileService";
	public static final String SOAP_SESSIONID_URL = "https://eiyp.fa.us6.oraclecloud.com/analytics-ws/saw.dll?SoapImpl=nQSessionService";
	public static final String SOAP_INVTRANSACTIONS_URL = "https://eiyp.fa.us6.oraclecloud.com/analytics-ws/saw.dll?SoapImpl=xmlViewService";
	public static final String SOAP_ITEMCOSTS_URL = "https://eiyp.fa.us6.oraclecloud.com:443/fscmService/ItemCostServiceV2";
	public static final String SOAP_FIND_ORGANIZATION_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/FoundationPartiesOrganizationService";
	public static final String SOAP_FIND_LOCATION_URL = "https://eiyp.fa.us6.oraclecloud.com:443/crmService/FoundationPartiesLocationService";
	public static final String SOAP_AUTHENTICATION_ERROR = "Authentication error. Invalid session ID or session expired";
	
	public static final String SET_ID = "300000000002027";
	public static final String DEFAULT_CONTACT_ID = "300000005159152";
	public static final String NC_LOCATION="NCTXTLOCATION";
	
	public static final String CLOUD_CREDENTIALS = "INTEGUSER:Smartech2019$";
	public static final String RESPONSE_TXT_PATH ="NCRESPONSEPATH";
	public static final String NC_PREFIX ="NC";
	public static final String PROCCESEDFILE_SUFFIX ="_read";
	
	public static List<String> resultValues = new ArrayList<String>();
	
	public static final String SOAP_BULKIMPORT_URL = "https://eiyp.fa.us6.oraclecloud.com:443/fscmService/ErpIntegrationService";
	
	public static final String CLOUD_REQUEST_ERROR = "ERROR";
	public static final String CLOUD_REQUEST_SUCCEEDED = "SUCCEEDED";
	
	public static final String FILE_REQUEST_NEW ="NUEVO";
	public static final String FILE_REQUEST_SENT ="ENVIADO";
	public static final String FILE_REQUEST_ERROR ="ERROR";
	public static final String FILE_REQUEST_SUCCESS ="PROCESADO";
	 
	public static final String INVENTORY_STATUS_OK ="ESTABLE";
	public static final String INVENTORY_STATUS_UNESPECTED ="REVISAR";	 	 
	
	public static final String XML_NULL_RESPONSE = "{\"xsi:nil\":\"true\"}";
	public static final int PAYLOAD_CUSTOMER_FETCH_SIZE =500;
	public static final int MAX_REQUEST_ATTEMPTS = 15;
	
	public static final String KEY_CURRENT_CODE_CEDI = "CURRENT_CODE_CEDI";
	public static final String KEY_INVENTORY_DAYS_ADD = "INVENTORY_DAYS_ADD";
	
	public static void addResult(String result) {
		resultValues.add(result);
	}
	
	public static List<String> getResults(){
		return resultValues;
	}

}

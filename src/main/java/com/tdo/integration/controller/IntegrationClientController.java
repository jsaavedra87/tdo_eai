package com.tdo.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import com.tdo.integration.data.services.IntegrationCDBService;
import com.tdo.integration.data.services.IntegrationRestServiceTDO;

public class IntegrationClientController {

	@Autowired
	IntegrationCDBService integrationCDBService;
	
	@Autowired
	IntegrationRestServiceTDO integrationRestServiceTDO;
	
	@Scheduled(fixedDelay=90000, initialDelay=1000)	
	public void getInventoryTransactions() {
		integrationCDBService.newInventoryTransactionRecord();
	}
	
}

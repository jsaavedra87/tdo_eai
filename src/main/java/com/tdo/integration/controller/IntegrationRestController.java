package com.tdo.integration.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tdo.integration.data.model.tdo.CustomerAdditionalSpecs;
import com.tdo.integration.data.model.tdo.Udc;
import com.tdo.integration.data.services.IntegrationCDBService;
import com.tdo.integration.data.services.IntegrationDataService;
import com.tdo.integration.data.services.IntegrationRestServiceTDO;
import com.tdo.integration.dto.ArLockBoxImportC;
import com.tdo.integration.dto.ArticuloDTO;
import com.tdo.integration.dto.CreditDTO;
import com.tdo.integration.dto.CreditNoteDTO;
import com.tdo.integration.dto.CreditNoteResponseDTO;
import com.tdo.integration.dto.CustomerDTO;
import com.tdo.integration.dto.GLInterface;
import com.tdo.integration.dto.InventoryMonitorDTO;
import com.tdo.integration.dto.InventoryTransaction;
import com.tdo.integration.dto.InventoryTransactionDTO;
import com.tdo.integration.dto.RaInterface;
import com.tdo.integration.dto.SalesDTO;
import com.tdo.integration.pojo.ArticulosJson;
import com.tdo.integration.pojo.BaseResponse;
import com.tdo.integration.pojo.ClientesJson;
import com.tdo.integration.services.NCService;
import com.tdo.integration.util.AppConstants;
import com.tdo.integration.ws.SOAPCustomerService;
import com.tdo.integration.ws.SOAPFileImportService;
import com.tdo.integration.ws.SOAPInventoryTransactionsService;
import com.tdo.integration.ws.SOAPItemMasterService;

@RestController
@RequestMapping("/rest")
public class IntegrationRestController {
	
	@Autowired
	ServletContext servletContext;
	
	@Autowired
	IntegrationRestServiceTDO integrationRestServiceTDO;
	
	@Autowired
	IntegrationDataService integrationDataService;
	
	@Autowired
	SOAPCustomerService SOAPCustomerService;
	
	@Autowired
	SOAPItemMasterService SOAPItemMasterService;
	
	@Autowired
	IntegrationCDBService integrationCDBService;
	
	@Autowired
	SOAPInventoryTransactionsService SOAPInventoryTransactionsService;
	
	@Autowired
	SOAPFileImportService SOAPFileImportService;
	
	@Autowired 
	NCService ncService;
		
	@RequestMapping(value = "/inventory", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse inventory(@RequestBody InventoryTransaction request) {
		return integrationRestServiceTDO.processInventoryFile(request);
	}
	
	@RequestMapping(value = "/ar", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse ar(@RequestBody RaInterface request) {
		return integrationRestServiceTDO.processARFile(request);
	}
	
	@RequestMapping(value = "/gl", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse gl(@RequestBody GLInterface request) {
		return integrationRestServiceTDO.processGlFile(request);
	}

	@RequestMapping(value = "/oracleCustomers", method = RequestMethod.GET)
	 public List<ClientesJson> oracleCustomers(@RequestParam String request) {
		if("".equals(request)) {
			return null;
		}
		return SOAPCustomerService.getCustomers(request);
	}
	
	@RequestMapping(value = "/createOracleCustomers", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public List<CustomerDTO> createOracleCustomers(@RequestBody List<CustomerDTO> request) {
		return SOAPCustomerService.insertCustomers(request);
	}

	@RequestMapping(value = "/getCustomerCreditByName", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public List<CreditDTO> getCustomerCreditByName(@RequestBody String request) {
		return integrationCDBService.getCustomerCreditByName(request);
	}
	
	@RequestMapping(value = "/updateCustomerCreditLimit", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse updateCustomerCreditLimit(@RequestBody CreditDTO request) {
		return integrationCDBService.updateCustomerCreditLimit(request);
	}
	
	@RequestMapping(value = "/updateCustomerCredit", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public List<CreditDTO> updateCustomerCredit(@RequestBody List<CreditDTO> request) {
		return integrationCDBService.updateCustomerCredit(request);
	}

	@RequestMapping(value = "/createCustomerPayment", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse createCustomerPayment(@RequestBody List<ArLockBoxImportC> request) {
		return integrationRestServiceTDO.processARLockBoxImportCFile(request);
	}
	
	@RequestMapping(value = "/oracleItems", method = RequestMethod.GET)
	 public List<ArticulosJson> oracleItems(@RequestParam String request) {
		if("".equals(request)) {
			return null;
		}
		return SOAPItemMasterService.getItems(request);
	}
	
	@RequestMapping(value = "/registraClienteCultivo", method = RequestMethod.POST)
	 public BaseResponse registraClienteCultivo(@RequestBody CustomerAdditionalSpecs specs) {
		return integrationRestServiceTDO.registerCustomerAdditionalSpecs(specs);	
	}
	
	@RequestMapping(value = "/salesService", method = RequestMethod.POST)
	 public BaseResponse salesService(@RequestBody SalesDTO sales) {
		return integrationCDBService.insertSales(sales);
	}
		
	
	@RequestMapping(value = "/registro", method = RequestMethod.POST)
	 public BaseResponse registro(@RequestBody String message) {
		
		JSONObject jsonObj=new JSONObject(message);
		String str = "*" + jsonObj.getString("message") + "*" + " Recibido en: " +  getCurrentTimeStamp();
		System.out.println(str);
		AppConstants.addResult(str);
		BaseResponse response = new BaseResponse();
		response.setContent("Su registro ha sido recibido");
		return response;
	}
		
	@RequestMapping(value = "/invTransfers", method = RequestMethod.GET)
	 public List<InventoryTransactionDTO> transfers(@RequestParam String request) {
		if("".equals(request)) {
			return null;
		}
		return integrationCDBService.getInventoryTransactions(request);
	}
	
	@RequestMapping(value = "/avgRate", method = RequestMethod.GET)
	 public double getAvgRate(@RequestParam String request) {
		if("".equals(request)) {
			return 0;
		}
		return SOAPInventoryTransactionsService.getItemRateAverage(request);
	}
	
	@RequestMapping(value = "/usdItemCost", method = RequestMethod.GET)
	 public List<ArticuloDTO> oracleItems(@RequestBody List<ArticuloDTO> list) {
		return null;
	}
	
	
	public String getCurrentTimeStamp() {
	    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date());
	}
	
	@RequestMapping(value ="/getUdcList", method = RequestMethod.GET)
	public List<Udc> getCurrentExchangeRate (@RequestParam String request) {
		
		if("".equals(request)) {
			return null;
		}
	return  integrationCDBService.getUdcByUdcKey(request);
	}
	
	@RequestMapping(value = "/registraUdc", method = RequestMethod.GET)
	 public BaseResponse registraUdc(@RequestParam String udcValue, @RequestParam String udcKey) throws Exception {
		
		List<Udc> udcList =integrationCDBService.getUdcByUdcKey(udcKey);
		Udc udc=null;
		if(udcList.size()>0) {
			udc = udcList.get(0);
			udc.setUdcValue(udcValue);
		}
		
		return integrationCDBService.saveOrUpdateUdc(udc);	
	}
	
	@RequestMapping(value = "/processCreditNote", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse processCreditNote(@RequestBody CreditNoteDTO request) {
		return ncService.processCreditNoteService(request);
	}
	
	@RequestMapping(value ="/getTxtResponses", method = RequestMethod.GET)
	public List<CreditNoteResponseDTO> getTxtResponses () {
		try {
			return  ncService.getTxtResponse();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(value = "/arFind", method = RequestMethod.POST, consumes = "application/json; charset=utf-8")
	 public BaseResponse arFind(@RequestBody String request) {
		return integrationRestServiceTDO.processARFind(request);
	}
	
	@RequestMapping(value = "/inventoryMonitor", method = RequestMethod.POST)
	 public BaseResponse inventoryMonitor(@RequestBody InventoryMonitorDTO inventoryMonitorDTO) {
		return integrationCDBService.updateInventoryMonitor(inventoryMonitorDTO);
	}
	
}
